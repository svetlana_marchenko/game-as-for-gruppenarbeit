# Abgabe der Projektinhalte

## Projektbeschreibung

Unser Projekt ist eine **Sammlung von Minispielen**. Diese beinhaltet ein Punktesystem und eine Rangliste/Bestenliste, damit die Motivation für den Spieler hoch bleibt. Außerdem gibt es verschiedene Schwierigkeitsstufen zu vielen Spiel, damit der Spieler immer eine Herausforderung hat und sich weiterentwickeln kann. 

## Nutzen

Es sollen Spiele mit verschiedenen Graden an Komplexität entwickelt werden. So gibt es Knobelspiele, bei den man sich im Voraus Gedanken machen muss und somit strategisch denkt. Es gibt Spiele, bei denen man einfach nur reagieren muss. Diese eignen sich eher zum „Abschalten“ nach einem schwierigen Tag. Es gibt Spiele, die das Gedächtnis herausfordern und somit eher der Kategorie „Gehirnjogging“ zuzuordnen sind. Es ist somit eine Sammlung für alle Lebenslagen. Auch die verschiedenen Schwierigkeitsstufen sollen eine möglichst breite Nutzerbasis abdecken. 


## Gruppenname/Team

P03/Minispiele-Sammlung - Playbox


## Aktueller Projektstand

Das Projekt ist abgeschlossen. Erweiterungen der Spielsammlung sind jedoch jederzeit möglich.


## Kurzanleitung

Die Anwendung der App ist sehr simpel gehalten. Führt man die App aus, so wird man in einen Startbildschirm geleitet. Von dort lassen sich alle in der App verfügbaren Spiele auswählen und es kann losgespielt werden. Sollten Spiele nicht sofort verstanden werden, gibt es für komplexere Spiele einen Informationsbutton, der einem weiterhilft.


## Screenshots der laufenden Anwendung

![Screenshot_Startscreen](./ProjektInfo/Screenshots/Screenshot_20210205-131441_Playbox.jpg)
![Screenshot_GameMenu](./ProjektInfo/Screenshots/Screenshot_20210205-131524_Playbox.jpg)
![Screenshot_Minesweeper](./ProjektInfo/Screenshots/Screenshot_20210205-093921_Playbox.jpg)
![Screenshot_Statistics](./ProjektInfo/Screenshots/Screenshot_20210205-131505_Playbox.jpg)
![Screenshot_Settings](./ProjektInfo/Screenshots/Screenshot_20210205-131518_Playbox.jpg)

## Arbeitsaufteilung/Teamrollen

Unsere Arbeitsaufteilung sah folgendermaßen aus:

1.  Noah Wiedemer: Scrum Master/Integrator/Organisator
2.  Alexander Becker: Product Owner/Entwickler/Tester
3.  Bjarne Janesch: Entwickler/Tester
4.  Mathias Curio: Entwickler/Tester
5.  Michelle Moazami: Entwickler/Tester
6.  Svetlana Marchenko: Entwickler/Tester


## Grobe Architektur:

Das Projekt besteht aus einer Datenbank und einem Android Studio Projekt. Im Android Studio Projekt ist jedes Spiel als ein Package implementiert und jedes Spiel Package hat eine MainActiviy, die die entsprechende GameView startet. Von der MainActivity des ganzen Projekts oder auch des Menüs wird werden die entsprechenden Intents/Activities gestartet.

## How to build & run:

1.  In der Toolbar wird das Dropdownmenü für die Run Configurations geöffnet.
2.  In dem Dropdownmenü wird die App ausgewählt die gestartet werden soll. 
3.  Das Dropdownmenü für die Geräteauswahl kann genutzt werden um festzulegen wo die App local gestartet werden soll.
4.  Ein Klick auf Run startet den Build Prozess.

## How it is deployed:

Für Nutzer wird das Projekt mithilfe einer APK bereitgestellt. Diese muss vom Nutzer nur heruntergeladen und installiert werden. Dann kann die App genutzt werden.
Für Entwickler wird das Projekt als gitlab-Projekt zur Verfügung gestellt. Dieses kann heruntergeladen und dann mit Android Studio bearbeitet werden.

## Verwendete Tools/Technologien/Frameworks

Folgende Tools, Technologien und Frameworks haben wir für die Umsetzung unseres Projekte genutzt:

1.  IDE/Framework: Android Studio
2.  Management Tools: Trello, Miro, git, Discord
3.  Design Tools: Figma

## Links zu Ressourcen

Link zu Trello:

    https://trello.com/b/zHeGIl56/minigames

Link zu lauffähigem Produkt bzw. Software:

    https://www.dropbox.com/s/76tbfuzis2uh3xu/app-debug.apk?dl=0

Link zu Master/Release-Branch git:

    https://gitlab.beuth-hochschule.de/minigame-app
