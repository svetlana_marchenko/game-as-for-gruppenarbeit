package com.example.myapplication.game_DragonFlight;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;

import java.io.IOException;

public class GameDragonFlight extends Activity {

    private int currentTheme;

    private ImageButton backToMenu;
    private ImageButton toSettings;
    private long startTime;
    private GamePanel gamePanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.startTime = System.currentTimeMillis();
        // Get saved Theme out of Shared Preferances
        changeTheme();


        super.onCreate(savedInstanceState);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;

        setContentView(R.layout.activity_game_dragonflight);

        FrameLayout game = (FrameLayout) findViewById(R.id.frameLayout);

        gamePanel = new GamePanel(this);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });
        game.setZ(-1);
        game.addView(gamePanel);
    }

        // Brings the user to the previous activity
        private void goBack() {
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }

        // Brings the user to the settings menu
        private void goToSettings() {
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    @Override
    protected void onStop() {

        for(int i = 0; i < gamePanel.roundsPlayed; i++){
            ((GlobalVariables) this.getApplication()).addOneRound();
        }

        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_dragon(millis); // Hier setTime_deinSpiel

    }
}




