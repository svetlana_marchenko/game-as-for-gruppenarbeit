package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.myapplication.game_DragonFlight.Constants;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class Stats extends AppCompatActivity {

    private ImageButton backToMenu;
    private int currentTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        backToMenu = findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBackToMenu();
            }
        });

        // Set Total Playtime and Total Rounds played
        TextView playtime = findViewById(R.id.totalPlaytimeText);
        TextView rounds = findViewById(R.id.totalRoundsText);

        long totalPlaytime = ((GlobalVariables) this.getApplication()).getTotalTime();

        String totalTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(totalPlaytime),
                TimeUnit.MILLISECONDS.toMinutes(totalPlaytime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalPlaytime)),
                TimeUnit.MILLISECONDS.toSeconds(totalPlaytime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalPlaytime)));

        int totalRounds = ((GlobalVariables) this.getApplication()).getTotalRounds();

        playtime.setText(totalTime);
        rounds.setText(String.valueOf(totalRounds));

        // Adds the data to the bar chart and displays it
        setChart();

    }

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goBackToMenu() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void setChart() {

        // Prepare Data for Chart

        Game_Helper g = new Game_Helper();
        HashMap<String, Long> playtimes = new HashMap<>();

        playtimes.put(g.GAMENAME_2048, ((GlobalVariables) this.getApplication()).getTime_2048());
        playtimes.put(g.GAMENAME_CAPITALS, ((GlobalVariables) this.getApplication()).getTime_capitals());
        playtimes.put(g.GAMENAME_DRAGONFLIGHT, ((GlobalVariables) this.getApplication()).getTime_dragon());
        playtimes.put(g.GAMENAME_FLAGS, ((GlobalVariables) this.getApplication()).getTime_flags());
        playtimes.put(g.GAMENAME_FLAPPYBIRD, ((GlobalVariables) this.getApplication()).getTime_flappybird());
        playtimes.put(g.GAMENAME_GOSPRINGER, ((GlobalVariables) this.getApplication()).getTime_knight());
        playtimes.put(g.GAMENAME_KNIFFEL, ((GlobalVariables) this.getApplication()).getTime_kniffel());
        playtimes.put(g.GAMENAME_LONGERLINE, ((GlobalVariables) this.getApplication()).getTime_longerline());
        playtimes.put(g.GAMENAME_MINESWEEPER, ((GlobalVariables) this.getApplication()).getTime_minesweeper());
        playtimes.put(g.GAMENAME_PUZZLE, ((GlobalVariables) this.getApplication()).getTime_puzzle());
        playtimes.put(g.GAMENAME_SNAKE, ((GlobalVariables) this.getApplication()).getTime_snake());
        playtimes.put(g.GAMENAME_SUDUKO, ((GlobalVariables) this.getApplication()).getTime_sudoku());
        playtimes.put(g.GAMENAME_XOBIG, ((GlobalVariables) this.getApplication()).getTime_xobig());

        List<Map.Entry<String, Long>> sortedTimes = sortHashMap(playtimes);

        String[] arr = new String[10];
        arr[0] = sortedTimes.get(7).toString().split("=")[0];
        arr[1] = sortedTimes.get(6).toString().split("=")[0];
        arr[2] = sortedTimes.get(5).toString().split("=")[0];
        arr[3] = sortedTimes.get(4).toString().split("=")[0];
        arr[4] = sortedTimes.get(3).toString().split("=")[0];
        arr[5] = sortedTimes.get(2).toString().split("=")[0];
        arr[6] = sortedTimes.get(1).toString().split("=")[0];
        arr[7] = sortedTimes.get(0).toString().split("=")[0];


        // Barchart for displaying playtime per game
        ArrayList<BarEntry> barEntriesArrayList = new ArrayList<>();

        int j = 7;
        for (int i =0; i < 8;i++){
            barEntriesArrayList.add(new BarEntry(i,sortedTimes.get(j).getValue()));
            j--;
        }

        HorizontalBarChart barChart = findViewById(R.id.bar_chart);

        BarDataSet barDataSet = new BarDataSet(barEntriesArrayList,"Playtime");
        barDataSet.setColors(ColorTemplate.PASTEL_COLORS);

        Description description = new Description();
        description.setText("");
        barChart.setDescription(description);

        BarData barData = new BarData(barDataSet);
        barChart.setData(barData);

        barChart.getLegend().setEnabled(false);
        barChart.getAxisRight().setTextColor(Color.WHITE);
        barChart.getXAxis().setTextColor(Color.WHITE);
        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(arr));
        barChart.getXAxis().setGranularity(1f);
        barChart.getXAxis().setDrawGridLines(false);
        barData.setDrawValues(false);

        barChart.getAxisRight().setValueFormatter(new IndexAxisValueFormatter());
        YAxis left = barChart.getAxisLeft();
        left.setDrawLabels(false); // no axis labels
        left.setDrawAxisLine(false); // no axis line
        left.setDrawGridLines(false); // no grid lines
        barChart.getAxisRight().setDrawAxisLine(false);
        barChart.getXAxis().setDrawAxisLine(false);
        barChart.getAxisRight().setDrawGridLines(false);

        barChart.animateY(200);
        barChart.invalidate();

    }


    // Sort a list by its values
    // Source: https://stackoverflow.com/questions/32496409/java-sort-a-hashmap-on-value

    private List<Map.Entry<String, Long>> sortHashMap(HashMap<String, Long> map){

        Set<Map.Entry<String, Long>> set = map.entrySet();
        List<Map.Entry<String, Long>> list = new ArrayList<Map.Entry<String, Long>>(set);
        Collections.sort( list, new Comparator<Map.Entry<String, Long>>()
        {
            public int compare( Map.Entry<String, Long> o1, Map.Entry<String, Long> o2 )
            {
                int result = (o2.getValue()).compareTo( o1.getValue() );
                if (result != 0) {
                    return result;
                } else {
                    return o1.getKey().compareTo(o2.getKey());
                }
            }
        } );

        return list;
    }

}
