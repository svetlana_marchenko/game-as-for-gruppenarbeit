// Alexander Becker

package com.example.myapplication.game_longerline;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;
import com.example.myapplication.game_DragonFlight.Constants;
import com.example.myapplication.game_DragonFlight.GamePanel;

import java.util.Random;

public class Game_LongerLine extends AppCompatActivity {

    long startTime;

    int    absoluteLengthLineRed  = 0;
    int    absoluteLengthLineBlue = 0;
    int    ivWidth                = 360;
    int    ivHeight               = 480;
    double variancePercent        = 0.3;

    ImageView imageView;

    int xVarianceInPixels = (int) (ivWidth * variancePercent);
    int yVarianceInPixels = (int) (ivHeight * variancePercent);

    Random random = new Random();

    // Abweichung zwischen Startecke und Startpunkt, default sollte ca. 20% betragen

    // je nach Schwierigkeitsgrad: 2, 3, 4
    int numberOfPartsInLine = 3;

    // ergibt die mögliche Varianz der Pixelanzahl von 20%
    // bei einer ImageView-Breite von 400 Pixel ergibt das eine Varianz von 80 Pixel
    // d. h. der Startpunkt befindet sich zwischen einer Ecke und einer Breite/Höhe von 80 Pixel

    int correctAnswers;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    TextView tvCorrectAnswers;
    TextView tvRecord;

    private int currentTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.startTime = System.currentTimeMillis();
        ((GlobalVariables) this.getApplication()).addOneRound();

        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longerline);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        // Views einbinden, onClickListener, Records, startCorner, createLine in Methode "initialise" ausgelagert,
        // da diese von mehreren Stellen aus aufgerufen wird:
        // - wenn Level gewonnen ~ korrekt geraten: neu initialisieren
        // - wenn Level verloren ~ falsch geraten, in neuer Activity Button "Spiel neu starten" geklickt

        imageView = findViewById(R.id.imageView);
//        ivWidth  = imageView.getDrawable().getIntrinsicWidth();
//        ivHeight = imageView.getDrawable().getIntrinsicHeight();
//        ivWidth   = imageView.getLayoutParams().width;
//        ivHeight  = imageView.getLayoutParams().width;

//        ivWidth = 360;
//        ivHeight = 540;
//
//        xVarianceInPixels = (int) (ivWidth * variancePercent);
//        yVarianceInPixels = (int) (ivHeight * variancePercent);

//        ImageView image = (ImageView) findViewById(R.id.imageView);
//        ivWidth = image.getLayoutParams().width;
//        ivHeight = image.getLayoutParams().width;

//        int width = imgView.getDrawable().getIntrinsicWidth();
//        int height = imgView.getDrawable().getIntrinsicHeight();


//        print(".....");
//        print(".....");
//        print(".....");
//        print("width: " + ivWidth);
//        print("height: " + ivHeight);

        Button buttonRed  = findViewById(R.id.buttonRed);
        Button buttonBlue = findViewById(R.id.buttonBlue);

        buttonRed.setOnClickListener(onClickListener);
        buttonBlue.setOnClickListener(onClickListener);

        tvCorrectAnswers = findViewById(R.id.tvCorrectAnswers);
        tvRecord = findViewById(R.id.tcRecord);
        correctAnswers = 0;

        initialise();

    } // Ende: onCreate

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_longerline(millis);

    }

    public void initialise() {
        tvCorrectAnswers.setText(String.valueOf(correctAnswers));
        tvRecord.setText("" + getRecord());

        int startCorner = getStartCorner();
//        createLine("red", startCorner);
//        createLine("blue", startCorner);
        createLine(startCorner);

//        print(".....");
//        print("startCorner: " + startCorner);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buttonRed:
                    checkIsCorrect("red");
                    break;
                case R.id.buttonBlue:
                    checkIsCorrect("blue");
                    break;
            }
        }
    };


    //    private void createLine(String lineColor, int startCorner) {
    private void createLine(int startCorner) {

        Bitmap bitmap = Bitmap.createBitmap(ivWidth, ivHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint  paint  = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(4);
        paint.setStrokeCap(Paint.Cap.ROUND);

        for (int k = 0; k <= 1; k++) {

            if (k == 1) {
                if (startCorner == 4 || startCorner == 2) startCorner--;
                else startCorner++;
            }

            int xStart = getStartX(startCorner);
            int yStart = getStartY(startCorner);
            int xNew;
            int yNew;

            if (k == 0)
                paint.setColor(Color.RED);
            if (k == 1)
                paint.setColor(Color.BLUE);

//            if (lineColor.equals("red"))
//                paint.setColor(Color.RED);
//            else if (lineColor.equals("blue"))
//                paint.setColor(Color.BLUE);

//            print(".....");
//            print("startCorner:" + startCorner);
//            print("startPoint:");
//            print("   x: " + xStart);
//            print("   y: " + yStart);


            for (int i = 0; i < numberOfPartsInLine; i++) {
                xNew = getNewX(xStart);
                yNew = getNewY(yStart);

//                print(".....");
//                print("newPoint:");
//                print("   x: " + xNew);
//                print("   y: " + yNew);

                // hier fehlt noch ein Paint paint Objekt
                canvas.drawLine(xStart, yStart, xNew, yNew, paint);

                // Länge der jeweiligen Lineien (1 und 2) bestimmen und im Attribut speichern
//                if (lineColor.equals("red"))
//                    absoluteLengthLineRed += getLineLength(xStart, yStart, xNew, yNew);
//                else if (lineColor.equals("blue"))
//                    absoluteLengthLineBlue += getLineLength(xStart, yStart, xNew, yNew);

                if (k == 0)
                    absoluteLengthLineRed += getLineLength(xStart, yStart, xNew, yNew);
                if (k == 1)
                    absoluteLengthLineBlue += getLineLength(xStart, yStart, xNew, yNew);

                xStart = xNew;
                yStart = yNew;

            } // Ende: for

//            print(".....");
//            print("lineLength red: " + absoluteLengthLineRed);
//            print("lineLength blue: " + absoluteLengthLineBlue);

            imageView.setImageBitmap(bitmap);
        }

    } // Ende: createLine

    private void checkIsCorrect(String guessedColorOfLongerLine) {

        print("guessedColorOfLongerLine: " + guessedColorOfLongerLine);

        String longerLine;
        // längere/längste Linie bestimmen
        if (absoluteLengthLineRed > absoluteLengthLineBlue)
            longerLine = "red";
        else
            longerLine = "blue";

        print(".....");
        print("longer/longest line: " + longerLine);

        // User-Eingabe mit korrekter Antwort vergleichen
        if (!guessedColorOfLongerLine.equals(longerLine)) {
            // falsch, Spiel vorbei...
            // Activity, die anzeigt: Spielstand/Punkte, Button "neu starten", Button "zurück zur Übersicht"
            print("falsch geraten");
            correctAnswers = 0;
            initialise();
            ((GlobalVariables) this.getApplication()).addOneRound();

        } else {
            // richtig geraten, weiterspielen

            print("richtig geraten");

            correctAnswers++;
            tvCorrectAnswers.setText(String.valueOf(correctAnswers));

            if (correctAnswers > getRecord()) {
                setRecord(correctAnswers);
            }

            // Werte zurücksetzen
            absoluteLengthLineRed = 0;
            absoluteLengthLineBlue = 0;
            // Canvas/Bitmap zurücksetzen

            int startCorner = getStartCorner();
            // createLine("red", startCorner);
            // createLine("blue", startCorner);
            createLine(startCorner);

        }

    } // Ende: checkIsCorrect

    private int getRecord() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPref.getInt("record", 0);
    }

    private void setRecord(int correctAnswers) {
        SharedPreferences        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor     = sharedPref.edit();
        editor.putInt("record", correctAnswers);
        editor.commit();

        // Put record into database
        String          name       = sharedPref.getString("playername", "Username");
        String          gamename   = new Game_Helper().GAMENAME_LONGERLINE;
        Database_Helper database   = new Database_Helper();
        Database_Score  scoreEntry = new Database_Score(gamename, correctAnswers, name, 1);
        database.addScoreToDatabase(scoreEntry);

    }


    private void print(String txt) {
        System.out.println(txt);
    }

    //   corner:
    //   ----------------
    //   |1            2|
    //   |              |
    //   |              |
    //   |              |
    //   |3            4|
    //   ----------------
    // corner 1: oben links
    // corner 2: oben rechts
    // corner 3: unten links
    // corner 4: unten rechts

    private int getStartX(int corner) {
        int xStart = 0;
        if (corner == 1 || corner == 3) {
            // linke Seite
            xStart = random.nextInt(xVarianceInPixels);
        } else {
            // rechte Seite
            xStart = random.nextInt(xVarianceInPixels) + (ivWidth - xVarianceInPixels);
        }
        return xStart;
    }

    private int getStartY(int corner) {
        int yStart = 0;
        if (corner == 1 || corner == 2) {
            // obere Seite
            yStart = random.nextInt(yVarianceInPixels);
        } else {
            // untere Seite
            yStart = random.nextInt(yVarianceInPixels) + (ivHeight - yVarianceInPixels);
        }
        return yStart;
    }

    private int getNewX(int xStart) {
        // Breite in Hälfte aufteilen
        // wenn xStart kleiner als Hälfte, sind wir in der linken Hälfte: müssen nach rechts gehen
        // wenn xStart größer als Hälfte, sind wir in der rechten Hälfte: müssen nach links gehen
        int xNew;
//        Random random = new Random();
        if (xStart <= ivWidth / 2) {
            // neuen Punkt am rechten Rand bestimmen
//            xNew = random.nextInt(ivWidth - xVarianceInPixels, ivWidth);
            // liefert einen Punkt rechts, im Bereich [max. Breite - Varianz] und max. Breite
//            xNew = random.nextInt(xVarianceInPixels) + (ivWidth - xVarianceInPixels);
            xNew = random.nextInt(xVarianceInPixels * 2) + (ivWidth - 2 * xVarianceInPixels);
        } else {
            // neuen Punkt am linken Rand bestimmen
            xNew = random.nextInt(xVarianceInPixels * 2);
        }
        return xNew;
    }

    private int getNewY(int yStart) {
        // Höhe in Hälfte aufteilen
        // wenn yStart kleiner als Hälfte, sind wir in der oberen Hälfte: müssen nach unten gehen
        // wenn yStart größer als Hälfte, sind wir in der unteren Hälfte: müssen nach oben gehen
        int yNew;
//        Random random = new Random();
        if (yStart <= ivHeight / 2) {
            // neuen Punkt am unteren Rand bestimmen
            // yNew = random.nextInt(ivHeight - yVarianceInPixels, ivHeight);
            // liefert den Bereich unten, von Varianz bis maximale Höhe
            yNew = random.nextInt(yVarianceInPixels) + (ivHeight - yVarianceInPixels);
        } else {
            // neuen Punkt am oberen Rand bestimmen
//            yNew = random.nextInt(0, yVarianceInPixels);
            yNew = random.nextInt(yVarianceInPixels);
        }
        return yNew;
    }

    private int getLineLength(int xStart, int yStart, int xNew, int yNew) {
        // über Pythagoras Länge bestimmen:
        // entspricht x² + y²
        int lineLenght = (int) Math.pow(Math.abs(xStart - xNew), 2) + (int) Math.pow(Math.abs(yStart - yNew), 2);
        // Wurzel aus Summe ziehen
        lineLenght = (int) Math.sqrt(lineLenght);
        return lineLenght;
    }


    private int getStartCorner() {
        // liefert eine Zahl zwischen 1 und 4, die die Startecke repräsentiert
        // nextInt(4) liefert 0-3 ... + 1 liefert Zahlenbereich 1-4
//        Random random = new Random();
        int corner = random.nextInt(4) + 1;

        print(".....");
        print("corner: " + corner);

        return corner;
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


} // GameActivity
