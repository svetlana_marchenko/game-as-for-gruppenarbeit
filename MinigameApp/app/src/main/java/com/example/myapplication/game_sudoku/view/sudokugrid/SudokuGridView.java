package com.example.myapplication.game_sudoku.view.sudokugrid;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.myapplication.game_sudoku.GameEngine;

public class SudokuGridView extends GridView {

    private final Context context;

    private int clicked = -1;

    public SudokuGridView(final Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        SudokuGridViewAdapter gridViewAdapter = new SudokuGridViewAdapter(this.getContext());

        setAdapter(gridViewAdapter);

        setOnItemClickListener(new OnItemClickListener() {
            View viewPrev;
            View viewX;


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int x = position % 9;
                int y = position / 9;
                GameEngine.getInstance().setSelectedPosition(x, y);

                int[][] grid = new int[9][9];

                // Sets the current clicked item to a new background color
                try {
                    if(clicked != -1) {
                        viewPrev = (View) parent.getChildAt(clicked);
                        viewPrev.setBackgroundColor(Color.parseColor("#ffffff"));
                    }
                    clicked = position;
                    if(clicked == position) {
                        view.setBackgroundColor(Color.parseColor("#9999ff"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}