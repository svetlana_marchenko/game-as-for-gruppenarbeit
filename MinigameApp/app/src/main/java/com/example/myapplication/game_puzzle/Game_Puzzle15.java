// Alexander Becker

package com.example.myapplication.game_puzzle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;

import static com.example.myapplication.R.id.textView4;

public class Game_Puzzle15 extends AppCompatActivity {

    long startTime;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    private int                 N;
    private int                 turnsCounter = 0;
    private TextView            tvYourScore;
    private TextView            tvHighscore;
    private Game_Puzzle15_Cards gamePuzzle15Cards;
    private ImageButton[][]     buttons;

    private TextView textView;
    private TextView textView4;

    private final int EASY   = 1;
    private final int MEDIUM = 2;
    private final int HARD   = 3;
    private       int difficulty;

    private int BUTTON_ID[][];
    private int CARDS_ID[];
    private int CARDS_WINNER_ID[];

    private int currentTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.startTime = System.currentTimeMillis();

        changeTheme();

        // default value medium
        difficulty = MEDIUM;
        Bundle b          = getIntent().getExtras();
        int diff = b.getInt("difficulty");
        if(diff == 1)       difficulty = EASY;
        else if(diff == 2)  difficulty = MEDIUM;
        else                difficulty = HARD;

        switch (difficulty) {
            case 1:
                setContentView(R.layout.activity_puzzle15_3x3);
                initializeEasy();
                break;
            case 2:
                setContentView(R.layout.activity_puzzle15_4x4);
                initializeMedium();
                break;
            case 3:
                setContentView(R.layout.activity_puzzle15_5x5);
                initializeHard();
                break;
        }

//        setContentView(R.layout.activity_puzzle15_4x4);
        textView = findViewById(R.id.textView);
        textView4 = findViewById(R.id.textView4);
        tvYourScore = findViewById(R.id.tvAnzahlZuege);
        tvHighscore = findViewById(R.id.tvRekord);
        tvHighscore.setText("0");

        Button buttonNewGame = findViewById(R.id.buttonNeuesSpiel);
        buttonNewGame.setOnClickListener(onClickListener);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        buttons = new ImageButton[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                buttons[i][j] = findViewById(BUTTON_ID[i][j]);
                buttons[i][j].setOnClickListener(onClickListener);
            }
        }

        gamePuzzle15Cards = new Game_Puzzle15_Cards(N, N);
        newGame();

    } // onCreate

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // ClickListener der Buttons setzen, die die Spielkarten repraesentieren
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (v.getId() == BUTTON_ID[i][j]) {
                        move(i, j);
                    }
                }
            }


            switch (v.getId()) {
                case R.id.buttonNeuesSpiel:
                    newGame();
                    break;
                default:
                    break;
            } // switch
        } // onClick
    }; // View.OnClickListener

    private void initializeEasy() {
        N = 3;
        BUTTON_ID = new int[][]{{R.id.button_00, R.id.button_01, R.id.button_02},
                {R.id.button_10, R.id.button_11, R.id.button_12},
                {R.id.button_20, R.id.button_21, R.id.button_22}};
        CARDS_ID = new int[]{R.drawable.puzzle15_card_00, R.drawable.puzzle15_card_01, R.drawable.puzzle15_card_02, R.drawable.puzzle15_card_03,
                R.drawable.puzzle15_card_04, R.drawable.puzzle15_card_05, R.drawable.puzzle15_card_06, R.drawable.puzzle15_card_07,
                R.drawable.puzzle15_card_08, R.drawable.puzzle15_card_09};
        CARDS_WINNER_ID = new int[]{R.drawable.puzzle15_card_00_winner, R.drawable.puzzle15_card_01_winner, R.drawable.puzzle15_card_02_winner, R.drawable.puzzle15_card_03_winner,
                R.drawable.puzzle15_card_04_winner, R.drawable.puzzle15_card_05_winner, R.drawable.puzzle15_card_06_winner, R.drawable.puzzle15_card_07_winner,
                R.drawable.puzzle15_card_08_winner, R.drawable.puzzle15_card_09_winner};
    } // initializeEasy

    private void initializeMedium() {
        N = 4;
        BUTTON_ID = new int[][]{{R.id.button_00, R.id.button_01, R.id.button_02, R.id.button_03},
                {R.id.button_10, R.id.button_11, R.id.button_12, R.id.button_13},
                {R.id.button_20, R.id.button_21, R.id.button_22, R.id.button_23},
                {R.id.button_30, R.id.button_31, R.id.button_32, R.id.button_33}};
        CARDS_ID = new int[]{R.drawable.puzzle15_card_00, R.drawable.puzzle15_card_01, R.drawable.puzzle15_card_02, R.drawable.puzzle15_card_03,
                R.drawable.puzzle15_card_04, R.drawable.puzzle15_card_05, R.drawable.puzzle15_card_06, R.drawable.puzzle15_card_07,
                R.drawable.puzzle15_card_08, R.drawable.puzzle15_card_09, R.drawable.puzzle15_card_10, R.drawable.puzzle15_card_11,
                R.drawable.puzzle15_card_12, R.drawable.puzzle15_card_13, R.drawable.puzzle15_card_14, R.drawable.puzzle15_card_15};
        CARDS_WINNER_ID = new int[]{R.drawable.puzzle15_card_00_winner, R.drawable.puzzle15_card_01_winner, R.drawable.puzzle15_card_02_winner, R.drawable.puzzle15_card_03_winner,
                R.drawable.puzzle15_card_04_winner, R.drawable.puzzle15_card_05_winner, R.drawable.puzzle15_card_06_winner, R.drawable.puzzle15_card_07_winner,
                R.drawable.puzzle15_card_08_winner, R.drawable.puzzle15_card_09_winner, R.drawable.puzzle15_card_10_winner, R.drawable.puzzle15_card_11_winner,
                R.drawable.puzzle15_card_12_winner, R.drawable.puzzle15_card_13_winner, R.drawable.puzzle15_card_14_winner, R.drawable.puzzle15_card_15_winner};
    } // initializeMedium

    private void initializeHard() {
        N = 5;
        BUTTON_ID = new int[][]{{R.id.button_00, R.id.button_01, R.id.button_02, R.id.button_03, R.id.button_04},
                {R.id.button_10, R.id.button_11, R.id.button_12, R.id.button_13, R.id.button_14},
                {R.id.button_20, R.id.button_21, R.id.button_22, R.id.button_23, R.id.button_24},
                {R.id.button_30, R.id.button_31, R.id.button_32, R.id.button_33, R.id.button_34},
                {R.id.button_40, R.id.button_41, R.id.button_42, R.id.button_43, R.id.button_44}};
        CARDS_ID = new int[]{R.drawable.puzzle15_card_00, R.drawable.puzzle15_card_01, R.drawable.puzzle15_card_02, R.drawable.puzzle15_card_03,
                R.drawable.puzzle15_card_04, R.drawable.puzzle15_card_05, R.drawable.puzzle15_card_06, R.drawable.puzzle15_card_07,
                R.drawable.puzzle15_card_08, R.drawable.puzzle15_card_09, R.drawable.puzzle15_card_10, R.drawable.puzzle15_card_11,
                R.drawable.puzzle15_card_12, R.drawable.puzzle15_card_13, R.drawable.puzzle15_card_14, R.drawable.puzzle15_card_15,
                R.drawable.puzzle15_card_16, R.drawable.puzzle15_card_17, R.drawable.puzzle15_card_18, R.drawable.puzzle15_card_19,
                R.drawable.puzzle15_card_20, R.drawable.puzzle15_card_21, R.drawable.puzzle15_card_22, R.drawable.puzzle15_card_23, R.drawable.puzzle15_card_24};
        CARDS_WINNER_ID = new int[]{R.drawable.puzzle15_card_00_winner, R.drawable.puzzle15_card_01_winner, R.drawable.puzzle15_card_02_winner, R.drawable.puzzle15_card_03_winner,
                R.drawable.puzzle15_card_04_winner, R.drawable.puzzle15_card_05_winner, R.drawable.puzzle15_card_06_winner, R.drawable.puzzle15_card_07_winner,
                R.drawable.puzzle15_card_08_winner, R.drawable.puzzle15_card_09_winner, R.drawable.puzzle15_card_10_winner, R.drawable.puzzle15_card_11_winner,
                R.drawable.puzzle15_card_12_winner, R.drawable.puzzle15_card_13_winner, R.drawable.puzzle15_card_14_winner, R.drawable.puzzle15_card_15_winner,
                R.drawable.puzzle15_card_16_winner, R.drawable.puzzle15_card_17_winner, R.drawable.puzzle15_card_18_winner, R.drawable.puzzle15_card_19_winner,
                R.drawable.puzzle15_card_20_winner, R.drawable.puzzle15_card_21_winner, R.drawable.puzzle15_card_22_winner, R.drawable.puzzle15_card_23_winner, R.drawable.puzzle15_card_24_winner};
    } // initializeHard

    private void newGame() {
        ((GlobalVariables) this.getApplication()).addOneRound();

        textView.setTextColor(textView4.getCurrentTextColor());
        tvYourScore.setTextColor(textView4.getCurrentTextColor());

        gamePuzzle15Cards.shuffleCards();
        turnsCounter = getRecord();
        tvHighscore.setText(String.valueOf(getRecord()));
        turnsCounter = 0;
        update();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_puzzle(millis); // Hier setTime_deinSpiel

    }


    public void move(int row, int column) {
        gamePuzzle15Cards.moveCard(row, column);
        if (gamePuzzle15Cards.isPossible()) {
            turnsCounter++;
            update();
            checkFinished();
        }
    }

    public void update() {
//        tvYourScore.setText(Integer.toString(turnsCounter));
        tvYourScore.setText(Integer.toString(turnsCounter));

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                // aktuelle Zahlenwerte setzen
                buttons[i][j].setImageResource(CARDS_ID[gamePuzzle15Cards.getFieldValue(i, j)]);
            }
        }
    }

    public void checkFinished() {
        if (gamePuzzle15Cards.checkFinished(N, N)) {
//            update();
            if (turnsCounter < getRecord())  setRecord();
            for (int r = 0; r < N; r++) {
                for (int c = 0; c < N; c++) {
//                    buttons[r][c].setImageResource(R.drawable.puzzle15_card_winner);
                    buttons[r][c].setOnClickListener(null);

                }
            }
            setWinnerCards();
        }
    }

    public void setWinnerCards(){
        for (int r = 0; r < N; r++) {
            for (int c = 0; c < N; c++) {
                // get current field value, use value for reading card_id, set new image via card_id

                int value = gamePuzzle15Cards.getFieldValue(r, c);
                int cards_id = CARDS_WINNER_ID[value];
                buttons[r][c].setImageResource(cards_id);
//                buttons[r][c].setImageResource(CARDS_WINNER_ID[gamePuzzle15Cards.getFieldValue(r, c)]);

            }
        }
    }

    public int getRecord() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPref.getInt("Puzzle", 0);
    } // getRecord


    private void setRecord() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("Puzzle", turnsCounter);
        editor.commit();

        // Put record into database
        String          name       = sharedPref.getString("playername", "Username");
        String          gamename   = new Game_Helper().GAMENAME_PUZZLE;
        Database_Helper database   = new Database_Helper();
        Database_Score  scoreEntry = new Database_Score(gamename, -turnsCounter, name, difficulty);
        database.addScoreToDatabase(scoreEntry);
    } // setRecord

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


} // Spiel15_Activity
