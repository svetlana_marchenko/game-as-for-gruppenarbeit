package com.example.myapplication;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class GlobalVariables extends Application {

    SharedPreferences pref;

    @Override
    public void onCreate() {
        super.onCreate();
        this.pref = getApplicationContext().getSharedPreferences("globals", Context.MODE_PRIVATE);
    }

    public void resetAllStats(){

        pref.edit().putLong("totalplaytime", 0).apply();
        pref.edit().putInt("totalroundsplayed", 0).apply();
        pref.edit().putLong("time_puzzle", 0).apply();
        pref.edit().putLong("time_snake", 0).apply();
        pref.edit().putLong("time_minesweeper", 0).apply();
        pref.edit().putLong("time_kniffel", 0).apply();
        pref.edit().putLong("time_flags", 0).apply();
        pref.edit().putLong("time_capitals", 0).apply();
        pref.edit().putLong("time_2048", 0).apply();
        pref.edit().putLong("time_flappybird", 0).apply();
        pref.edit().putLong("time_xobig", 0).apply();
        pref.edit().putLong("time_knight", 0).apply();
        pref.edit().putLong("time_sudoku", 0).apply();
        pref.edit().putLong("time_dragon", 0).apply();
        pref.edit().putLong("time_longerline", 0).apply();

    }

    // Global variables for all Games

    public long getTotalTime() {
        return pref.getLong("totalplaytime", 0);
    }

    public int getTotalRounds() {
        return pref.getInt("totalroundsplayed", 0);
    }


    public void setTotalTime(long time) {
        long totalTime = getTotalTime() + time;
        pref.edit().putLong("totalplaytime", totalTime).apply();
    }

    public void addOneRound() {
        int rounds = pref.getInt("totalroundsplayed", 0) + 1;
        pref.edit().putInt("totalroundsplayed", rounds).apply();
    }

    // Themes

    public int getCurrentTheme() {
        return pref.getInt("currenttheme", 2);
    }

    public void setCurrentTheme(int theme) {
        pref.edit().putInt("currenttheme", theme).apply();
    }


    // Playtime for each game

    public long getTime_puzzle() {
        return pref.getLong("time_puzzle", 0);
    }

    public void setTime_puzzle(long time_puzzle) {
        long time_puzzle1 = getTime_puzzle() + time_puzzle;
        pref.edit().putLong("time_puzzle", time_puzzle1).apply();
    }

    public long getTime_snake() {
        return pref.getLong("time_snake", 0);
    }

    public void setTime_snake(long time_snake) {
        long time_snake1 = getTime_snake() + time_snake;
        pref.edit().putLong("time_snake", time_snake1).apply();
    }

    public long getTime_minesweeper() {
        return pref.getLong("time_minesweeper", 0);
    }

    public void setTime_minesweeper(long time_minesweeper) {
        long time_minesweeper1 = getTime_minesweeper() + time_minesweeper;
        pref.edit().putLong("time_minesweeper", time_minesweeper1).apply();
    }

    public long getTime_kniffel() {
        return pref.getLong("time_kniffel", 0);
    }

    public void setTime_kniffel(long time_kniffel) {
        long time_kniffel1 = getTime_kniffel() + time_kniffel;
        pref.edit().putLong("time_kniffel", time_kniffel1).apply();
    }

    public long getTime_flags() {
        return pref.getLong("time_flags", 0);
    }

    public void setTime_flags(long time_flags) {
        long time_flags1 = getTime_flags() + time_flags;
        pref.edit().putLong("time_flags", time_flags1).apply();
    }

    public long getTime_capitals() {
        return pref.getLong("time_capitals", 0);
    }

    public void setTime_capitals(long time_capitals) {
        long time_capitals1 = getTime_capitals() + time_capitals;
        pref.edit().putLong("time_capitals", time_capitals1).apply();
    }

    public long getTime_2048() {
        return pref.getLong("time_2048", 0);
    }

    public void setTime_2048(long time_2048) {
        long time_20481 = getTime_2048() + time_2048;
        pref.edit().putLong("time_2048", time_20481).apply();
    }

    public long getTime_flappybird() {
        return pref.getLong("time_flappybird", 0);
    }

    public void setTime_flappybird(long time_flappybird) {
        long time_flappybird1 = getTime_flappybird() + time_flappybird;
        pref.edit().putLong("time_flappybird", time_flappybird1).apply();
    }

    public long getTime_xobig() {
        return pref.getLong("time_xobig", 0);
    }

    public void setTime_xobig(long time_xobig) {
        long time_xobig1 = getTime_xobig() + time_xobig;
        pref.edit().putLong("time_xobig", time_xobig1).apply();
    }

    public long getTime_knight() {
        return pref.getLong("time_knight", 0);
    }

    public void setTime_knight(long time_knight) {
        long time_knight1 = getTime_knight() + time_knight;
        pref.edit().putLong("time_knight", time_knight1).apply();
    }

    public long getTime_sudoku() {
        return pref.getLong("time_sudoku", 0);
    }

    public void setTime_sudoku(long time_sudoku) {
        long time_sudoku1 = getTime_sudoku() + time_sudoku;
        pref.edit().putLong("time_sudoku", time_sudoku1).apply();
    }

    public long getTime_dragon() {
        return pref.getLong("time_dragon", 0);
    }

    public void setTime_dragon(long time_dragon) {
        long time_dragon1 = getTime_dragon() + time_dragon;
        pref.edit().putLong("time_dragon", time_dragon1).apply();
    }

    public long getTime_longerline() {
        return pref.getLong("time_longerline", 0);
    }

    public void setTime_longerline(long time_longerline) {
        long time_longerline1 = getTime_longerline() + time_longerline;
        pref.edit().putLong("time_longerline", time_longerline1).apply();
    }

}
