// Alexander Becker
// Michelle Moazami

package com.example.myapplication.game_snake;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.game_DragonFlight.GamePanel;

import java.util.Random;

public class SnakeThread extends SurfaceView implements Runnable {
    public int roundsPlayed;

    //Bearbeitung für GameOverScreen
    private long gameOverTime;
    private Rect r = new Rect();
    private boolean screenDrawn;

    // snake game is thread that is being updated constantly
    private Thread thread = null;

    // reference to the Activity
    private Context context;

    // enum for direction of snake
    public enum Direction {UP, RIGHT, DOWN, LEFT}

    // initial direction is right
    private Direction moving = Direction.RIGHT;

    // screen size in pixel
    private int screenX;
    private int screenY;

    private int snakeLength;

    // position of snake's food
    private int foodX;
    private int foodY;

    // The size in pixels of a snake segment
    // we cannot use one pixel as snake-element... too small
    // therefore we define block elements with certain size
    private int blockElementSize;

    // define the number of blocks that shall be used as playground/field... here: 40 blocks
    private int numberBlocksWidth = 40;
    // will be calculated
    private int numberBlocksHeight;

    // control pausing between updates
    private long nextFrameTime;
    // Update the game certain times per second ... speed of the snake
    // 7 = easy   // 10 = medium   // 13 = hard
    private long FPS = 10;
    // 1000 milliseconds in a second
    private final long MILLIS_PER_SECOND = 1000;

    private int score;
    private int difficulty;

    // arrays that contain all elements of the snake
    private int[] snake_X_Position;
    private int[] snake_Y_Position;

    private int lengthToWin = 200;

    private volatile boolean isPlaying;
    private boolean gameOver;

    private Canvas canvas;
    // required for canvas
    private SurfaceHolder surfaceHolder;
    private Paint paint;


    public SnakeThread(Context context, Point size, int difficulty) {
        super(context);

        roundsPlayed = 0;

        switch (difficulty) {
            case 1:
                FPS = 10;
                this.difficulty = difficulty;
                break;
            case 2:
                FPS = 15;
                this.difficulty = difficulty;
                break;
            case 3:
                FPS = 20;
                this.difficulty = difficulty;
                break;
        }

        Constants.CURRENT_CONTEXT = context;
        context = context;

        screenX = size.x;
        // subtraction of 3 * numberBlocksWidth because of bottom button-bar (back, home, all tasks)
        screenY = size.y - (3 * numberBlocksWidth);

        // calculate size of block elements
        // for example: 800 pixel width and blocks of 40 pixels per block:
        // 800px / 40 px per block =  20 blocks
        blockElementSize = screenX / numberBlocksWidth;
        // now that we have the blocksize, we can calculate how many blocks fit in vertical direction (height)
        numberBlocksHeight = screenY / blockElementSize;

        // Initialize the drawing objects
        surfaceHolder = getHolder();
        paint = new Paint();

        snake_X_Position = new int[lengthToWin];
        snake_Y_Position = new int[lengthToWin];

        newGame();
    }

    @Override
    public void run() {

        while (isPlaying) {

            // Update [FPS] times a second (7: easy, 10: medium, 13: hard)
            if (updateRequired()) {
                update();
                draw();
            }

        }
    }

    public void pause() {
        isPlaying = false;
        try {
            thread.join();
        } catch (InterruptedException e) {

        }
    }

    public void resume() {
        isPlaying = true;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        //get x and y position of clickEvent
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        // calculate x/y positions into block elements size
        int xBlock = x / blockElementSize;
        int yBlock = y / blockElementSize;

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                if (gameOver && System.currentTimeMillis() - gameOverTime >= 1000) {
                    screenDrawn = false;
                    newGame();
                    gameOver = false;
                }
                break;
            // old movement-handling:
            case MotionEvent.ACTION_UP:

//                switch (moving) {
//                    case UP:
//                    case DOWN:
//                        // if snake is moving up or down, change direction to left or right
//                        // if click-position is on left hand side of snake's head, move left
//                        // if click-position is on right hand side of snake's head, move right
//                        moving = (xBlock < snake_X_Position[0]) ? Direction.LEFT : Direction.RIGHT;
//                        break;
//                    case LEFT:
//                    case RIGHT:
//                        // if snake is moving left or right, change direction to up or down
//                        // if click-position is above snake's head, move up
//                        // if click-position is under snake's head, move down
//                        moving = (yBlock < snake_Y_Position[0]) ? Direction.UP : Direction.DOWN;
//                        break;
//                }

                // another version of movement handling
                // x < screenX/2 means user clicked on left side, move left
                if (x < screenX / 2) {
                    switch (moving) {
                        case UP:
                            moving = Direction.LEFT;
                            break;
                        case LEFT:
                            moving = Direction.DOWN;
                            break;
                        case DOWN:
                            moving = Direction.RIGHT;
                            break;
                        case RIGHT:
                            moving = Direction.UP;
                            break;
                    }
                } else {
                    // move right
                    switch (moving) {
                        case UP:
                            moving = Direction.RIGHT;
                            break;
                        case RIGHT:
                            moving = Direction.DOWN;
                            break;
                        case DOWN:
                            moving = Direction.LEFT;
                            break;
                        case LEFT:
                            moving = Direction.UP;
                            break;
                    }
                }


        }
        return true;
    }


    public void newGame() {
        roundsPlayed++;
        gameOver = false;

        // Start with a single snake segment
        snakeLength = 1;
        snake_X_Position[0] = numberBlocksWidth / 2;
        snake_Y_Position[0] = numberBlocksHeight / 2;

        // create food at random position
        createFood();

        score = 0;

        // setup nextFrameTime so an update is triggered
        nextFrameTime = System.currentTimeMillis();
    }


    public void createFood() {
        Random random = new Random();
        foodX = random.nextInt(numberBlocksWidth - 1) + 1;
        foodY = random.nextInt(numberBlocksHeight - 1) + 1;
    }

    private void eatFood() {
        snakeLength++;
        createFood();
        score = score + 1;
    }

    private void moveSnake() {
        // move only body without head
        for (int i = snakeLength; i > 0; i--) {
            // start at last position and place it in position before
            snake_X_Position[i] = snake_X_Position[i - 1];
            snake_Y_Position[i] = snake_Y_Position[i - 1];
        }
        // head has no position before
        // head get position depending on users click position
        switch (moving) {
            case UP:
                snake_Y_Position[0]--;
                break;

            case RIGHT:
                snake_X_Position[0]++;
                break;

            case DOWN:
                snake_Y_Position[0]++;
                break;

            case LEFT:
                snake_X_Position[0]--;
                break;
        }
    }

    private boolean checkDeath() {

        // Has the snake died?
        boolean dead = false;

        // check for hitting the display edge
        if (snake_X_Position[0] == -1) {
            dead = true;
            showGameOverScreen();
        }
        if (snake_X_Position[0] >= numberBlocksWidth) {
            dead = true;
            showGameOverScreen();
        }
        if (snake_Y_Position[0] == -1) {
            dead = true;
            showGameOverScreen();
        }
        if (snake_Y_Position[0] == numberBlocksHeight) {
            dead = true;
            showGameOverScreen();
        }

        // check  snake has eaten itself
        // iterate through each element and check if element's position is like head's position
        for (int i = snakeLength - 1; i > 0; i--) {
            if ((i > 4) && (snake_X_Position[0] == snake_X_Position[i]) && (snake_Y_Position[0] == snake_Y_Position[i])) {
                dead = true;
                showGameOverScreen();
            }
        }


        return dead;
    }

    public void update() {
        if (!gameOver) {
            moveSnake();
        }
        // check if snake has eaten food
        if (snake_X_Position[0] == foodX && snake_Y_Position[0] == foodY) {
            eatFood();
        }

        if (checkDeath()) {

            // hier stehen geblieben, etwas überlegen

//            if(highscore >= getRecord()){
//                setRecord();
//                print("new record: " + highscore);
//            }
            gameOver = true;
            //thread.sleep(1000);

        }
    }

    private void showGameOverScreen() {
        // Wird nur ein mal ausgeführt
        if (!screenDrawn) {

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(SnakeThread.this.getContext());
            SharedPreferences.Editor ed = pref.edit();

            int highscore;
            if (difficulty == 1) highscore = pref.getInt("highscore_easy", 0);
            else if (difficulty == 2) highscore = pref.getInt("highscore_medium", 0);
            else highscore = pref.getInt("highscore_hard", 0);

            if (score > highscore && score > 0) {

                if (difficulty == 1) ed.putInt("highscore_easy", score).apply();
                else if (difficulty == 2) ed.putInt("highscore_medium", score).apply();
                else ed.putInt("highscore_hard", score).apply();

                Looper.prepare();

                // Put record into database
                String name = pref.getString("playername", "Username");
                String gamename = new Game_Helper().GAMENAME_SNAKE;
                Database_Helper database = new Database_Helper();
                Database_Score scoreEntry = new Database_Score(gamename, score, name, difficulty);
                database.addScoreToDatabase(scoreEntry);
            }


            canvas = surfaceHolder.lockCanvas();
            Paint paint2 = new Paint();
            paint2.setColor(Color.GRAY);
            Typeface plain = Typeface.createFromAsset(com.example.myapplication.game_snake.Constants.CURRENT_CONTEXT.getAssets(), "secular_one.ttf");
            paint2.setColor(Color.WHITE);
            paint2.setTypeface(plain);
            paint2.setTextSize(70);
            drawCenterText(canvas, paint2, "Highscore: " + highscore, Constants.SCREEN_HEIGHT / 9 * 3 + 200);
            drawCenterText(canvas, paint2,"Your Score: " + score, Constants.SCREEN_HEIGHT/9 * 3 + 300);


            paint2.setTextSize(80);
            drawCenterText(canvas, paint2, "Tap to play again!", Constants.SCREEN_HEIGHT/9 * 3 );
            surfaceHolder.unlockCanvasAndPost(canvas);
            screenDrawn = true;
        }
    }

    public void draw() {
        if (!gameOver) {
            // lock the canvas
            if (surfaceHolder.getSurface().isValid()) {
                canvas = surfaceHolder.lockCanvas();

                // fill the screen with blue
                canvas.drawColor(Color.argb(255, 26, 128, 182));

                // set the color of the paint to draw the snake white
                paint.setColor(Color.argb(255, 255, 255, 255));

                // scale the HUD text
                Paint p = new Paint();
                p.setTextSize(70);
                Typeface plain = Typeface.createFromAsset(com.example.myapplication.game_snake.Constants.CURRENT_CONTEXT.getAssets(), "secular_one.ttf");
                p.setTypeface(plain);
                p.setAlpha(80);
                drawCenterText(canvas, p, "" + score, Constants.SCREEN_HEIGHT / 9);

                // draw snake elements one by one
                for (int i = 0; i < snakeLength; i++) {
                    canvas.drawRect(snake_X_Position[i] * blockElementSize,
                            (snake_Y_Position[i] * blockElementSize),
                            (snake_X_Position[i] * blockElementSize) + blockElementSize,
                            (snake_Y_Position[i] * blockElementSize) + blockElementSize,
                            paint);
                }

                // set food color red
                paint.setColor(Color.argb(255, 255, 0, 0));

                // draw food
                canvas.drawRect(foodX * blockElementSize,
                        (foodY * blockElementSize),
                        (foodX * blockElementSize) + blockElementSize,
                        (foodY * blockElementSize) + blockElementSize,
                        paint);

                // unlock canvas and post graphics for this frame
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public boolean updateRequired() {
        // check if update has been triggered
        if (nextFrameTime <= System.currentTimeMillis()) {
            // set trigger for next frame update
            nextFrameTime = System.currentTimeMillis() + MILLIS_PER_SECOND / FPS;
            // true for updating
            return true;
        }
        return false;
    }

    private void print(String msg) {
        System.out.println(msg);
    }


    private void drawCenterText(Canvas canvas, Paint paint, String text, float yVal) {
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.getTextBounds(text, 0, text.length(), r);

        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y;
        if (yVal < 0) {
            y = cHeight / 2f + r.height() / 2f - r.bottom;
        } else {
            y = yVal;
        }
        canvas.drawText(text, x, y, paint);
    }

} // end of SnakeThread
