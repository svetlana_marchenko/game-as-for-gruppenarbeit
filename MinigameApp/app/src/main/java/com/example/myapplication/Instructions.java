package com.example.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Instructions extends AppCompatActivity {

    private ImageButton backToMenu;
    private ImageButton toSettings;
    Game game;

    private int currentTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        this.game = (Game) getIntent().getExtras().get("game");

        TextView gamename = findViewById(R.id.gamename);
        gamename.setText(game.getGameName() + " - Instructions");

        String text_2048 = "2048 is a game where you combine numbered tiles in order to gain a higher numbered tile. \n" +
                "You start with two tiles, the lowest possible number available is two. \nThen you will play by swiping upward, " +
                "downward or sideways in order to combine same numbered tiles to have a tile with the sum of the number on the two tiles. " +
                "Everytime you move the Tiles, one random Tile will be added to the board. \n\n" +
                "This means the board will fill up until you either reach 2048 points or dont have any space left to combine tiles.";

        String text_sweeper = "Minesweeper is a single-player logic puzzle game. The objective of the game is to clear a rectangular board containing hidden mines without detonating any of them. \n" +
                "\n" +
                "The cells of the board have three states: uncovered, covered and flagged. \n" +
                "Covered cell: blank and clickable\n" +
                "Uncovered cell: unclickable field containing Number or blank field (=\"0\"), indicating the quantity of mines diagonally or horizontally/vertically adjacent to it. \n" +
                "Flagged cell: marked by touching and holding the covered cell to indicate a potential mine location.\n" +
                "\n" +
                "The following example shows the logic behind the game. \"1\" has 1 mine next to it, \"2\" has 2 mines next to it and so on. ";

        String text_kniffel = "Rules : \n\n" +
                "1. Roll the dices by clicking 'THROW' n" +
                "2. Every player has three trys to fulfill a task " +
                "to save the dices click at one\n" +
                "3. For the next round/player click next \n" +
                "4. To enter a result click the corresponding field \n" +
                "5. If you reach 63 or more points in the top six" +
                " tasks you get 35 extra points \n\n" +
                "Counting: \n\n" +
                "Ones - The sum of the numbers of the rolled ones \n" +
                "Twos - The sum of the numbers of the rolled twos \n" +
                "Threes - The sum of the numbers of the rolled threes \n" +
                "Fours - The sum of the numbers of the rolled fours \n" +
                "Fives - The sum of the numbers of the rolled fives \n" +
                "Six' - The sum of the numbers of the rolled six' \n" +
                "Three/Four of a kind - The sum of the rolled numbers \n" +
                "Fullhouse - Three and 2 of a kind. Counts 25 \n" +
                "Sm straight - Four consecutive numbers. Counts 30 \n" +
                "Gr straight - Four consecutive numbers. Counts 40 \n" +
                "Yatzee - Five of a kind. Counts 50 \n" +
                "Chance - Every comination. Sum of numbers \n";

        String text_enigma = "Enigma is a numbers puzzlegame, where you have to order numbers in a ascending order with as few moves as possible.\n" +
                "You move a number to the empty spot by clicking on it. It is also possible to move multiple numbers at once," +
                " by clicking numbers that are further away from the empty field. It is not possible to move numbers diagonaly.\n\n" +
                "This is how the numbers look after completing the game:";

        String text_xobig = "XOBIG is a Strategie game for two players, X and O, who take turns marking the spaces in a grid. \n" +
                "The one who's turn it is selects a field by clicking it.\n" +
                "The player who succeeds in placing five of their marks in a diagonal, horizontal, or vertical row is the winner.";

        String text_snake = "Inspired by the original arcade game, the objective of this game is to maneuver a line without hitting either the edge of the screen or the line itself. Aiming for the highscore, you have to collect food with the head of the snake.\n" +
                "You want the snake to turn to its right? Tap the right side of your screen. For turning left, tap the left side.\n" +
                "The snake moves on his own axis, so sometimes you have to think twice.";

        TextView description = findViewById(R.id.description);
        ImageView picture = findViewById(R.id.image);

        if (game.getGameName().equals(new Game_Helper().GAMENAME_2048)){
            description.setText(text_2048);
            picture.setImageResource(R.drawable.instructions_2048);
        }

        if (game.getGameName().equals(new Game_Helper().GAMENAME_MINESWEEPER)){
            description.setText(text_sweeper);
            picture.setImageResource(R.drawable.game_minesweeper_example);
        }

        if (game.getGameName().equals(new Game_Helper().GAMENAME_KNIFFEL)){
            description.setText(text_kniffel);
            picture.setVisibility(View.GONE);
        }

        if (game.getGameName().equals(new Game_Helper().GAMENAME_PUZZLE)){
            description.setText(text_enigma);
            picture.setImageResource(R.drawable.instructions_puzzle);
        }

        if (game.getGameName().equals(new Game_Helper().GAMENAME_XOBIG)){
            description.setText(text_xobig);
            picture.setImageResource(R.drawable.instructions_xobig);
        }

        if (game.getGameName().equals(new Game_Helper().GAMENAME_SNAKE)){
            description.setText(text_snake);
            picture.setImageResource(R.drawable.instructions_xobig);
        }

    }

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}
