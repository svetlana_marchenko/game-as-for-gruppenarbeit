// Alexander Becker

package com.example.myapplication.game_minesweeper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;

import android.graphics.drawable.Drawable;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Game_Minesweeper extends AppCompatActivity {

    long startTime;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    // 16x20-matrix: 25 pixel per field/button
    private final int             COLUMN        = 15;
    private final int             ROW           = 20;
    // 2-dimensional array that contains the values of each field
    private       int[][]         fieldValues   = new int[ROW][COLUMN];
    // 2-dimensional array that contains all the clickable buttons, that represent the fields
    private       ImageButton[][] buttons       = new ImageButton[ROW][COLUMN];
    private       Button          buttonNewGame;
    // 2-dimensional array that contains all the ID's of all buttons (to communicate with the buttons)
    private final int             BUTTON_ID[][] =
            {{R.id.button_00_00, R.id.button_00_01, R.id.button_00_02, R.id.button_00_03, R.id.button_00_04, R.id.button_00_05, R.id.button_00_06, R.id.button_00_07, R.id.button_00_08, R.id.button_00_09, R.id.button_00_10, R.id.button_00_11, R.id.button_00_12, R.id.button_00_13, R.id.button_00_14},
                    {R.id.button_01_00, R.id.button_01_01, R.id.button_01_02, R.id.button_01_03, R.id.button_01_04, R.id.button_01_05, R.id.button_01_06, R.id.button_01_07, R.id.button_01_08, R.id.button_01_09, R.id.button_01_10, R.id.button_01_11, R.id.button_01_12, R.id.button_01_13, R.id.button_01_14},
                    {R.id.button_02_00, R.id.button_02_01, R.id.button_02_02, R.id.button_02_03, R.id.button_02_04, R.id.button_02_05, R.id.button_02_06, R.id.button_02_07, R.id.button_02_08, R.id.button_02_09, R.id.button_02_10, R.id.button_02_11, R.id.button_02_12, R.id.button_02_13, R.id.button_02_14},
                    {R.id.button_03_00, R.id.button_03_01, R.id.button_03_02, R.id.button_03_03, R.id.button_03_04, R.id.button_03_05, R.id.button_03_06, R.id.button_03_07, R.id.button_03_08, R.id.button_03_09, R.id.button_03_10, R.id.button_03_11, R.id.button_03_12, R.id.button_03_13, R.id.button_03_14},
                    {R.id.button_04_00, R.id.button_04_01, R.id.button_04_02, R.id.button_04_03, R.id.button_04_04, R.id.button_04_05, R.id.button_04_06, R.id.button_04_07, R.id.button_04_08, R.id.button_04_09, R.id.button_04_10, R.id.button_04_11, R.id.button_04_12, R.id.button_04_13, R.id.button_04_14},
                    {R.id.button_05_00, R.id.button_05_01, R.id.button_05_02, R.id.button_05_03, R.id.button_05_04, R.id.button_05_05, R.id.button_05_06, R.id.button_05_07, R.id.button_05_08, R.id.button_05_09, R.id.button_05_10, R.id.button_05_11, R.id.button_05_12, R.id.button_05_13, R.id.button_05_14},
                    {R.id.button_06_00, R.id.button_06_01, R.id.button_06_02, R.id.button_06_03, R.id.button_06_04, R.id.button_06_05, R.id.button_06_06, R.id.button_06_07, R.id.button_06_08, R.id.button_06_09, R.id.button_06_10, R.id.button_06_11, R.id.button_06_12, R.id.button_06_13, R.id.button_06_14},
                    {R.id.button_07_00, R.id.button_07_01, R.id.button_07_02, R.id.button_07_03, R.id.button_07_04, R.id.button_07_05, R.id.button_07_06, R.id.button_07_07, R.id.button_07_08, R.id.button_07_09, R.id.button_07_10, R.id.button_07_11, R.id.button_07_12, R.id.button_07_13, R.id.button_07_14},
                    {R.id.button_08_00, R.id.button_08_01, R.id.button_08_02, R.id.button_08_03, R.id.button_08_04, R.id.button_08_05, R.id.button_08_06, R.id.button_08_07, R.id.button_08_08, R.id.button_08_09, R.id.button_08_10, R.id.button_08_11, R.id.button_08_12, R.id.button_08_13, R.id.button_08_14},
                    {R.id.button_09_00, R.id.button_09_01, R.id.button_09_02, R.id.button_09_03, R.id.button_09_04, R.id.button_09_05, R.id.button_09_06, R.id.button_09_07, R.id.button_09_08, R.id.button_09_09, R.id.button_09_10, R.id.button_09_11, R.id.button_09_12, R.id.button_09_13, R.id.button_09_14},
                    {R.id.button_10_00, R.id.button_10_01, R.id.button_10_02, R.id.button_10_03, R.id.button_10_04, R.id.button_10_05, R.id.button_10_06, R.id.button_10_07, R.id.button_10_08, R.id.button_10_09, R.id.button_10_10, R.id.button_10_11, R.id.button_10_12, R.id.button_10_13, R.id.button_10_14},
                    {R.id.button_11_00, R.id.button_11_01, R.id.button_11_02, R.id.button_11_03, R.id.button_11_04, R.id.button_11_05, R.id.button_11_06, R.id.button_11_07, R.id.button_11_08, R.id.button_11_09, R.id.button_11_10, R.id.button_11_11, R.id.button_11_12, R.id.button_11_13, R.id.button_11_14},
                    {R.id.button_12_00, R.id.button_12_01, R.id.button_12_02, R.id.button_12_03, R.id.button_12_04, R.id.button_12_05, R.id.button_12_06, R.id.button_12_07, R.id.button_12_08, R.id.button_12_09, R.id.button_12_10, R.id.button_12_11, R.id.button_12_12, R.id.button_12_13, R.id.button_12_14},
                    {R.id.button_13_00, R.id.button_13_01, R.id.button_13_02, R.id.button_13_03, R.id.button_13_04, R.id.button_13_05, R.id.button_13_06, R.id.button_13_07, R.id.button_13_08, R.id.button_13_09, R.id.button_13_10, R.id.button_13_11, R.id.button_13_12, R.id.button_13_13, R.id.button_13_14},
                    {R.id.button_14_00, R.id.button_14_01, R.id.button_14_02, R.id.button_14_03, R.id.button_14_04, R.id.button_14_05, R.id.button_14_06, R.id.button_14_07, R.id.button_14_08, R.id.button_14_09, R.id.button_14_10, R.id.button_14_11, R.id.button_14_12, R.id.button_14_13, R.id.button_14_14},
                    {R.id.button_15_00, R.id.button_15_01, R.id.button_15_02, R.id.button_15_03, R.id.button_15_04, R.id.button_15_05, R.id.button_15_06, R.id.button_15_07, R.id.button_15_08, R.id.button_15_09, R.id.button_15_10, R.id.button_15_11, R.id.button_15_12, R.id.button_15_13, R.id.button_15_14},
                    {R.id.button_16_00, R.id.button_16_01, R.id.button_16_02, R.id.button_16_03, R.id.button_16_04, R.id.button_16_05, R.id.button_16_06, R.id.button_16_07, R.id.button_16_08, R.id.button_16_09, R.id.button_16_10, R.id.button_16_11, R.id.button_16_12, R.id.button_16_13, R.id.button_16_14},
                    {R.id.button_17_00, R.id.button_17_01, R.id.button_17_02, R.id.button_17_03, R.id.button_17_04, R.id.button_17_05, R.id.button_17_06, R.id.button_17_07, R.id.button_17_08, R.id.button_17_09, R.id.button_17_10, R.id.button_17_11, R.id.button_17_12, R.id.button_17_13, R.id.button_17_14},
                    {R.id.button_18_00, R.id.button_18_01, R.id.button_18_02, R.id.button_18_03, R.id.button_18_04, R.id.button_18_05, R.id.button_18_06, R.id.button_18_07, R.id.button_18_08, R.id.button_18_09, R.id.button_18_10, R.id.button_18_11, R.id.button_18_12, R.id.button_18_13, R.id.button_18_14},
                    {R.id.button_19_00, R.id.button_19_01, R.id.button_19_02, R.id.button_19_03, R.id.button_19_04, R.id.button_19_05, R.id.button_19_06, R.id.button_19_07, R.id.button_19_08, R.id.button_19_09, R.id.button_19_10, R.id.button_19_11, R.id.button_19_12, R.id.button_19_13, R.id.button_19_14}};

    // -10 is dummy value; VALUE_BOMB variable has to be of type integer, because all other possible values (0...8) are integers.
    // defining VALUE_BOMB as boolean would cause exception since get/setFieldValue and other methods operate with integers
    private final int VALUE_BOMB = -10;

    // amount of bombs that are placed across the field
    private final int EASY   = 15;
    private final int MEDIUM = 30;
    private final int HARD   = 50;

    private int     difficulty;

    private  boolean gameOver;
    private  String  currentTime;
//    private  String  highscore;
    private int score;

    private Timer timer;
    private TextView tvTimer;
    private TextView tvYourScore;

    private int currentTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.startTime = System.currentTimeMillis();

        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minesweeper);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        buttonNewGame = findViewById(R.id.buttonNewGame);
        buttonNewGame.setOnClickListener(onClickListener);

        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvYourScore = (TextView) findViewById(R.id.tvYourScore);

//        difficulty = MEDIUM;
        Bundle b = getIntent().getExtras();
        int diff = b.getInt("difficulty");
        if(diff == 1)       difficulty = EASY;
        else if(diff == 2)  difficulty = MEDIUM;
        else                difficulty = HARD;

        gameOver = false;
        newGame();

    } // end of onCreate

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // setting ClickListener for each button in the field
            for (int i = 0; i < ROW; i++) {
                for (int j = 0; j < COLUMN; j++) {
                    if (v.getId() == BUTTON_ID[i][j]) {
                        handleClick(i, j);
                    }
                }
            }

            if (v.getId() == R.id.buttonNewGame) {
                newGame();
            }
        } // onClick
    }; // View.OnClickListener

    private void newGame() {

        ((GlobalVariables) this.getApplication()).addOneRound();

        gameOver = false;
        tvYourScore.setText("0");
        score = 0;
        currentTime = "00:00";
        // reset timer, otherwise a second timer will be created, parallel to the first one
        if(timer != null){
            timer.cancel();
        }
        startTimer();

        // initialize all fields:
        // setting clickListener, longClickListener (pressing button for longer period of time)
        for (int i = 0; i < ROW; i++) {
            for (int j = 0; j < COLUMN; j++) {
                buttons[i][j] = findViewById(BUTTON_ID[i][j]);
                buttons[i][j].setOnClickListener(onClickListener);

                //setting longClickListener for placing and removing flags
                int finalJ = j;
                int finalI = i;
                buttons[i][j].setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        setFlag(finalI, finalJ);
                        return true;
                    }
                });
                // initializing all fields with grey image, as uncovered/closed/unknown
                buttons[i][j].setImageResource(R.drawable.minesweeper_unknown);
            }
        }

        // reset all values and counter amount of bombs
        // without resetting you get values from the past game. these values are no longer actual and cause logical issues
        resetValues();
        int amountOfBombs = 0;

        amountOfBombs = difficulty;

        generateBombs(amountOfBombs);
        calculateValues();
    } // newGame

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_minesweeper(millis); // Hier setTime_deinSpiel

    }


    private void startTimer() {
//        Timer timer = new Timer();    //declare the timer
        timer = new Timer();    //declare the timer
        timer.scheduleAtFixedRate(new TimerTask() { //Set the schedule function and rate
            int minutes = 0;
            int seconds = 0;
//            TextView tvTimer = (TextView) findViewById(R.id.tvTimer);

            @Override
            public void run() {
                runOnUiThread(() -> {
//                    if (gameOver) {
////                        highscore = minutes + ":" + seconds;
////                        score--;
////                        tvYourScore.setText(String.valueOf(score));
//                    }
                    tvTimer.setText(String.format("%02d:%02d", minutes % 60, seconds));
                    tvYourScore.setText(String.valueOf(score));
                    seconds++;
                    score++;
                    if (seconds == 60) {
                        seconds = 0;
                        minutes++;
                    }
                });
            }
        }, 0L, 1000L);
    } // startTimer


    private void setFlag(int row, int column) {
        // used by longer clicking on button
        // if longer clicking on unknown, you get a flag
        // if longer clicking on flag, you turn flag into unknown
        Drawable drawable = buttons[row][column].getDrawable();
        if (drawable.getConstantState().equals(getResources().getDrawable(R.drawable.minesweeper_unknown).getConstantState())) {
            buttons[row][column].setImageResource(R.drawable.minesweeper_flag);
        } else if (drawable.getConstantState().equals(getResources().getDrawable(R.drawable.minesweeper_flag).getConstantState())) {
            buttons[row][column].setImageResource(R.drawable.minesweeper_unknown);
        }
    }

    private void generateBombs(int amountOfBombs) {
        // generating randomly bombs
        int    bombCounter = amountOfBombs;
        Random ran         = new Random();
        int    row;
        int    column;

        // while block will be executed just as much, as many bombs are to be set (EASY/MEDIUM/HARD)
        while (bombCounter != 0) {
//            print("bombCounter: " + bombCounter);
            row = ran.nextInt(ROW);
//            print("row: " + row);
            column = ran.nextInt(COLUMN);
//            print("column: " + column);
            // if randomly generated field already has a bomb placed in it, continue (= execute this while-block again without counting down)
            // otherwise you replace a bomb with a bomb, you count the bombCounter down and have effectivly no bomb created
            if (getFieldValue(row, column) == VALUE_BOMB) continue;
            setFieldValue(row, column, VALUE_BOMB);
            bombCounter--;
        }
    }

    private void handleClick(int row, int column) {

        int fieldValue = getFieldValue(row, column);
        // if the field value is 0, you automatically open all 0-fields that are direct neighbours
        if (fieldValue == 0) {
            findNearbyZeros_recursive(row, column);
        }

        // check whether the clicked field has a flag. if so, ignore the click because flags are dealt with longer clicks
        Drawable drawable = buttons[row][column].getDrawable();
        if (drawable.getConstantState().equals(getResources().getDrawable(R.drawable.minesweeper_flag).getConstantState())) {
            // uncomment next line, if normal (short) click shall turn flag into unknown field (this is not the case by default)
//            buttons[row][column].setImageResource(R.drawable.minesweeper_unknown);
            return;
        }

        // open clicked field
        openField(row, column);

        // if clicked field contains a bomb --> game over
        // show all bombs, set correct bomb-images, deactivate clickListener so actions are not possible anymore
        if (fieldValue == VALUE_BOMB) {
            showALlBombs();
            buttons[row][column].setImageResource(R.drawable.minewseeper_bomb_explode);
            for (int i = 0; i < ROW; i++) {
                for (int j = 0; j < COLUMN; j++) {
                    timer.cancel();
                    buttons[i][j] = findViewById(BUTTON_ID[i][j]);
                    buttons[i][j].setOnClickListener(null);
                }
            }

            // if time counter or click counter will be included, state the time or amount of clicks here
        }

        // if you are here, you have opened the value(s), next to-do: check if the game is finished
        checkFinished();
    }

    private void findNearbyZeros_recursive(int row, int column) {
        // this is a recursive mothod, that calls itself everytime it finds a zero as his neighbour

//        print("entering recursion");
        // if row/column is out of index/out of field, stop... otherwise exection
        if (row < 0 || row >= ROW || column < 0 || column >= COLUMN)
            return;
        int fieldValue = getFieldValue(row, column);
        // if method found a first value that is not zero: open it and stop recursion
        if (fieldValue != 0) {
            openField(row, column);
            return;
        }

        // only check unknown fields. if method finds an already opened field, stop
        Drawable drawable = buttons[row][column].getDrawable();
        if (!drawable.getConstantState().equals(getResources().getDrawable(R.drawable.minesweeper_unknown).getConstantState()))
            return;

        // if the method hasn't stopped up to this point, the field is not out of index, is not between 1...8 and is still covered/unknown ---> open it up as zero
        buttons[row][column].setImageResource(R.drawable.minewseeper_0);

        // recursive call on neighboured fields
        findNearbyZeros_recursive(row - 1, column);     // recursion on upper field
        findNearbyZeros_recursive(row + 1, column);     // recursion on bottom field
        findNearbyZeros_recursive(row, column - 1);  // recursion on left field
        findNearbyZeros_recursive(row, column + 1);  // recursion on right field

        return;
    }

    private void openField(int row, int column) {
        // open number values 1...8
        int fieldValue = getFieldValue(row, column);

        if (fieldValue == 1)
            buttons[row][column].setImageResource(R.drawable.minewseeper_1);
        if (fieldValue == 2)
            buttons[row][column].setImageResource(R.drawable.minewseeper_2);
        if (fieldValue == 3)
            buttons[row][column].setImageResource(R.drawable.minewseeper_3);
        if (fieldValue == 4)
            buttons[row][column].setImageResource(R.drawable.minewseeper_4);
        if (fieldValue == 5)
            buttons[row][column].setImageResource(R.drawable.minewseeper_5);
        if (fieldValue == 6)
            buttons[row][column].setImageResource(R.drawable.minewseeper_6);
        if (fieldValue == 7)
            buttons[row][column].setImageResource(R.drawable.minewseeper_7);
        if (fieldValue == 8)
            buttons[row][column].setImageResource(R.drawable.minewseeper_8);
    }

    private void checkFinished() {
        // check for each field: if it is a value betwenn 0...8 (all values except of bombs) and still covered, stop checkFinished since there is at least one field value that is still covered
        for (int r = 0; r < ROW; r++) {
            for (int c = 0; c < COLUMN; c++) {
                int      fieldValue = getFieldValue(r, c);
                Drawable drawable   = buttons[r][c].getDrawable();
                // value between 0 and 8 AND still covered means that there still is a field to uncover = not finished ---> return;
                if (fieldValue >= 0 && fieldValue <= 8 && drawable.getConstantState().equals(getResources().getDrawable(R.drawable.minesweeper_unknown).getConstantState())) {
//                    print("r: " + r + " c: " + c + " still uncovered, game not over yet");
                    return;
                }
            }
        }
        // if this method hasn't been returned until this point, all values except the bombs have been successfully uncovered ---> win
//        print("all fields with values != bomb have been uncovered... you won");

        // set all bombs as smileys and deactivate all clickListeners
        for (int r = 0; r < ROW; r++) {
            for (int c = 0; c < COLUMN; c++) {
                if (getFieldValue(r, c) == VALUE_BOMB)
                    buttons[r][c].setImageResource(R.drawable.minewseeper_smiley);
                buttons[r][c].setOnClickListener(null);
            }
        }

        timer.cancel();
        if(score < getRecord()){
            setRecord();
        }


    }

    private void calculateValues() {
        // this method goes through each field and calculates the amount of bombs as neighbours
        // for example: field(4,4) has 3 bombs as his direct neighbours: set the field value as 3
        // direct neighbours are field in the horizontal (left, right), vertical (up, down) and diagonal (upper left, upper right, bottom left, bottom right)

        int bombCounter = 0;

        // calculate the field values of each fields except the ones on the first row, last row, first column, last column (those need another algorithm)
        for (int r = 1; r < ROW - 1; r++) {
            for (int c = 1; c < COLUMN - 1; c++) {
                bombCounter = 0;
                // if the checked field contains a bomb, no calculation of the field value is needed, since VALUE_BOMB is his actual value despite of his neighbours
                if (getFieldValue(r, c) == VALUE_BOMB) continue;
                if (getFieldValue(r - 1, c - 1) == VALUE_BOMB)
                    bombCounter++;    // upper left from current field
                if (getFieldValue(r - 1, c + 0) == VALUE_BOMB) bombCounter++;    // upper
                if (getFieldValue(r - 1, c + 1) == VALUE_BOMB) bombCounter++;    // upper right
                if (getFieldValue(r + 0, c - 1) == VALUE_BOMB) bombCounter++;    // left
                if (getFieldValue(r + 0, c + 1) == VALUE_BOMB) bombCounter++;    // right
                if (getFieldValue(r + 1, c - 1) == VALUE_BOMB) bombCounter++;    // bottom left
                if (getFieldValue(r + 1, c + 0) == VALUE_BOMB) bombCounter++;    // bottom
                if (getFieldValue(r + 1, c + 1) == VALUE_BOMB) bombCounter++;    // bottom right

//                print("bombCounter: " + bombCounter);
                setFieldValue(r, c, bombCounter);
//                debugShowValues(r,c);
            }
        }

        // the four corners need an extra algorithm, because they have only three direct neighbours. any other handling would cause an out-of-bound-exception

        // check single field: upper left corner
        if (getFieldValue(0, 0) != VALUE_BOMB) {
            bombCounter = 0;
            if (getFieldValue(0, 1) == VALUE_BOMB) bombCounter++;    // right from current field
            if (getFieldValue(1, 0) == VALUE_BOMB) bombCounter++;    // bottom
            if (getFieldValue(1, 1) == VALUE_BOMB) bombCounter++;    // bottom right
            print("bombCounter: " + bombCounter);
            setFieldValue(0, 0, bombCounter);

//            debugShowValues(0,0);
        }

        // check single field: upper right corner
        if (getFieldValue(0, COLUMN - 1) != VALUE_BOMB) {
            bombCounter = 0;
            if (getFieldValue(0, COLUMN - 2) == VALUE_BOMB)
                bombCounter++;    // left from current field
            if (getFieldValue(1, COLUMN - 2) == VALUE_BOMB) bombCounter++;    // left bottom
            if (getFieldValue(1, COLUMN - 1) == VALUE_BOMB) bombCounter++;           // bottom
            print("bombCounter: " + bombCounter);
            setFieldValue(0, COLUMN - 1, bombCounter);
        }

        // check single field: bottom left corner
        if (getFieldValue(ROW - 1, 0) != VALUE_BOMB) {
            bombCounter = 0;
            if (getFieldValue(ROW - 2, 0) == VALUE_BOMB)
                bombCounter++;    // right from current field
            if (getFieldValue(ROW - 2, 1) == VALUE_BOMB) bombCounter++;    // bottom
            if (getFieldValue(ROW - 1, 1) == VALUE_BOMB) bombCounter++;          // bottom right
            print("bombCounter: " + bombCounter);
            setFieldValue(ROW - 1, 0, bombCounter);

//            debugShowValues(ROW - 1, 0);
        }


        // check single field: bottom right corner
        if (getFieldValue(ROW - 1, COLUMN - 1) != VALUE_BOMB) {
            bombCounter = 0;
            if (getFieldValue(ROW - 1, COLUMN - 2) == VALUE_BOMB)
                bombCounter++;         // left from current field
            if (getFieldValue(ROW - 2, COLUMN - 2) == VALUE_BOMB) bombCounter++;  // upper left
            if (getFieldValue(ROW - 2, COLUMN - 1) == VALUE_BOMB) bombCounter++;         // upper
            print("bombCounter: " + bombCounter);
            setFieldValue(ROW - 1, COLUMN - 1, bombCounter);

//            debugShowValues(ROW - 1, COLUMN - 1);
        }

        // check values in first column (c=0)
        for (int r = 1; r < ROW - 1; r++) {
            bombCounter = 0;
            if (getFieldValue(r, 0) == VALUE_BOMB) continue;
            if (getFieldValue(r - 1, 0) == VALUE_BOMB) bombCounter++;    // upper from current field
            if (getFieldValue(r + 1, 0) == VALUE_BOMB) bombCounter++;    // bottom
            if (getFieldValue(r - 1, 1) == VALUE_BOMB) bombCounter++;    // upper right
            if (getFieldValue(r + 0, 1) == VALUE_BOMB) bombCounter++;    // right
            if (getFieldValue(r + 1, 1) == VALUE_BOMB) bombCounter++;    // bottom right

            print("bombCounter: " + bombCounter);
            setFieldValue(r, 0, bombCounter);

//            debugShowValues(r, 0);
        }

        // check values in last column (c=COLUMN)
        for (int r = 1; r < ROW - 1; r++) {
            bombCounter = 0;
            if (getFieldValue(r, COLUMN - 1) == VALUE_BOMB) continue;
            if (getFieldValue(r - 1, COLUMN - 1) == VALUE_BOMB)
                bombCounter++;           // upper from current field
            if (getFieldValue(r + 1, COLUMN - 1) == VALUE_BOMB) bombCounter++;           // bottom
            if (getFieldValue(r - 1, COLUMN - 2) == VALUE_BOMB) bombCounter++;    // upper left
            if (getFieldValue(r + 0, COLUMN - 2) == VALUE_BOMB) bombCounter++;    // left
            if (getFieldValue(r + 1, COLUMN - 2) == VALUE_BOMB) bombCounter++;    // bottom left

            print("bombCounter: " + bombCounter);
            setFieldValue(r, COLUMN - 1, bombCounter);

//            debugShowValues(r, COLUMN - 1);
        }

        // check values in first row (r=0)
        for (int c = 1; c < COLUMN - 1; c++) {
            bombCounter = 0;
            if (getFieldValue(0, c) == VALUE_BOMB) continue;
            if (getFieldValue(0, c - 1) == VALUE_BOMB) bombCounter++;    // left from current field
            if (getFieldValue(0, c + 1) == VALUE_BOMB) bombCounter++;    // right
            if (getFieldValue(1, c - 1) == VALUE_BOMB) bombCounter++;    // bottom left
            if (getFieldValue(1, c + 0) == VALUE_BOMB) bombCounter++;    // bottom
            if (getFieldValue(1, c + 1) == VALUE_BOMB) bombCounter++;    // bottom right

            print("bombCounter: " + bombCounter);
            setFieldValue(0, c, bombCounter);

//            debugShowValues(0, c);
        }

        // check values in last row (r=ROW)
        for (int c = 1; c < COLUMN - 1; c++) {
            bombCounter = 0;
            if (getFieldValue(ROW - 1, c) == VALUE_BOMB) continue;
            if (getFieldValue(ROW - 1, c - 1) == VALUE_BOMB)
                bombCounter++;    // left from current field
            if (getFieldValue(ROW - 1, c + 1) == VALUE_BOMB) bombCounter++;    // right
            if (getFieldValue(ROW - 2, c - 1) == VALUE_BOMB) bombCounter++;    // upper left
            if (getFieldValue(ROW - 2, c + 0) == VALUE_BOMB) bombCounter++;    // upper
            if (getFieldValue(ROW - 2, c + 1) == VALUE_BOMB) bombCounter++;    // upper right

            print("bombCounter: " + bombCounter);
            setFieldValue(ROW - 1, c, bombCounter);

//            debugShowValues(ROW - 1, c);
        }
//        debugShowValues();
    } // end: calculateValues

    private void showALlBombs() {
        for (int r = 0; r < ROW; r++) {
            for (int c = 0; c < COLUMN; c++) {
                if (getFieldValue(r, c) == VALUE_BOMB)
                    buttons[r][c].setImageResource(R.drawable.minewseeper_bomb);
            }
        }
    }

    private void resetValues() {
        for (int i = 0; i < ROW; i++) {
            for (int j = 0; j < COLUMN; j++) {
                setFieldValue(i, j, 0);
            }
        }
    }

    private void debugShowValues() {
        // helper method for development and debugging
        for (int r = 0; r < ROW; r++) {
            for (int c = 0; c < COLUMN; c++) {
                int bombCounter = getFieldValue(r, c);
                if (bombCounter == 0) buttons[r][c].setImageResource(R.drawable.minewseeper_0);
                if (bombCounter == 1) buttons[r][c].setImageResource(R.drawable.minewseeper_1);
                if (bombCounter == 2) buttons[r][c].setImageResource(R.drawable.minewseeper_2);
                if (bombCounter == 3) buttons[r][c].setImageResource(R.drawable.minewseeper_3);
                if (bombCounter == 4) buttons[r][c].setImageResource(R.drawable.minewseeper_4);
                if (bombCounter == 5) buttons[r][c].setImageResource(R.drawable.minewseeper_5);
                if (bombCounter == 6) buttons[r][c].setImageResource(R.drawable.minewseeper_6);
                if (bombCounter == 7) buttons[r][c].setImageResource(R.drawable.minewseeper_7);
                if (bombCounter == 8) buttons[r][c].setImageResource(R.drawable.minewseeper_8);
            }
        }
    }

    public int getRecord() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPref.getInt("Minesweeper", 0);
    } // getRecord


    private void setRecord() {
        SharedPreferences        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor     = sharedPref.edit();
        editor.putInt("Minesweeper", score);
        editor.commit();

        // Put record into database
        Bundle b = getIntent().getExtras();
        int diff = b.getInt("difficulty");
        String          name       = sharedPref.getString("playername", "Username");
        String          gamename   = new Game_Helper().GAMENAME_MINESWEEPER;
        Database_Helper database   = new Database_Helper();
        Database_Score  scoreEntry = new Database_Score(gamename, -score, name, diff);
        database.addScoreToDatabase(scoreEntry);
    } // setRecord


    public int getFieldValue(int i, int j) {
        return fieldValues[i][j];
    }

    public void setFieldValue(int i, int j, int value) {
        fieldValues[i][j] = value;
    }

    private void print(String msg) {
        System.out.println(msg);
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}
