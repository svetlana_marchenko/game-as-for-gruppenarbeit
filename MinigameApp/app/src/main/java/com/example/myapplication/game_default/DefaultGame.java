package com.example.myapplication.game_default;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.Settings;

public class DefaultGame extends AppCompatActivity {

    private ImageButton backToMenu;
    private ImageButton toSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Get saved Theme out of Shared Preferances
        SharedPreferences theme = getSharedPreferences("theme", 0);

        switch (theme.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                break;
            case 2:
                setTheme(R.style.green_theme);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                break;
            default:
                setTheme(R.style.blue_theme);
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default); // <-- Hier eigene Activity eintragen !!!

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}
