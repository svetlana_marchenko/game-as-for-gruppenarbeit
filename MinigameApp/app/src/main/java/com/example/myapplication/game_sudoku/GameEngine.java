package com.example.myapplication.game_sudoku;

import android.content.Context;

import com.example.myapplication.game_sudoku.view.sudokugrid.GameGrid;

public class GameEngine {

    private static GameEngine instance;

    private GameGrid grid = null;

    private Game_Sudoku activity = new Game_Sudoku();

    private int selectedPosX = -1, selectedPosY = -1;

    private GameEngine(){}

    public static GameEngine getInstance(){
        if( instance == null ){
            instance = new GameEngine();
        }
        return instance;
    }

    // creates the grid
    public void createGrid( Context context){
        Game_Sudoku activity = (Game_Sudoku) context;
        int[][] Sudoku = SudokuGenerator.getInstance().generateGrid();
        Sudoku = SudokuGenerator.getInstance().removeElements(Sudoku, activity.getDifficulty());
        grid = new GameGrid(context);
        grid.setGrid(Sudoku);
    }

    public void getDifficulty(Game_Sudoku activity) {
        activity.getBtn_easy();
    }

    public GameGrid getGrid(){
        return grid;
    }

    public void setSelectedPosition( int x , int y ){
        selectedPosX = x;
        selectedPosY = y;
    }

    public void setNumber( int number ){
        if( selectedPosX != -1 && selectedPosY != -1 ){
            grid.setItem(selectedPosX,selectedPosY,number);
        }
        grid.checkGame();
    }
}