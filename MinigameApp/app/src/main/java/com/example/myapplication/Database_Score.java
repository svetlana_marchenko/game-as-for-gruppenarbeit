package com.example.myapplication;

public class Database_Score {

    private String game;
    private int score;
    private String username;
    private int difficulty;

    public Database_Score(String game, int score, String username, int difficulty) {
        this.game = game;
        this.score = -score;
        this.username = username.trim().toLowerCase();
        this.difficulty = difficulty;
    }

    //public Database_Score(String game, String score, String username, int difficulty) {
    //    this.game = game;
    //    this.score = score;
    //    this.username = username;
    //    this.difficulty = difficulty;
    //}

    public Database_Score() {
        this.game = "No gamename set!";
        this.score = -999;
        this.username = "No username set!";
        this.difficulty = 1;
    }

    // Getter-methods
    public int getScore() {
        return score;
    }

    public String getGame() {
        return game;
    }

    public String getUsername() {
        return username;
    }

    public int getDifficulty() {
        return difficulty;
    }

    // Setter-methods
    public void setGame(String game) {
        this.game = game;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }
}
