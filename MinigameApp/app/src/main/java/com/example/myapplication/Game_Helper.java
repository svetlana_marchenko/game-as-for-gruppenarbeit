package com.example.myapplication;

public class Game_Helper {

    public final String GAMENAME_SUDUKO = "Sudoku";
    public final String GAMENAME_GOSPRINGER = "Knight";
    public final String GAMENAME_LONGERLINE = "Lines";
    public final String GAMENAME_XOBIG = "Tic Tac";
    public final String GAMENAME_PUZZLE = "Enigma";
    public final String GAMENAME_KNIFFEL = "Yahtzee";
    public final String GAMENAME_DRAGONFLIGHT = "Dragonfly";
    public final String GAMENAME_FLAPPYBIRD = "Hatty Bird";
    public final String GAMENAME_2048 = "2048";
    public final String GAMENAME_MINESWEEPER = "Sweeper";
    public final String GAMENAME_CAPITALS = "Capitals";
    public final String GAMENAME_FLAGS = "Flags";
    public final String GAMENAME_SNAKE = "Snake";

}