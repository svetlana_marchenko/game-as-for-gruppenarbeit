package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class GameConfig extends AppCompatActivity {

    private ImageButton backToMenu;
    private ImageButton toSettings;
    private ImageButton instructions;
    private Game game;

    private int currentTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        changeTheme();

        super.onCreate(savedInstanceState);
        game = (Game) getIntent().getExtras().get("game");
        int difficulty = game.getDifficulty();

        if (difficulty == 1) setContentView(R.layout.activity_game_config_easy);
        else if (difficulty == 2) setContentView(R.layout.activity_game_config_medium);
        else setContentView(R.layout.activity_game_config_hard);

        Button playEasy = findViewById(R.id.playeasy);
        playEasy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent play = new Intent(getApplicationContext(), game.getGame());
                play.putExtra("difficulty", 1);
                startActivity(play);
            }
        });

        if (difficulty >= 2) {
            Button playMedium = findViewById(R.id.playmedium);
            playMedium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent play = new Intent(getApplicationContext(), game.getGame());
                    play.putExtra("difficulty", 2);
                    startActivity(play);
                }
            });
        }

        if (difficulty >= 3) {

            Button playHard = findViewById(R.id.playhard);
            playHard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent play = new Intent(getApplicationContext(), game.getGame());
                    play.putExtra("difficulty", 3);
                    startActivity(play);
                }
            });

            Button rankEasy = findViewById(R.id.rank_easy);
            rankEasy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Database_Helper database = new Database_Helper();
                    database.setPlayerScores(game.getGameName(), 1, getTextViews());
                }
            });

            Button rankMedium = findViewById(R.id.rank_medium);
            rankMedium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Database_Helper database = new Database_Helper();
                    database.setPlayerScores(game.getGameName(), 2, getTextViews());
                }
            });

            Button rankHard = findViewById(R.id.rank_hard);
            rankHard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Database_Helper database = new Database_Helper();
                    database.setPlayerScores(game.getGameName(), 3, getTextViews());
                }
            });

        }

        // Set name of the game
        TextView tv_gamename = findViewById(R.id.gamename);
        String gamename = game.getGameName();
        tv_gamename.setText(gamename);

        // Getting and setting the scores from the database
        Database_Helper database = new Database_Helper();
        database.setPlayerScores(game.getGameName(), 1, getTextViews());


        backToMenu = findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBackToMenu();
            }
        });

        toSettings = findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });


        // Setting the instructionbutton for the nessecary games

        Game_Helper g = new Game_Helper();
        instructions = findViewById(R.id.toInstructions);

        if (game.getGameName().equals(g.GAMENAME_2048) || game.getGameName().equals(g.GAMENAME_KNIFFEL)
                || game.getGameName().equals(g.GAMENAME_MINESWEEPER) || game.getGameName().equals(g.GAMENAME_PUZZLE)
                || game.getGameName().equals(g.GAMENAME_XOBIG) || game.getGameName().equals(g.GAMENAME_SNAKE)){

            instructions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToInstruction();
                }
            });

        } else {
            instructions.setVisibility(View.GONE);
        }


        // Removing rankings for local multiplayergames

        if (game.getGameName().equals(g.GAMENAME_KNIFFEL) || game.getGameName().equals(g.GAMENAME_XOBIG)){
            findViewById(R.id.localMultiplayer).setVisibility(View.VISIBLE);
            findViewById(R.id.rankings).setVisibility(View.GONE);
        }

    }

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void goBackToMenu() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void goToInstruction() {
        Intent intent = new Intent(this, Instructions.class);
        intent.putExtra("game", this.game);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private ArrayList<TextView> getTextViews() {

        ArrayList<TextView> views = new ArrayList<>();

        TextView score1_name = findViewById(R.id.score1_name);
        TextView score1_num = findViewById(R.id.score1_num);
        TextView score2_name = findViewById(R.id.score2_name);
        TextView score2_num = findViewById(R.id.score2_num);
        TextView score3_name = findViewById(R.id.score3_name);
        TextView score3_num = findViewById(R.id.score3_num);
        TextView score4_name = findViewById(R.id.score4_name);
        TextView score4_num = findViewById(R.id.score4_num);
        TextView score5_name = findViewById(R.id.score5_name);
        TextView score5_num = findViewById(R.id.score5_num);
        TextView score6_name = findViewById(R.id.score6_name);
        TextView score6_num = findViewById(R.id.score6_num);
        TextView score7_name = findViewById(R.id.score7_name);
        TextView score7_num = findViewById(R.id.score7_num);
        TextView score8_name = findViewById(R.id.score8_name);
        TextView score8_num = findViewById(R.id.score8_num);
        TextView score9_name = findViewById(R.id.score9_name);
        TextView score9_num = findViewById(R.id.score9_num);
        TextView score10_name = findViewById(R.id.score10_name);
        TextView score10_num = findViewById(R.id.score10_num);

        views.add(score1_name);
        views.add(score1_num);
        views.add(score2_name);
        views.add(score2_num);
        views.add(score3_name);
        views.add(score3_num);
        views.add(score4_name);
        views.add(score4_num);
        views.add(score5_name);
        views.add(score5_num);
        views.add(score6_name);
        views.add(score6_num);
        views.add(score7_name);
        views.add(score7_num);
        views.add(score8_name);
        views.add(score8_num);
        views.add(score9_name);
        views.add(score9_num);
        views.add(score10_name);
        views.add(score10_num);

        return views;

    }

}