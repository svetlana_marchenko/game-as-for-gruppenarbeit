package com.example.myapplication.game_DragonFlight;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.example.myapplication.R;

public class Obstacle implements GameObject{

    private Rect rectangle;
    private Rect rectangle2;
    private int color;
    private Bitmap bm = new BitmapFactory().decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.dragonflight_stonywall);


    public Rect getRectangle(){
        return rectangle;
    }

    public void incrementY(float y){
        rectangle.top += y;
        rectangle.bottom += y;
        rectangle2.top += y;
        rectangle2.bottom += y;
    }

    public Obstacle(int rectHeight, int color, int startX, int startY, int playerGap){
        this.color = color;
        rectangle = new Rect(0, startY, startX, startY + rectHeight);
        rectangle2 = new Rect(startX + playerGap, startY, Constants.SCREEN_WIDTH, startY + rectHeight);
    }

    public boolean playerCollide(RectPlayer player){
        return (Rect.intersects(rectangle, player.getRectangle()) || Rect.intersects(rectangle2, player.getRectangle()));
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(rectangle, paint);
        canvas.drawRect(rectangle2, paint);
        int right = rectangle.width();
        int height = rectangle.height();
        int right2 = rectangle2.width();
        int height2 = rectangle2.height();
        canvas.drawBitmap(bm, new Rect(0, 0, right*2, height*8), rectangle, paint);
        canvas.drawBitmap(bm, new Rect(0, 0, right2*2, height2*8), rectangle2, paint);
    }

    @Override
    public void update() {

    }
}
