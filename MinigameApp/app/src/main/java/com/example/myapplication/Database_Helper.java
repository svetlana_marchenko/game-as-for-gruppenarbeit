package com.example.myapplication;

import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Database_Helper extends AppCompatActivity {

    FirebaseDatabase fb;

    public Database_Helper() {
        this.fb = FirebaseDatabase.getInstance("https://minigamecollection-4ddce-default-rtdb.europe-west1.firebasedatabase.app/");
    }

    public void addScoreToDatabase(Database_Score scoreItem) {

        System.out.println("Adding score of " + scoreItem.getScore() + " by " + scoreItem.getUsername() + " to database");

        DatabaseReference reference = fb.getReference(scoreItem.getGame());
        reference.child("" + scoreItem.getDifficulty()).child(scoreItem.getUsername()).setValue(scoreItem);

    }

    public void setPlayerScores(String gamename, int difficulty, ArrayList<TextView> views) {

        DatabaseReference ref = fb.getReference(gamename).child("" + difficulty);

        ref.orderByChild("score").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int i = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (i == 20) {
                        return;
                    }

                    Database_Score scoreItem = ds.getValue(Database_Score.class);
                    String name = scoreItem.getUsername();
                    Game_Helper g = new Game_Helper();

                    int score = scoreItem.getScore();
                    if (!scoreItem.getGame().equals(g.GAMENAME_PUZZLE)
                            && !scoreItem.getGame().equals(g.GAMENAME_MINESWEEPER)) {
                        score = -score;
                    }

                    String add = "";
                    if (scoreItem.getGame().equals(g.GAMENAME_GOSPRINGER)
                            || scoreItem.getGame().equals(g.GAMENAME_FLAGS)
                            || scoreItem.getGame().equals(g.GAMENAME_CAPITALS)) {
                        add = " %";
                    }

                    System.out.println(name);
                    System.out.println(score);

                    views.get(i).setText(name);
                    views.get(i + 1).setText("" + score + add);

                    i += 2;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Error in getPlayerScores while getting playerdata");
            }

        });

    }

}
