package com.example.myapplication.game_DragonFlight;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;

import com.example.myapplication.R;
import android.graphics.Point;
import android.graphics.Rect;

public class RectPlayer implements GameObject{

    private Rect rectangle;
    private int color;

    private Animation idle;
    private Animation walkRight;
    private Animation walkLeft;
    private AnimationManager animationManager;

    public Rect getRectangle(){
        return rectangle;
    }

    public RectPlayer(Rect rectangle, int color){
        this.color = color;
        this.rectangle = rectangle;

        BitmapFactory bf = new BitmapFactory();
        /*
            Author:
            Warlock's Gauntlet team
            Warlock's Gauntlet artists - rAum, jackFlower, DrZoliparia, Neil2D
            (Submitted by Liosan)
            https://opengameart.org/content/top-down-green-dragon-animated
            license: http://creativecommons.org/licenses/by/3.0/
         */
        Bitmap idleImage = bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.dragonflight_dragonone);
        Bitmap walk1 = bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.dragonflight_dragontwo);
        Bitmap walk2 = bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.dragonflight_dragonthree);

        idle = new Animation(new Bitmap[]{idleImage}, 2);
        walkRight = new Animation(new Bitmap[]{walk1, walk2}, 0.5f);

        Matrix m = new Matrix();
        m.preScale(-1, 1);
        walk1 = Bitmap.createBitmap(walk1, 0, 0, walk1.getWidth(), walk1.getHeight(), m, false);
        walk2 = Bitmap.createBitmap(walk2, 0, 0, walk2.getWidth(), walk2.getHeight(), m, false);

        walkLeft = new Animation(new Bitmap[]{walk1, walk2}, 0.5f);

        animationManager = new AnimationManager(new Animation[]{idle, walkRight, walkLeft});

    }

    @Override
    public void draw(Canvas canvas) { animationManager.draw(canvas, rectangle); }

    @Override
    public void update() {
        animationManager.update();
    }

    public void update(Point point){
        float oldLeft = rectangle.left;

        rectangle.set(point.x - rectangle.width()/2, point.y - rectangle.height()/2, point.x + rectangle.width()/2, point.y + rectangle.height()/2 );

        /*
        overall music??????
        https://opengameart.org/content/battle-theme-ii
         */
        int state = 0;
        if(rectangle.left - oldLeft > 5){
            state = 1;
        } else if (rectangle.left - oldLeft < -5){
            state = 2;
        }
        animationManager.playEnum(state);
        animationManager.update();
    }
}
