package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.myapplication.game_2048.Game_2048;
import com.example.myapplication.game_DragonFlight.GameDragonFlight;
import com.example.myapplication.game_flappybird.Game_FlappyBird;
import com.example.myapplication.game_gospringer.Game_GoSpringer;
import com.example.myapplication.game_guessthecapital.Game_GuessTheCapital;
import com.example.myapplication.game_guesstheflag.Game_GuessTheFlag;
import com.example.myapplication.game_snake.Game_Snake;
import com.example.myapplication.game_kniffel.GameKniffel;
import com.example.myapplication.game_longerline.Game_LongerLine;
import com.example.myapplication.game_minesweeper.Game_Minesweeper;
import com.example.myapplication.game_puzzle.Game_Puzzle15;
import com.example.myapplication.game_sudoku.Game_Sudoku;
import com.example.myapplication.game_xobig.Game_XObig;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ImageButton toStats;
    private ImageButton toSettings;
    private GridView gridView;

    private int currentTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        SharedPreferences pref = getSharedPreferences("firstrun", 0);
        boolean firstRun = pref.getBoolean("run", true);

        if (firstRun) {
            ((GlobalVariables) this.getApplication()).resetAllStats();
            pref.edit().putBoolean("run", false).apply();
        }

        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toStats = (ImageButton) findViewById(R.id.tostats);
        toStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToStatistics();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.tosettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        List<Game> image_details = getListData();
        gridView = (GridView) findViewById(R.id.gridviewCategories);
        gridView.setAdapter(new CategoriesAdapter(this, image_details));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                // Get name of the selected game
                ConstraintLayout gameItem = (ConstraintLayout) gridView.getChildAt((int) id);
                CardView gameCard = (CardView) gameItem.getChildAt(0);
                ConstraintLayout gameItemLayout = (ConstraintLayout) gameCard.getChildAt(0);
                TextView gameNameTv = (TextView) gameItem.getChildAt(gameItem.getChildCount() - 1);
                String gameName = gameNameTv.getText().toString();

                Game_Helper g = new Game_Helper();

                // Opens the right activity for the selected game
                switch (gameName) {

                    case "Sudoku": {
                        Game sudoku = new Game(g.GAMENAME_SUDUKO, 3, Game_Sudoku.class, R.layout.activity_sudoku);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), sudoku);
                        break;
                    }
                    case "Knight": {
                        Game goSpringer = new Game(g.GAMENAME_GOSPRINGER, 1, Game_GoSpringer.class, R.layout.activity_gospringer);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), goSpringer);
                        break;
                    }
                    case "Yahtzee": {
                        Game kniffel = new Game(g.GAMENAME_KNIFFEL, 1, GameKniffel.class, R.layout.activity_game_kniffel);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), kniffel);
                        break;
                    }
                    case "Lines": {
                        Game kniffel = new Game(g.GAMENAME_LONGERLINE, 1, Game_LongerLine.class, R.layout.activity_longerline);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), kniffel);
                        break;
                    }
                    case "Enigma": {
                        Game puzzle = new Game(g.GAMENAME_PUZZLE, 3, Game_Puzzle15.class,
                                R.layout.activity_puzzle15_4x4);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), puzzle);
                        break;
                    }
                    case "Tic Tac": {
                        Game xoBig = new Game(g.GAMENAME_XOBIG, 1, Game_XObig.class, R.layout.activity_xobig);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), xoBig);
                        break;
                    }
                    case "Dragonfly": {
                        Game dragon = new Game(g.GAMENAME_DRAGONFLIGHT, 1, GameDragonFlight.class, R.layout.activity_default);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), dragon);
                        break;
                    }
                    case "Hatty Bird": {
                        Game flappyBird = new Game(g.GAMENAME_FLAPPYBIRD, 1, Game_FlappyBird.class, R.layout.activity_game_flappybird);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), flappyBird);
                        break;
                    }
                    case "2048": {
                        Game zweiNullVierAcht = new Game(g.GAMENAME_2048, 1, Game_2048.class, R.layout.activity_game_2048);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), zweiNullVierAcht);
                        break;
                    }
                    case "Sweeper": {
                        Game minesweeper = new Game(g.GAMENAME_MINESWEEPER, 3, Game_Minesweeper.class, R.layout.activity_minesweeper);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), minesweeper);
                        break;
                    }
                    case "Capitals": {
                        Game capitals = new Game(g.GAMENAME_CAPITALS, 3, Game_GuessTheCapital.class, R.layout.activity_guessthecapital);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), capitals);
                        break;
                    }
                    case "Flags": {
                        Game flags = new Game(g.GAMENAME_FLAGS, 3, Game_GuessTheFlag.class, R.layout.activity_guesstheflag);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), flags);
                        break;
                    }
                    case "Snake": {
                        Game snake = new Game(g.GAMENAME_SNAKE, 3, Game_Snake.class, R.layout.activity_default);
                        goToGame(new Intent(getApplicationContext(), GameConfig.class), snake);
                        break;
                    }
                    default:
                        break;
                }

            }
        });

    }

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    private void goToStatistics() {
        Intent intent = new Intent(this, Stats.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        onStop();
    }

    private void goToGame(Intent intent, Game game) {
        // Passing Difficultycounter for config
        intent.putExtra("game", game);

        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private List<Game> getListData() {

        Game_Helper g = new Game_Helper();

        List<Game> list = new ArrayList<Game>();
        Game game1 = new Game(g.GAMENAME_SUDUKO, "sudoku", "190");
        Game game2 = new Game(g.GAMENAME_GOSPRINGER, "gospringer", "190");
        Game game3 = new Game(g.GAMENAME_KNIFFEL, "yahtzee", "190");
        Game game4 = new Game(g.GAMENAME_LONGERLINE, "longer_lines", "190");
        Game game5 = new Game(g.GAMENAME_PUZZLE, "enigma", "190");
        Game game6 = new Game(g.GAMENAME_XOBIG, "ticcitac", "190");
        Game game7 = new Game(g.GAMENAME_DRAGONFLIGHT, "dragon", "190");
        Game game8 = new Game(g.GAMENAME_FLAPPYBIRD, "flappybirdpic", "190");
        Game game9 = new Game(g.GAMENAME_2048, "twothousand", "190");
        Game game10 = new Game(g.GAMENAME_MINESWEEPER, "minesweeper", "190");
        Game game11 = new Game(g.GAMENAME_CAPITALS, "capitals", "190");
        Game game12 = new Game(g.GAMENAME_FLAGS, "flags", "190");
        Game game13 = new Game(g.GAMENAME_SNAKE, "snake", "190");

        list.add(game1);
        list.add(game2);
        list.add(game3);
        list.add(game4);
        list.add(game5);
        list.add(game6);
        list.add(game7);
        list.add(game8);
        list.add(game9);
        list.add(game10);
        list.add(game11);
        list.add(game12);
        list.add(game13);

        return list;
    }

}