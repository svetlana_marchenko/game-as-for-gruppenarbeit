// Alexander Becker

package com.example.myapplication.game_guesstheflag;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.*;

public class Game_GuessTheFlag extends AppCompatActivity {

    private int currentTheme;

    long startTime;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    private ImageView imageView;
    private Button    buttonNewGame;
    private Button    button1;
    private Button    button2;
    private Button    button3;
    private Button    button4;

    private Button buttonTest;

    private ProgressBar progressBar;
    private TextView    textViewProgress;
    private TextView    textViewPercentage;
    private Handler     handler = new Handler();
    private TextView    textViewResult;

    private ArrayList<String> flagsEasy   = new ArrayList<String>();
    private ArrayList<String> flagsMedium = new ArrayList<String>();
    private ArrayList<String> flagsHard   = new ArrayList<String>();

    private ArrayList<String> flags = new ArrayList<String>();

    private int    currentIndex = 0;
    private String correctAnswer;
    private int numberToFinish = 40;


    private final int EASY   = 1;
    private final int MEDIUM = 2;
    private final int HARD   = 3;
    private       int difficulty;

    private int counterCorrectAnswers;
    private int percentageCorrectAnswers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.startTime = System.currentTimeMillis();

        // Get saved Theme out of Shared Preferances
        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guesstheflag);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        imageView = findViewById(R.id.imageView);
        buttonNewGame = findViewById(R.id.buttonNewGame);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        buttonNewGame.setOnClickListener(onClickListener);
        button1.setOnClickListener(onClickListener);
        button2.setOnClickListener(onClickListener);
        button3.setOnClickListener(onClickListener);
        button4.setOnClickListener(onClickListener);

        progressBar = findViewById(R.id.progressBar);
        textViewProgress = findViewById(R.id.textViewProgress);
        textViewPercentage = findViewById(R.id.textViewPercentage);
        textViewResult = findViewById(R.id.textViewResult);

        difficulty = EASY;
        Bundle b = getIntent().getExtras();
        int diff = b.getInt("difficulty");
        if(diff == 1)       difficulty = EASY;
        else if(diff == 2)  difficulty = MEDIUM;
        else                difficulty = HARD;

        newGame();

    } // end of onCreate

    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.buttonNewGame:
                    print("button clicked new game");
                    newGame();
                    break;
                case R.id.button1:
                    handleAnswer(button1);
                    break;
                case R.id.button2:
                    handleAnswer(button2);
                    break;
                case R.id.button3:
                    handleAnswer(button3);
                    break;
                case R.id.button4:
                    handleAnswer(button4);
                    break;
                default:
                    break;
            }

        }
    }; // View.OnClickListener

    private void initializeProgressBar() {
        new Thread(new Runnable() {
            public void run() {
//                while (currentIndex <= flags.size()) {
                while (currentIndex <= numberToFinish) {
                    // Update the progress bar and display the
                    // current value in the text view
//                    progressBar.setMax(flags.size());
                    progressBar.setMax(numberToFinish);
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(currentIndex);
//                            textViewProgress.setText(currentIndex + "/" + progressBar.getMax());
                            textViewProgress.setText(currentIndex + "/" + progressBar.getMax());
                            int percentage = Math.round(100 * currentIndex / numberToFinish);
                            textViewPercentage.setText(percentage + " %");
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void newGame() {
        currentIndex = 0;
        flags.clear();
        counterCorrectAnswers = 0;
        percentageCorrectAnswers = 0;

        ((GlobalVariables) this.getApplication()).addOneRound();

        button1.setText("");
        button2.setText("");
        button3.setText("");
        button4.setText("");
        imageView.setImageDrawable(null);
        textViewProgress.setText("");
        textViewPercentage.setText("");
        textViewResult.setText("");

        initializeFlags(difficulty);
        initializeProgressBar();
        nextRound();
    }

    private void nextRound() {
        button1.setText("");
        button2.setText("");
        button3.setText("");
        button4.setText("");
        imageView.setImageDrawable(null);

        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimaryDark, typedValue, true);
        int color = typedValue.data;

        button1.setBackgroundColor(color);
        button2.setBackgroundColor(color);
        button3.setBackgroundColor(color);
        button4.setBackgroundColor(color);


        if (isFinished() == true) return;

        correctAnswer = flags.get(currentIndex);
        String fileNameFlag = "flag_" + correctAnswer;
        currentIndex++;

        // get flag of current country and place it in imageview
        Context context = imageView.getContext();
        int id = context.getResources().getIdentifier(fileNameFlag, "drawable", context.getPackageName());
        imageView.setImageResource(id);

        correctAnswer = correctAnswer.replaceAll("_", " ");

        // get random button for correct answer
        int random1to4 = getRandom1to4();
        // bound correct answer to button
        if (random1to4 == 1) button1.setText(correctAnswer);
        else if (random1to4 == 2) button2.setText(correctAnswer);
        else if (random1to4 == 3) button3.setText(correctAnswer);
        else if (random1to4 == 4) button4.setText(correctAnswer);

        // fill other 3 buttons with wrong answers (unequal to current, correct answer saved in string)
        if (!button1.getText().equals(correctAnswer))
            button1.setText(getUnequalCountry(correctAnswer));
        if (!button2.getText().equals(correctAnswer))
            button2.setText(getUnequalCountry(correctAnswer));
        if (!button3.getText().equals(correctAnswer))
            button3.setText(getUnequalCountry(correctAnswer));
        if (!button4.getText().equals(correctAnswer))
            button4.setText(getUnequalCountry(correctAnswer));

        // generate name for buttons, change underscope
        replaceUnderscopeWithBlank();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_flags(millis); // Hier setTime_deinSpiel

    }

    private boolean isFinished() {
//        if (currentIndex == flags.size()) {
        if (currentIndex == numberToFinish) {
            button1.setBackgroundColor(Color.GREEN);
            button2.setBackgroundColor(Color.GREEN);
            button3.setBackgroundColor(Color.GREEN);
            button4.setBackgroundColor(Color.GREEN);

//            textViewResult.setText("you have " + counterCorrectAnswers + " of " + flags.size() + " correct (" + percentageCorrectAnswers + "%)");
            textViewResult.setText("you have " + counterCorrectAnswers + " of " + numberToFinish + " correct (" + percentageCorrectAnswers + "%)");

            // send results to backend
            setRecord();

            return true;
        }

        return false;
    }


    private String getUnequalCountry(String currentCountry) {
        int randomIndex = getRandomIndexForArrayList();
        while (flags.get(randomIndex).equals(currentCountry)) {
            randomIndex = getRandomIndexForArrayList();
        }
        return flags.get(randomIndex);
    }

    private void handleAnswer(View v) {
        Button buttonClicked = (Button) findViewById(v.getId());
        String clickedAnswer = buttonClicked.getText().toString();
        Button correctButton = getCorrectButton();

        @ColorInt int green = getResources().getColor(R.color.green_window);

        if (clickedAnswer.equals(correctAnswer)) {
            print("correct answer");
            buttonClicked.setBackgroundColor(green);
            counterCorrectAnswers++;
//            percentageCorrectAnswers = Math.round(100 * counterCorrectAnswers / flags.size());
            percentageCorrectAnswers = Math.round(100 * counterCorrectAnswers / numberToFinish);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // Actions to do after 3 seconds
                    nextRound();
                }
            }, 700);
        } else {
            print("wrong answer");
            buttonClicked.setBackgroundColor(Color.RED);
            correctButton.setBackgroundColor(green);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // Actions to do after 3 seconds
                    nextRound();
                }
            }, 2000);
        }


    }

    private Button getCorrectButton() {
        if (button1.getText().toString().equals(correctAnswer)) return button1;
        else if (button2.getText().toString().equals(correctAnswer)) return button2;
        else if (button3.getText().toString().equals(correctAnswer)) return button3;
        else return button4;
    }

    private int getRandom1to4() {
        // nextInt(4) gives 0...3
        // +1 makes 1...4
        return new Random().nextInt(4) + 1;
    }

    private int getRandomIndexForArrayList() {
        // generates an integer between 0 and max. size of ArrayList flags
        // will be used as index for ArrayList
        return new Random().nextInt(flags.size());
    }

    private void replaceUnderscopeWithBlank() {
        String str;
        str = button1.getText().toString().replaceAll("_", " ");
        button1.setText(str);
        str = button2.getText().toString().replaceAll("_", " ");
        button2.setText(str);
        str = button3.getText().toString().replaceAll("_", " ");
        button3.setText(str);
        str = button4.getText().toString().replaceAll("_", " ");
        button4.setText(str);
    }

    private void replaceBlankWithUnderscope() {
        String str;
        str = button1.getText().toString().replaceAll(" ", "_");
        button1.setText(str);
        str = button2.getText().toString().replaceAll(" ", "_");
        button2.setText(str);
        str = button3.getText().toString().replaceAll(" ", "_");
        button3.setText(str);
        str = button4.getText().toString().replaceAll(" ", "_");
        button4.setText(str);
    }


    private void print(String msg) {
        System.out.println(msg);
    }

    private void initializeFlags(int mode) {
        if (mode == 1)
            initializeEasy();
        else if (mode == 2)
            initializeMedium();
        else if (mode == 3)
            initializeHard();
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public int getRecord() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPref.getInt("Flags", 0);
    } // getRecord


    private void setRecord() {
        SharedPreferences        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor     = sharedPref.edit();
        editor.putInt("Flags", percentageCorrectAnswers);
        editor.commit();

        // Put record into database
        String          name       = sharedPref.getString("playername", "Username");
        String          gamename   = new Game_Helper().GAMENAME_FLAGS;
        Database_Helper database   = new Database_Helper();
        Database_Score  scoreEntry = new Database_Score(gamename, percentageCorrectAnswers, name, difficulty);
        database.addScoreToDatabase(scoreEntry);
    } // setRecord

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void initializeEasy() {
        // easy mode contains countries from europe, north and south america
        flagsEasy.clear();

        // europe
        flagsEasy.add("albania");
        flagsEasy.add("andorra");
        flagsEasy.add("austria");
        flagsEasy.add("belarus");
        flagsEasy.add("belgium");
        flagsEasy.add("bosnia_and_herzegovina");
        flagsEasy.add("bulgaria");
        flagsEasy.add("croatia");
        flagsEasy.add("cyprus");
        flagsEasy.add("czech_republic");
        flagsEasy.add("denmark");
        flagsEasy.add("england");
        flagsEasy.add("estonia");
        flagsEasy.add("finland");
        flagsEasy.add("france");
        flagsEasy.add("germany");
        flagsEasy.add("greece");
        flagsEasy.add("hungary");
        flagsEasy.add("iceland");
        flagsEasy.add("ireland");
        flagsEasy.add("italy");
        flagsEasy.add("kosovo");
        flagsEasy.add("latvia");
        flagsEasy.add("liechtenstein");
        flagsEasy.add("lithuania");
        flagsEasy.add("luxembourg");
        flagsEasy.add("malta");
        flagsEasy.add("moldova");
        flagsEasy.add("monaco");
        flagsEasy.add("montenegro");
        flagsEasy.add("netherlands");
        flagsEasy.add("north_macedonia");
        flagsEasy.add("northern_ireland");
        flagsEasy.add("norway");
        flagsEasy.add("poland");
        flagsEasy.add("portugal");
        flagsEasy.add("romania");
        flagsEasy.add("russia");
        flagsEasy.add("san_marino");
        flagsEasy.add("scotland");
        flagsEasy.add("serbia");
        flagsEasy.add("slovakia");
        flagsEasy.add("slovenia");
        flagsEasy.add("spain");
        flagsEasy.add("sweden");
        flagsEasy.add("switzerland");
        flagsEasy.add("ukraine");
        flagsEasy.add("united_kingdom");
        flagsEasy.add("vatican");
        flagsEasy.add("wales");


        // north america
        flagsEasy.add("canada");
        flagsEasy.add("mexico");
        flagsEasy.add("usa");

        // south america
        flagsEasy.add("argentina");
        flagsEasy.add("bolivia");
        flagsEasy.add("brazil");
        flagsEasy.add("chile");
        flagsEasy.add("colombia");
        flagsEasy.add("ecuador");
        flagsEasy.add("guyana");
        flagsEasy.add("paraguay");
        flagsEasy.add("peru");
        flagsEasy.add("suriname");
        flagsEasy.add("uruguay");
        flagsEasy.add("venezuela");

        // randomize all entries, so there is no particular order
        Collections.shuffle(flagsEasy);

        flags = flagsEasy;
    }

    private void initializeMedium() {
        // medium mode contains countries from asia and africa

        flagsMedium.clear();

        // africa
        flagsMedium.add("algeria");
        flagsMedium.add("angola");
        flagsMedium.add("benin");
        flagsMedium.add("botswana");
        flagsMedium.add("burkina_faso");
        flagsMedium.add("burundi");
        flagsMedium.add("cameroon");
        flagsMedium.add("cape_verde");
        flagsMedium.add("central_african_republic");
        flagsMedium.add("chad");
        flagsMedium.add("comoros");
        flagsMedium.add("congo");
        flagsMedium.add("cote_d_ivoire");
        flagsMedium.add("djibouti");
        flagsMedium.add("egypt");
        flagsMedium.add("equatorial_guinea");
        flagsMedium.add("eritrea");
        flagsMedium.add("ethiopia");
        flagsMedium.add("gabon");
        flagsMedium.add("gambia");
        flagsMedium.add("ghana");
        flagsMedium.add("guinea");
        flagsMedium.add("guinea_bissau");
        flagsMedium.add("kenya");
        flagsMedium.add("lesotho");
        flagsMedium.add("liberia");
        flagsMedium.add("libya");
        flagsMedium.add("madagascar");
        flagsMedium.add("malawi");
        flagsMedium.add("mali");
        flagsMedium.add("mauritania");
        flagsMedium.add("mauritius");
        flagsMedium.add("morocco");
        flagsMedium.add("mozambique");
        flagsMedium.add("namibia");
        flagsMedium.add("niger");
        flagsMedium.add("nigeria");
        flagsMedium.add("republic_of_congo");
        flagsMedium.add("rwanda");
        flagsMedium.add("san_tome_and_principe");
        flagsMedium.add("senegal");
        flagsMedium.add("seychelles");
        flagsMedium.add("sierra_leone");
        flagsMedium.add("somalia");
        flagsMedium.add("south_africa");
        flagsMedium.add("south_sudan");
        flagsMedium.add("sudan");
        flagsMedium.add("swaziland");
        flagsMedium.add("tanzania");
        flagsMedium.add("togo");
        flagsMedium.add("tunisia");
        flagsMedium.add("uganda");
        flagsMedium.add("western_sahara");
        flagsMedium.add("zambia");
        flagsMedium.add("zimbabwe");

        // asia
        flagsMedium.add("afghanistan");
        flagsMedium.add("armenia");
        flagsMedium.add("australia");
        flagsMedium.add("azerbaijan");
        flagsMedium.add("bahrain");
        flagsMedium.add("bangladesh");
        flagsMedium.add("bhutan");
        flagsMedium.add("brunei");
        flagsMedium.add("cambodia");
        flagsMedium.add("china");
        flagsMedium.add("east_timor");
        flagsMedium.add("georgia");
        flagsMedium.add("hong_kong");
        flagsMedium.add("india");
        flagsMedium.add("indonesia");
        flagsMedium.add("iran");
        flagsMedium.add("iraq");
        flagsMedium.add("israel");
        flagsMedium.add("japan");
        flagsMedium.add("jordan");
        flagsMedium.add("kazakhstan");
        flagsMedium.add("kuwait");
        flagsMedium.add("kyrgyzstan");
        flagsMedium.add("laos");
        flagsMedium.add("lebanon");
        flagsMedium.add("malaysia");
        flagsMedium.add("maldives");
        flagsMedium.add("mongolia");
        flagsMedium.add("myanmar");
        flagsMedium.add("nepal");
        flagsMedium.add("new_zealand");
        flagsMedium.add("north_korea");
        flagsMedium.add("oman");
        flagsMedium.add("pakistan");
        flagsMedium.add("palestine");
        flagsMedium.add("philippines");
        flagsMedium.add("qatar");
        flagsMedium.add("saudi_arabia");
        flagsMedium.add("singapore");
        flagsMedium.add("south_korea");
        flagsMedium.add("sri_lanka");
        flagsMedium.add("syria");
        flagsMedium.add("taiwan");
        flagsMedium.add("tajikistan");
        flagsMedium.add("thailand");
        flagsMedium.add("turkey");
        flagsMedium.add("turkmenistan");
        flagsMedium.add("united_arab_emirates");
        flagsMedium.add("uzbekistan");
        flagsMedium.add("vietman");
        flagsMedium.add("yemen");

        Collections.shuffle(flagsMedium);

        flags = flagsMedium;
    }

    private void initializeHard() {
        // hard mode contain countries from central america, caribic islands and small oceanic islands

        flagsHard.clear();

        // oceanic islands
        flagsHard.add("aland");
        flagsHard.add("american_samoa");
        flagsHard.add("anguilla");
        flagsHard.add("antarctica");
        flagsHard.add("aruba");
        flagsHard.add("bermuda");
        flagsHard.add("bonaire");
        flagsHard.add("british_indian_ocean_territory");
        flagsHard.add("british_virgin_islands");
        flagsHard.add("cayman_islands");
        flagsHard.add("christmas_island");
        flagsHard.add("cocos_islands");
        flagsHard.add("cook_islands");
        flagsHard.add("curacao");
        flagsHard.add("falkland_islands");
        flagsHard.add("faroe_islands");
        flagsHard.add("fiji");
        flagsHard.add("french_guiana");
        flagsHard.add("french_polynesia");
        flagsHard.add("french_southern_and_antarctic_lands");
        flagsHard.add("gibraltar");
        flagsHard.add("greenland");
        flagsHard.add("guadeloupe");
        flagsHard.add("guam");
        flagsHard.add("guernsey");
        flagsHard.add("isle_of_man");
        flagsHard.add("jersey_island");
        flagsHard.add("kiribati");
        flagsHard.add("macau");
        flagsHard.add("marshall_islands");
        flagsHard.add("martinique");
        flagsHard.add("mayotte");
        flagsHard.add("micronesia");
        flagsHard.add("montserrat");
        flagsHard.add("nauru");
        flagsHard.add("new_caledonia");
        flagsHard.add("niue");
        flagsHard.add("norfolk_island");
        flagsHard.add("northern_mariana_islands");
        flagsHard.add("palau");
        flagsHard.add("papua_new_guinea");
        flagsHard.add("pitcairn_islands");
        flagsHard.add("puerto_rico");
        flagsHard.add("reunion");
        flagsHard.add("saint_barthelemy");
        flagsHard.add("saint_pierre_and_miquelon");
        flagsHard.add("samoa");
        flagsHard.add("sint_maarten");
        flagsHard.add("solomon_islands");
        flagsHard.add("southern_rhodesia");
        flagsHard.add("south_georgia_and_the_south_sandwich_islands");
        flagsHard.add("tokelau");
        flagsHard.add("tonga");
        flagsHard.add("turks_and_caicos_islands");
        flagsHard.add("tuvalu");
        flagsHard.add("united_states_virgin_islands");
        flagsHard.add("vanuatu");
        flagsHard.add("wallis_and_futuna");

        //central america and caribic
        flagsHard.add("antigua_and_barbuda");
        flagsHard.add("bahamas");
        flagsHard.add("barbados");
        flagsHard.add("belize");
        flagsHard.add("costa_rica");
        flagsHard.add("cuba");
        flagsHard.add("dominica");
        flagsHard.add("dominican_republic");
        flagsHard.add("el_salvador");
        flagsHard.add("grenada");
        flagsHard.add("guatemala");
        flagsHard.add("haiti");
        flagsHard.add("honduras");
        flagsHard.add("jamaica");
        flagsHard.add("nicaragua");
        flagsHard.add("panama");
        flagsHard.add("saint_kitts_and_nevis");
        flagsHard.add("saint_lucia");
        flagsHard.add("saint_thomas_and_prince");
        flagsHard.add("saint_vincent_and_grenadines");
        flagsHard.add("trinidad_and_tobago");

        Collections.shuffle(flagsHard);

        flags = flagsHard;
    }

}
