// Alexander Becker

package com.example.myapplication.game_gospringer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;

public class Game_GoSpringer extends AppCompatActivity {

    private int currentTheme;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    long startTime;

    private final int             N                    = 6;
    private       TextView        tvCurrentCoverage;
    private       TextView        tvRecordCoverage;
    private       TextView        tvGameOver;
    private       int             stepCounter;
    private       int             currentCoverage      = 0;
    private final int             FLAG_IMPOSSIBLE      = 0;
    private final int             FLAG_POSSIBLE        = 1;
    private final int             FLAG_COVERED         = 2;
    private final int             FLAG_CURRENTPOSITION = 3;
    // contains the values, wether a turn/move is possible, impossible, already done or is current position
    private       int[][]         fieldValues          = new int[N][N];
    private       int             rowCurrent;
    private       int             columnCurrent;
    private       ImageButton[][] buttons;
    private       Button          buttonNewGame;
    private final int             BUTTON_ID[][]        = {{R.id.button_00, R.id.button_01, R.id.button_02, R.id.button_03, R.id.button_04, R.id.button_05},
            {R.id.button_10, R.id.button_11, R.id.button_12, R.id.button_13, R.id.button_14, R.id.button_15},
            {R.id.button_20, R.id.button_21, R.id.button_22, R.id.button_23, R.id.button_24, R.id.button_25},
            {R.id.button_30, R.id.button_31, R.id.button_32, R.id.button_33, R.id.button_34, R.id.button_35},
            {R.id.button_40, R.id.button_41, R.id.button_42, R.id.button_43, R.id.button_44, R.id.button_45},
            {R.id.button_50, R.id.button_51, R.id.button_52, R.id.button_53, R.id.button_54, R.id.button_55}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.startTime = System.currentTimeMillis();
        // wird schon bei newRound() hochgezählt
//        ((GlobalVariables) this.getApplication()).addOneRound();

        // Get saved Theme out of Shared Preferances
        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gospringer);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        tvCurrentCoverage = findViewById(R.id.tvCurrentCoverage);
        tvRecordCoverage = findViewById(R.id.tvRecordCoverage);
        tvGameOver = findViewById(R.id.tvGameOver);

        buttonNewGame = findViewById(R.id.buttonNewGame);
        buttonNewGame.setOnClickListener(onClickListener);

        buttons = new ImageButton[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                buttons[i][j] = findViewById(BUTTON_ID[i][j]);
                buttons[i][j].setOnClickListener(onClickListener);
            }
        }
        newGame();
    } // end: onCreate

    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_knight(millis);

    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // ClickListener for buttons which represent the fields
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (v.getId() == BUTTON_ID[i][j]) {
                        move(i, j);
                    }
                }
            }

            if (v.getId() == R.id.buttonNewGame) {
                newGame();
            }

        } // onClick
    }; // View.OnClickListener

    private void newGame() {
        ((GlobalVariables) this.getApplication()).addOneRound();
        stepCounter = 0;
        currentCoverage = 0;

        // initialized dummy values which will be replaced with the first move
        // need to be negative for checking >= 0 later on
        rowCurrent = -10;
        columnCurrent = -10;

        tvCurrentCoverage.setText(String.valueOf(getCoverage()) + " %");
        tvRecordCoverage.setText(String.valueOf(getRecord()) + " %");
        tvGameOver.setText("");

        // initialize all fields so any move is possible
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                fieldValues[i][j] = FLAG_POSSIBLE;
                buttons[i][j].setImageResource(R.drawable.gospringer_possible);
            }
        }
    }

    private void move(int row, int column) {

        if (getFieldValue(row, column) == 1) {

            // set current position and set new image
            setFieldValue(row, column, FLAG_CURRENTPOSITION);
            buttons[row][column].setImageResource(R.drawable.gospringer_springer);

            // mark previous position as COVERED, set COVERED image
            // check for >= 0 (wether it is the first move or not)
            if (rowCurrent >= 0 && columnCurrent >= 0) {
                setFieldValue(rowCurrent, columnCurrent, FLAG_COVERED);
                buttons[rowCurrent][columnCurrent].setImageResource(R.drawable.gospringer_covored);
            }

            // update current position
            this.rowCurrent = row;
            this.columnCurrent = column;

            stepCounter++;
            setCurrentCoverage(stepCounter);
            tvCurrentCoverage.setText(String.valueOf(currentCoverage) + " %");

            updateFields();
        }
    } // end: move


    private void updateFields() {
        // iterate throught every field and update value and image

        boolean atLeastOnePossibility = false;

        // r: row... c: column
        for (int r = 0; r < N; r++) {
            for (int c = 0; c < N; c++) {

                if (getFieldValue(r, c) == FLAG_COVERED || getFieldValue(r, c) == FLAG_CURRENTPOSITION) {
                    // continue on covered and current psitions
                    continue;
                }

                if (isPossible(this.rowCurrent, this.columnCurrent, r, c)) {
                    setFieldValue(r, c, FLAG_POSSIBLE);
                    buttons[r][c].setImageResource(R.drawable.gospringer_possible);
                    atLeastOnePossibility = true;
                } else {
                    setFieldValue(r, c, FLAG_IMPOSSIBLE);
                    buttons[r][c].setImageResource(R.drawable.gospringer_impossible);
                }
            } // for(c)
        } // for(r)

        if (!atLeastOnePossibility) {
            int coverage = getCoverage();
            if (coverage > getRecord()) {
                setRecord(coverage);
            }
            if (coverage < 100) {
                tvGameOver.setText("Game Over!");
            }
        }

    } // Ende: updateFields


    private boolean isPossible(int rowCurrent, int columnCurrent, int rowNew, int columnNew) {
        // check wether planned move is possible

        boolean possible = false;

        // all possible positions for a knight figure in chess
        // 2 up, 1 left/right
        if (rowCurrent - 2 == rowNew && columnCurrent - 1 == columnNew) possible = true;
        if (rowCurrent - 2 == rowNew && columnCurrent + 1 == columnNew) possible = true;

        // 2 right, 1 upp/down
        if (columnCurrent + 2 == columnNew && rowCurrent - 1 == rowNew) possible = true;
        if (columnCurrent + 2 == columnNew && rowCurrent + 1 == rowNew) possible = true;

        // 2 down, 1 left/right
        if (rowCurrent + 2 == rowNew && columnCurrent - 1 == columnNew) possible = true;
        if (rowCurrent + 2 == rowNew && columnCurrent + 1 == columnNew) possible = true;

        // 2 left, 1 up/down
        if (columnCurrent - 2 == columnNew && rowCurrent - 1 == rowNew) possible = true;
        if (columnCurrent - 2 == columnNew && rowCurrent + 1 == rowNew) possible = true;

        return possible;
    }

    private void checkFinished() {
        if (getCoverage() == 100) {
            toastie("Winner!");
            setRecord(getCoverage());
        }
    }


    private int getRecord() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPref.getInt("recordCoverage2", 0);
    }

    private void setRecord(int currentCoverage) {
        SharedPreferences        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor     = sharedPref.edit();
        editor.putInt("recordCoverage2", currentCoverage);
        editor.commit();

        // Put record into database
        String          name       = sharedPref.getString("playername", "Username");
        String          gamename   = new Game_Helper().GAMENAME_GOSPRINGER;
        Database_Helper database   = new Database_Helper();
        Database_Score  scoreEntry = new Database_Score(gamename, currentCoverage, name, 1);
        database.addScoreToDatabase(scoreEntry);
    }

    private void setCurrentCoverage(int stepCounter) {
        currentCoverage = (int) Math.abs(100 * stepCounter / (N * N));
    }

    private int getCoverage() {
        return currentCoverage;
    }

    public int getFieldValue(int i, int j) {
        return fieldValues[i][j];
    }

    public void setFieldValue(int i, int j, int value) {
        fieldValues[i][j] = value;
    }

    private void print(String msg) {
        System.out.println(msg);
    }

    private void toastie(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private void toastie(String msg, boolean lng) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


} // end: MainActivity
