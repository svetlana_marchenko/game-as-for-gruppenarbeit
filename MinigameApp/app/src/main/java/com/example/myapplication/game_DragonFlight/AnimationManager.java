package com.example.myapplication.game_DragonFlight;

import android.graphics.Canvas;
import android.graphics.Rect;

public class AnimationManager {

    private Animation[] animations;
    private  int animationIndex = 0;

    public AnimationManager(Animation[] animations){
        this.animations = animations;
    }

    public void playEnum(int index){
        for(int i = 0; i< animations.length; i++){
            if(i == index){
                if(!animations[index].getIsPlaying()) {
                    animations[i].play();
                }
            } else {
                animations[i].stop();
            }
        }
        animationIndex = index;
    }

    public void draw(Canvas canvas, Rect rect){
        if(animations[animationIndex].getIsPlaying()) {
            animations[animationIndex].draw(canvas, rect);
        }
    }

    public void update(){
        if(animations[animationIndex].getIsPlaying()) {
            animations[animationIndex].update();
        }
    }
}
