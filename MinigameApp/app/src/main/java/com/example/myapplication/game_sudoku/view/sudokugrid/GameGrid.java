package com.example.myapplication.game_sudoku.view.sudokugrid;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.game_sudoku.Game_Sudoku;
import com.example.myapplication.game_sudoku.SudokuChecker;


public class GameGrid {
    private SudokuCell[][] Sudoku = new SudokuCell[9][9];

    private Context context;

    boolean won = false;

    int score = 0;


    public GameGrid( Context context ){
        this.context = context;
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++){
                Sudoku[x][y] = new SudokuCell(context);
            }
        }
    }

    // Fills the grid with values between 1 and 9 and sets them as unmodifiable
    public void setGrid( int[][] grid ){
        timer.start();
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++){
                Sudoku[x][y].setInitValue(grid[x][y]);
                if( grid[x][y] != 0 ){
                    Sudoku[x][y].setNotModifiable();
                    Sudoku[x][y].setBackgroundColor(Color.parseColor("#dcdcdc"));
                }
            }
        }
    }

    public SudokuCell[][] getGrid(){
        return Sudoku;
    }

    public SudokuCell getItem(int x , int y ){
        return Sudoku[x][y];
    }

    public SudokuCell getItem(int position ){
        int x = position % 9;
        int y = position / 9;

        return Sudoku[x][y];
    }

    public void setItem( int x , int y , int number ){
        Sudoku[x][y].setValue(number);
        Sudoku[x][y].setBackgroundColor(Color.parseColor("#ffffff"));
    }

    // Checks if all grid elements are filled with the correct value. if thats true, change the screen to a winning screen
    public void checkGame(){
        int [][] sudGrid = new int[9][9];
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++ ){
                sudGrid[x][y] = getItem(x,y).getValue();
            }
        }

        if( SudokuChecker.getInstance().checkSudoku(sudGrid)){
            Game_Sudoku sudoku = (Game_Sudoku) context;

            if (getRecord() > 0) {
                if (score<getRecord()) {
                    setRecord(score);
                }
            } else {
                setRecord(score);
            }

            sudoku.getResult().setVisibility(View.VISIBLE);
            sudoku.getResult().setText("CONGRATULATIONS! \n YOU WIN!");
            sudoku.getResult().setGravity(Gravity.CENTER);
            sudoku.getButtonsGridView().setVisibility(View.INVISIBLE);
            sudoku.getSudokuGridView().setVisibility(View.INVISIBLE);
            sudoku.getGridPic().setVisibility(View.INVISIBLE);
            sudoku.getTimeTv().setVisibility(View.INVISIBLE);
            sudoku.getNewGame().setVisibility(View.VISIBLE);
            sudoku.getCardviewTime().setVisibility(View.INVISIBLE);
            timer.cancel();
            won = true;
        }
    }

    public void checkNumbers() {

        int [][] sudGrid = new int[9][9];
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++ ){
                sudGrid[x][y] = getItem(x,y).getValue();
            }
        }
    }

    public boolean isWon() {
        return won;
    }

    // Get the record from the database
    private int getRecord() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.context);
        System.out.println(sharedPref.getInt("record", 0));
        return sharedPref.getInt("record", 0);
    }

    // Set a new record and save it to the database
    private void setRecord(int score) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("record", score);
        editor.commit();

        // Put record into database
        Game_Sudoku sudoku = (Game_Sudoku) context;
        String name = sharedPref.getString("playername", "Username");
        String gamename = new Game_Helper().GAMENAME_SUDUKO;
        Database_Helper database = new Database_Helper();
        Database_Score scoreEntry = new Database_Score(gamename, score, name, sudoku.getDifficulty());
        database.addScoreToDatabase(scoreEntry);
        System.out.println("RECORD PUT INTO DATABASE");
    }

    // timer that counts up
    long totalSeconds = 100000;
    long intervalSeconds = 1;

    CountDownTimer timer = new CountDownTimer(totalSeconds * 1000, intervalSeconds * 1000) {
        public void onTick(long millisUntilFinished) {
            Game_Sudoku activity = (Game_Sudoku) context;
            activity.getTimeTv().setText("" + (totalSeconds * 1000 - millisUntilFinished) / 1000);
            score++;
        }

        public void onFinish() {
        }
    };
}