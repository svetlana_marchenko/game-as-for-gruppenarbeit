// Alexander Becker

package com.example.myapplication.game_snake;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;
import com.example.myapplication.game_DragonFlight.GamePanel;
import com.example.myapplication.game_snake.Constants;


public class Game_Snake extends AppCompatActivity {

    SnakeThread snakeThread;
    private final int EASY   = 1;
    private final int MEDIUM = 2;
    private final int HARD   = 3;
    private       int difficulty;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    private long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.startTime = System.currentTimeMillis();

        SharedPreferences theme = getSharedPreferences("theme", 0);

        switch (theme.getInt("theme", 0)){
            case 1: setTheme(R.style.blue_theme); break;
            case 2: setTheme(R.style.green_theme); break;
            case 3: setTheme(R.style.dark_theme); break;
            default: setTheme(R.style.blue_theme); break;
        }

        super.onCreate(savedInstanceState);


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;

        setContentView(R.layout.activity_game_snake);

        FrameLayout game = (FrameLayout) findViewById(R.id.frameLayout);
      //  LinearLayout gameWidgets = new LinearLayout(this);

        // Get the pixel dimensions of the screen
        Display display = getWindowManager().getDefaultDisplay();
        // Initialize the result into a Point object
        Point size = new Point();
        display.getSize(size);

        // change here
        difficulty = MEDIUM;
        Bundle b = getIntent().getExtras();
        int diff = b.getInt("difficulty");
        if(diff == 1)       difficulty = EASY;
        else if(diff == 2)  difficulty = MEDIUM;
        else                difficulty = HARD;

        // Create a new instance of the SnakeEngine class
        snakeThread = new SnakeThread(this, size, difficulty);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        game.setZ(-1);
        game.addView(snakeThread);

    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    // Start the thread in snakeEngine
    @Override
    protected void onResume() {
        super.onResume();
        snakeThread.resume();
    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        snakeThread.pause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStop() {
        super.onStop();

        for(int i = 0; i < snakeThread.roundsPlayed; i++){
            ((GlobalVariables) this.getApplication()).addOneRound();
        }

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_snake(millis); // Hier setTime_deinSpiel
    }
}
