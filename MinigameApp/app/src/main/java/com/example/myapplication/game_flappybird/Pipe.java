package com.example.myapplication.game_flappybird;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.Random;

public class Pipe extends BaseObject {
    public static int SPEED;
    public Pipe(float x, float y, int width, int height) {
        super(x, y, width, height);
        SPEED = 10* Constants.SCREEN_WIDTH/1080;
    }

    // draws the pipes on the screen
    public void draw(Canvas canvas) {
        this.x-=SPEED;
        canvas.drawBitmap(this.bm, this.x, this.y, null);
    }

    // defines the y-values of the pipes
    public void randomY() {
        Random r = new Random();
        this.y = r.nextInt((0 + this.height / 4) + 1) - this.height/4;
    }

    @Override
    public void setBm(Bitmap bm) {
        this.bm = Bitmap.createScaledBitmap(bm, width, height, true);
    }
}
