package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import static android.app.UiModeManager.MODE_NIGHT_YES;

public class Settings extends AppCompatActivity {

    private ImageButton backToPrevious;
    private Button dark;
    private Button green;
    private Button blue;
    private EditText namefield;
    private Switch soundToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Get saved Theme out of Shared Preferances
        SharedPreferences theme = getSharedPreferences("theme", 0);

        switch (theme.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                break;
            case 2:
                setTheme(R.style.green_theme);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                break;
            default:
                setTheme(R.style.blue_theme);
                break;
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        boolean soundOn = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("sound", true);

        backToPrevious = findViewById(R.id.back);
        blue = findViewById(R.id.bluetheme);
        green = findViewById(R.id.greentheme);
        dark = findViewById(R.id.darktheme);
        soundToggle = findViewById(R.id.soundswitch);

        soundToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    unmute();
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                    pref.edit().putBoolean("sound", true).apply();
                } else {
                    mute();
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                    pref.edit().putBoolean("sound", false).apply();
                }
            }
        });

        soundToggle.setChecked(soundOn);


        backToPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBackToPrevious();
            }
        });

        blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTheme(1);
                changeTheme(2);
            }
        });

        green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTheme(2);
                changeTheme(1);
            }
        });

        dark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTheme(3);
                changeTheme(3);
            }
        });

        String pname = PreferenceManager.getDefaultSharedPreferences(this).getString("playername", "Username");

        namefield = findViewById(R.id.nameField);
        namefield.setText(pname);

        namefield.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                pref.edit().putString("playername", namefield.getText().toString()).apply();

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void goBackToPrevious() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void saveTheme(int themeID) {
        SharedPreferences theme = getSharedPreferences("theme", 0);
        SharedPreferences.Editor editor = theme.edit();

        editor.putInt("theme", themeID);
        editor.commit();
    }

    private void changeTheme(int theme) {
        ((GlobalVariables) this.getApplication()).setCurrentTheme(theme);
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        finish();
    }

    private void mute() {
        //mute audio
        AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
    }

    public void unmute() {
        //unmute audio
        AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        amanager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
    }

}