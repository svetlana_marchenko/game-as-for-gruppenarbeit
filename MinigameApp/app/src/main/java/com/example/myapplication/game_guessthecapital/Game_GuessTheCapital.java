// Alexander Becker

package com.example.myapplication.game_guessthecapital;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;

import android.content.Context;
import android.graphics.Color;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.*;

public class Game_GuessTheCapital extends AppCompatActivity {

    private int currentTheme;

    long startTime;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    private ImageView imageView;
    private TextView  textViewCountry;
    private Button    buttonNewGame;
    private Button    button1;
    private Button    button2;
    private Button    button3;
    private Button    button4;

    private ProgressBar progressBar;
    private TextView    textViewProgress;
    private TextView    textViewPercentage;
    private Handler     handler = new Handler();
    private TextView    textViewResult;


    private HashMap<String, String> capitalsEasy      = new HashMap<String, String>();
    private HashMap<String, String> capitalsMedium    = new HashMap<String, String>();
    private HashMap<String, String> capitalsHard      = new HashMap<String, String>();
    private ArrayList<String>       countryCollection = new ArrayList<String>();
    private HashMap<String, String> capitalsMap       = new HashMap<String, String>();

    private int    currentIndex = 0;
    private String currentKey;
    private String currentAnswer;

    private int numberToFinish = 40;

    private final int EASY   = 1;
    private final int MEDIUM = 2;
    private final int HARD   = 3;

    private int difficulty;

    private int counterCorrectAnswers;
    private int percentageCorrectAnswers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.startTime = System.currentTimeMillis();

        // Get saved Theme out of Shared Preferances
        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guessthecapital);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        imageView = findViewById(R.id.imageView);
        textViewCountry = findViewById(R.id.textViewCountry);
        buttonNewGame = findViewById(R.id.buttonNewGame);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        buttonNewGame.setOnClickListener(onClickListener);
        button1.setOnClickListener(onClickListener);
        button2.setOnClickListener(onClickListener);
        button3.setOnClickListener(onClickListener);
        button4.setOnClickListener(onClickListener);


        progressBar = findViewById(R.id.progressBar);
        textViewProgress = findViewById(R.id.textViewProgress);
        textViewPercentage = findViewById(R.id.textViewPercentage);
        textViewResult = findViewById(R.id.textViewResult);


        difficulty = EASY;
        Bundle b = getIntent().getExtras();
        int diff = b.getInt("difficulty");
        if(diff == 1)       difficulty = EASY;
        else if(diff == 2)  difficulty = MEDIUM;
        else                difficulty = HARD;

        newGame();

    } // end of onCreate

    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.buttonNewGame:
                    print("button clicked new game");
                    newGame();
                    break;
                case R.id.button1:
                    handleAnswer(button1);
                    break;
                case R.id.button2:
                    handleAnswer(button2);
                    break;
                case R.id.button3:
                    handleAnswer(button3);
                    break;
                case R.id.button4:
                    handleAnswer(button4);
                    break;
                default:
                    break;
            }

        }
    }; // View.OnClickListener


    private void initializeProgressBar() {
        new Thread(new Runnable() {
            public void run() {
//                while (currentIndex <= capitalsMap.size()) {
                while (currentIndex <= numberToFinish) {
                    // Update the progress bar and display the
                    //current value in the text view
//                    progressBar.setMax(capitalsMap.size());
                    progressBar.setMax(numberToFinish);
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(currentIndex);
                            textViewProgress.setText(currentIndex + "/" + progressBar.getMax());
//                            int percentage = Math.round(100 * currentIndex / capitalsMap.size());
                            int percentage = Math.round(100 * currentIndex / numberToFinish);
                            textViewPercentage.setText(percentage + " %");
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void newGame() {
        currentIndex = 0;
        currentKey = "";
        currentAnswer = "";
//        countryCollection.clear();
        capitalsMap.clear();
        counterCorrectAnswers = 0;
        percentageCorrectAnswers = 0;

        ((GlobalVariables) this.getApplication()).addOneRound();

        button1.setText("");
        button2.setText("");
        button3.setText("");
        button4.setText("");
        imageView.setImageDrawable(null);
        textViewCountry.setText("");
        textViewProgress.setText("");
        textViewPercentage.setText("");
        textViewResult.setText("");

        initializeCapitals(difficulty);

        initializeProgressBar();
        nextRound();
    }

    private void nextRound() {
        button1.setText("");
        button2.setText("");
        button3.setText("");
        button4.setText("");
        imageView.setImageDrawable(null);
        textViewCountry.setText("");

        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.colorPrimaryDark, typedValue, true);
        int color = typedValue.data;

        button1.setBackgroundColor(color);
        button2.setBackgroundColor(color);
        button3.setBackgroundColor(color);
        button4.setBackgroundColor(color);


        if (isFinished() == true) return;

//        String currentCountry = flags.get(currentIndex);
        String newKey = countryCollection.get(currentIndex);
        currentAnswer = capitalsMap.get(newKey);
        String fileNameFlag = "flag_" + newKey;
        currentIndex++;

        // get flag of current country and place it in imageview
        Context context = imageView.getContext();
        int     id      = context.getResources().getIdentifier(fileNameFlag, "drawable", context.getPackageName());
        imageView.setImageResource(id);

        textViewCountry.setText(newKey.replaceAll("_", " "));

        currentAnswer = currentAnswer.replaceAll("_", " ");

        // get random button for correct answer
        int random1to4 = getRandom1to4();
        // bound correct answer to button
        if (random1to4 == 1) button1.setText(currentAnswer);
        else if (random1to4 == 2) button2.setText(currentAnswer);
        else if (random1to4 == 3) button3.setText(currentAnswer);
        else if (random1to4 == 4) button4.setText(currentAnswer);

        // fill other 3 buttons with wrong answers (unequal to current, correct answer saved in string)
        if (!button1.getText().equals(currentAnswer)) button1.setText(getUnequalCapital());
        if (!button2.getText().equals(currentAnswer)) button2.setText(getUnequalCapital());
        if (!button3.getText().equals(currentAnswer)) button3.setText(getUnequalCapital());
        if (!button4.getText().equals(currentAnswer)) button4.setText(getUnequalCapital());
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_capitals(millis); // Hier setTime_deinSpiel

    }

    private boolean isFinished() {
//        if (currentIndex == capitalsMap.size()) {
        if (currentIndex == numberToFinish) {

            button1.setBackgroundColor(Color.GREEN);
            button2.setBackgroundColor(Color.GREEN);
            button3.setBackgroundColor(Color.GREEN);
            button4.setBackgroundColor(Color.GREEN);

//            textViewResult.setText("you have " + counterCorrectAnswers + " of " + capitalsMap.size() + " correct (" + percentageCorrectAnswers + "%)");
            textViewResult.setText("you have " + counterCorrectAnswers + " of " + numberToFinish + " correct (" + percentageCorrectAnswers + "%)");

            // send results to backend
            setRecord();

            return true;
        }

        return false;
    }


    private String getUnequalCapital() {
        int randomIndex = getRandomIndexForArrayList();
        while (capitalsMap.get(countryCollection.get(randomIndex)).equals(currentAnswer)) {
            randomIndex = getRandomIndexForArrayList();
        }
        return capitalsMap.get(countryCollection.get(randomIndex));
    }

    private void handleAnswer(View v) {
        Button buttonClicked = (Button) findViewById(v.getId());
        String clickedAnswer = buttonClicked.getText().toString();
        Button correctButton = getCorrectButton();

        @ColorInt int green = getResources().getColor(R.color.green_window);

        if (clickedAnswer.equals(currentAnswer)) {
            print("correct answer");

            buttonClicked.setBackgroundColor(green);
            counterCorrectAnswers++;
//            percentageCorrectAnswers = Math.round(100 * counterCorrectAnswers / capitalsMap.size());
            percentageCorrectAnswers = Math.round(100 * counterCorrectAnswers / numberToFinish);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // Actions to do after 3 seconds
                    nextRound();
                }
            }, 700);
        } else {
            print("wrong answer");
            buttonClicked.setBackgroundColor(Color.RED);
            correctButton.setBackgroundColor(green);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // Actions to do after 3 seconds
                    nextRound();
                }
            }, 2000);
        }

    }

    private Button getCorrectButton() {
        if (button1.getText().toString().equals(currentAnswer)) return button1;
        else if (button2.getText().toString().equals(currentAnswer)) return button2;
        else if (button3.getText().toString().equals(currentAnswer)) return button3;
        else return button4;
    }

    private int getRandom1to4() {
        // nextInt(4) gives 0...3
        // +1 makes 1...4
        return new Random().nextInt(4) + 1;
    }

    private int getRandomIndexForArrayList() {
        // generates an integer between 0 and max. size of ArrayList flags
        // will be used as index for ArrayList
        return new Random().nextInt(countryCollection.size());
    }

    public int getRecord() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        return sharedPref.getInt("Capitals", 0);
    } // getRecord


    private void setRecord() {
        SharedPreferences        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor     = sharedPref.edit();
        editor.putInt("Capitals", percentageCorrectAnswers);
        editor.commit();

        // Put record into database
        String          name       = sharedPref.getString("playername", "Username");
        String          gamename   = new Game_Helper().GAMENAME_CAPITALS;
        Database_Helper database   = new Database_Helper();
        Database_Score  scoreEntry = new Database_Score(gamename, percentageCorrectAnswers, name, difficulty);
        database.addScoreToDatabase(scoreEntry);
    } // setRecord


    private void print(String msg) {
        System.out.println(msg);
    }

    private void toastie(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private void initializeCapitals(int mode) {
        if (mode == 1)
            initializeEasy();
        else if (mode == 2)
            initializeMedium();
        else if (mode == 3)
            initializeHard();
    }


    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void initializeEasy() {
        // easy mode contains countries from europe, north and south america
        capitalsEasy.clear();

        // europe
        capitalsEasy.put("albania", "tirana");
        capitalsEasy.put("andorra", "andorra la vella");
        capitalsEasy.put("austria", "vienna");
        capitalsEasy.put("belarus", "minsk");
        capitalsEasy.put("belgium", "brussels");
        capitalsEasy.put("bosnia_and_herzegovina", "sarajevo");
        capitalsEasy.put("bulgaria", "sofia");
        capitalsEasy.put("croatia", "zagreb");
        capitalsEasy.put("cyprus", "nicosia");
        capitalsEasy.put("czech_republic", "prague");
        capitalsEasy.put("denmark", "copenhagen");
        capitalsEasy.put("england", "london");
        capitalsEasy.put("estonia", "tallinn");
        capitalsEasy.put("finland", "helsinki");
        capitalsEasy.put("france", "paris");
        capitalsEasy.put("germany", "berlin");
        capitalsEasy.put("greece", "athens");
        capitalsEasy.put("hungary", "budapest");
        capitalsEasy.put("iceland", "reykjavik");
        capitalsEasy.put("ireland", "dublin");
        capitalsEasy.put("italy", "rome");
        capitalsEasy.put("kosovo", "pristina");
        capitalsEasy.put("latvia", "riga");
        capitalsEasy.put("liechtenstein", "vaduz");
        capitalsEasy.put("lithuania", "vilnius");
        capitalsEasy.put("luxembourg", "luxembourg");
        capitalsEasy.put("malta", "valletta");
        capitalsEasy.put("moldova", "chisinau");
        capitalsEasy.put("monaco", "monaco");
        capitalsEasy.put("montenegro", "podgorica");
        capitalsEasy.put("netherlands", "amsterdam");
        capitalsEasy.put("north_macedonia", "skopje");
        capitalsEasy.put("northern_ireland", "belfast");
        capitalsEasy.put("norway", "oslo");
        capitalsEasy.put("poland", "warsaw");
        capitalsEasy.put("portugal", "lisbon");
        capitalsEasy.put("romania", "bucharest");
        capitalsEasy.put("russia", "moscow");
        capitalsEasy.put("san_marino", "san marino");
        capitalsEasy.put("scotland", "glasgow");
        capitalsEasy.put("serbia", "belgrade");
        capitalsEasy.put("slovakia", "bratislava");
        capitalsEasy.put("slovenia", "ljubljana");
        capitalsEasy.put("spain", "madrid");
        capitalsEasy.put("sweden", "stockholm");
        capitalsEasy.put("switzerland", "bern");
        capitalsEasy.put("ukraine", "kiev");
        capitalsEasy.put("united_kingdom", "london");
        capitalsEasy.put("vatican", "vatican city");
        capitalsEasy.put("wales", "cardiff");


        // north america
        capitalsEasy.put("canada", "ottawa");
        capitalsEasy.put("mexico", "mexico city");
        capitalsEasy.put("usa", "washington d.c.");

        // south america
        capitalsEasy.put("argentina", "buenos aires");
        capitalsEasy.put("bolivia", "sucre");
        capitalsEasy.put("brazil", "brasilia");
        capitalsEasy.put("chile", "santiago");
        capitalsEasy.put("colombia", "bogota");
        capitalsEasy.put("ecuador", "quito");
        capitalsEasy.put("guyana", "georgetown");
        capitalsEasy.put("paraguay", "asuncion");
        capitalsEasy.put("peru", "lima");
        capitalsEasy.put("suriname", "paramaribo");
        capitalsEasy.put("uruguay", "paramaribo");
        capitalsEasy.put("venezuela", "caracas");


        countryCollection = new ArrayList(capitalsEasy.keySet());
        // randomize all entries, so there is no particular order
        Collections.shuffle(countryCollection);

        capitalsMap = capitalsEasy;
    }

    private void initializeMedium() {
        // medium mode contains countries from asia and africa

        capitalsMedium.clear();

        // africa
        capitalsMedium.put("algeria", "algiers");
        capitalsMedium.put("angola", "luanda");
        capitalsMedium.put("benin", "porto-novo");
        capitalsMedium.put("botswana", "gaborone");
        capitalsMedium.put("burkina_faso", "ouagadougou");
        capitalsMedium.put("burundi", "bujumbura");
        capitalsMedium.put("cameroon", "yaounde");
        capitalsMedium.put("cape_verde", "praia");
        capitalsMedium.put("central_african_republic", "bangui");
        capitalsMedium.put("chad", "n'djamena");
        capitalsMedium.put("comoros", "moroni");
        capitalsMedium.put("congo", "kinshasa");
        capitalsMedium.put("cote_d_ivoire", "yamoussoukro");
        capitalsMedium.put("djibouti", "djibouti city");
        capitalsMedium.put("egypt", "cairo");
        capitalsMedium.put("equatorial_guinea", "malabo");
        capitalsMedium.put("eritrea", "asmara");
        capitalsMedium.put("ethiopia", "addis ababa");
        capitalsMedium.put("gabon", "libreville");
        capitalsMedium.put("gambia", "banjul");
        capitalsMedium.put("ghana", "accra");
        capitalsMedium.put("guinea", "conakry");
        capitalsMedium.put("guinea_bissau", "bissau");
        capitalsMedium.put("kenya", "nairobi");
        capitalsMedium.put("lesotho", "maseru");
        capitalsMedium.put("liberia", "monrovia");
        capitalsMedium.put("libya", "tripoli");
        capitalsMedium.put("madagascar", "antananarivo");
        capitalsMedium.put("malawi", "lilongwe");
        capitalsMedium.put("mali", "bamako");
        capitalsMedium.put("mauritania", "nouakchott");
        capitalsMedium.put("mauritius", "port louis");
        capitalsMedium.put("morocco", "rabat");
        capitalsMedium.put("mozambique", "maputo");
        capitalsMedium.put("namibia", "windhoek");
        capitalsMedium.put("niger", "niamey");
        capitalsMedium.put("nigeria", "abuja");
        capitalsMedium.put("republic_of_congo", "brazzaville");
        capitalsMedium.put("rwanda", "kigali");
        capitalsMedium.put("saint_thomas_and_prince", "sao tome");
        capitalsMedium.put("senegal", "dakar");
        capitalsMedium.put("seychelles", "victoria");
        capitalsMedium.put("sierra_leone", "freetown");
        capitalsMedium.put("somalia", "mogadishu");
        capitalsMedium.put("south_africa", "pretoria / cape town");
        capitalsMedium.put("south_sudan", "juba");
        capitalsMedium.put("sudan", "khartoum");
        capitalsMedium.put("swaziland", "mbabane");
        capitalsMedium.put("tanzania", "dodoma");
        capitalsMedium.put("togo", "lome");
        capitalsMedium.put("tunisia", "tunis");
        capitalsMedium.put("uganda", "kampala");
        capitalsMedium.put("western_sahara", "el aaiun");
        capitalsMedium.put("zambia", "lusaka");
        capitalsMedium.put("zimbabwe", "harare");

        // asia
        capitalsMedium.put("afghanistan", "kabul");
        capitalsMedium.put("armenia", "yerevan");
        capitalsMedium.put("australia", "canberra");
        capitalsMedium.put("azerbaijan", "baku");
        capitalsMedium.put("bahrain", "manama");
        capitalsMedium.put("bangladesh", "dhaka");
        capitalsMedium.put("bhutan", "thimphu");
        capitalsMedium.put("brunei", "bandar seri begawan");
        capitalsMedium.put("cambodia", "phnom penh");
        capitalsMedium.put("china", "beijing");
        capitalsMedium.put("east_timor", "dili");
        capitalsMedium.put("georgia", "tbilisi");
        capitalsMedium.put("hong_kong", "hong kong");
        capitalsMedium.put("india", "new delhi");
        capitalsMedium.put("indonesia", "jakarta");
        capitalsMedium.put("iran", "tehran");
        capitalsMedium.put("iraq", "baghdad");
        capitalsMedium.put("israel", "jerusalem");
        capitalsMedium.put("japan", "tokyo");
        capitalsMedium.put("jordan", "amman");
        capitalsMedium.put("kazakhstan", "nur-sultan");
        capitalsMedium.put("kuwait", "kuwait city");
        capitalsMedium.put("kyrgyzstan", "bishkek");
        capitalsMedium.put("laos", "vientiane");
        capitalsMedium.put("lebanon", "beirut");
        capitalsMedium.put("malaysia", "kuala lumpur");
        capitalsMedium.put("maldives", "male");
        capitalsMedium.put("mongolia", "ulaanbaatar");
        capitalsMedium.put("myanmar", "nay pyi taw");
        capitalsMedium.put("nepal", "kathmandu");
        capitalsMedium.put("new_zealand", "wellington");
        capitalsMedium.put("north_korea", "pyongyang");
        capitalsMedium.put("oman", "muscat");
        capitalsMedium.put("pakistan", "islamabad");
        capitalsMedium.put("palestine", "east jerusalem");
        capitalsMedium.put("philippines", "manila");
        capitalsMedium.put("qatar", "doha");
        capitalsMedium.put("saudi_arabia", "riyadh");
        capitalsMedium.put("singapore", "singapore");
        capitalsMedium.put("south_korea", "seoul");
        capitalsMedium.put("sri_lanka", "colombo");
        capitalsMedium.put("syria", "damascus");
        capitalsMedium.put("taiwan", "taipeh");
        capitalsMedium.put("tajikistan", "dushanbe");
        capitalsMedium.put("thailand", "bangkok");
        capitalsMedium.put("turkey", "ankara");
        capitalsMedium.put("turkmenistan", "ashgabat");
        capitalsMedium.put("united_arab_emirates", "abu dhabi");
        capitalsMedium.put("uzbekistan", "tashkent");
        capitalsMedium.put("vietman", "hanoi");
        capitalsMedium.put("yemen", "sana'a");


        countryCollection = new ArrayList(capitalsMedium.keySet());
        // randomize all entries, so there is no particular order
        Collections.shuffle(countryCollection);

        capitalsMap = capitalsMedium;
    }

    private void initializeHard() {
        // hard mode contain countries from central america, caribic islands and small oceanic islands

        capitalsHard.clear();

        // oceanic islands
        capitalsHard.put("aland", "mariehamm");
        capitalsHard.put("american_samoa", "pago pago");
        capitalsHard.put("anguilla", "the valley");
        capitalsHard.put("antarctica", "[no capital city]");
        capitalsHard.put("aruba", "oranjestad");
        capitalsHard.put("bermuda", "hamilton");
        capitalsHard.put("bonaire", "kralendijk");
        capitalsHard.put("british_indian_ocean_territory", "camp justice");
        capitalsHard.put("british_virgin_islands", "road town");
        capitalsHard.put("cayman_islands", "george town");
        capitalsHard.put("christmas_island", "flying fish cove");
        capitalsHard.put("cocos_islands", "west island");
        capitalsHard.put("cook_islands", "avarua");
        capitalsHard.put("curacao", "willemstad");
        capitalsHard.put("falkland_islands", "stanley");
        capitalsHard.put("faroe_islands", "torshavn");
        capitalsHard.put("fiji", "suva");
        capitalsHard.put("french_guiana", "cayenne");
        capitalsHard.put("french_polynesia", "papeete");
//        capitalsHard.put("french_southern_and_antarctic_lands", "");
        capitalsHard.put("gibraltar", "gibraltar");
        capitalsHard.put("greenland", "nuuk");
        capitalsHard.put("guadeloupe", "basse-terre");
        capitalsHard.put("guam", "hagatna");
        capitalsHard.put("guernsey", "st peter port");
        capitalsHard.put("isle_of_man", "douglas");
        capitalsHard.put("jersey_island", "saint helier");
        capitalsHard.put("kiribati", "south tarawa");
        capitalsHard.put("macau", "beijing");
        capitalsHard.put("marshall_islands", "majuro");
        capitalsHard.put("martinique", "fort-de-france");
        capitalsHard.put("mayotte", "mamoudzou");
        capitalsHard.put("micronesia", "palikir");
        capitalsHard.put("montserrat", "plymouth / brades");
        capitalsHard.put("nauru", "yaren district");
        capitalsHard.put("new_caledonia", "noumã©a");
        capitalsHard.put("niue", "alofi");
        capitalsHard.put("norfolk_island", "kingston");
        capitalsHard.put("northern_mariana_islands", "saipan");
        capitalsHard.put("palau", "ngerulmud");
        capitalsHard.put("papua_new_guinea", "port moresby");
        capitalsHard.put("pitcairn_islands", "adamstown");
        capitalsHard.put("puerto_rico", "san juan");
        capitalsHard.put("reunion", "saint-denis");
        capitalsHard.put("saint_barthelemy", "gustavia");
        capitalsHard.put("saint_pierre_and_miquelon", "saint-pierre");
        capitalsHard.put("samoa", "apia");
        capitalsHard.put("sint_maarten", "philipsburg");
        capitalsHard.put("solomon_islands", "honiara");
        capitalsHard.put("southern_rhodesia", "salisbury");
        capitalsHard.put("south_georgia_and_the_south_sandwich_islands", "king edward point");
        capitalsHard.put("tokelau", "nukunonu");
        capitalsHard.put("tonga", "nukuê»alofa");
        capitalsHard.put("turks_and_caicos_islands", "cockburn town");
        capitalsHard.put("tuvalu", "funafuti");
        capitalsHard.put("united_states_virgin_islands", "charlotte amalie");
        capitalsHard.put("vanuatu", "port vila");
        capitalsHard.put("wallis_and_futuna", "mata-utu");

        //central america and caribic
        capitalsHard.put("antigua_and_barbuda", "saint john's");
        capitalsHard.put("bahamas", "nassau");
        capitalsHard.put("barbados", "bridgetown");
        capitalsHard.put("belize", "belmopan");
        capitalsHard.put("costa_rica", "san jose");
        capitalsHard.put("cuba", "havana");
        capitalsHard.put("dominica", "roseau");
        capitalsHard.put("dominican_republic", "santo domingo");
        capitalsHard.put("el_salvador", "san salvador");
        capitalsHard.put("grenada", "saint george's");
        capitalsHard.put("guatemala", "guatemala city");
        capitalsHard.put("haiti", "port-au-prince");
        capitalsHard.put("honduras", "tegucigalpa");
        capitalsHard.put("jamaica", "kingston");
        capitalsHard.put("nicaragua", "managua");
        capitalsHard.put("panama", "panama city");
        capitalsHard.put("saint_kitts_and_nevis", "basseterre");
        capitalsHard.put("saint_lucia", "castries");
        capitalsHard.put("saint_vincent_and_grenadines", "kingstown");
        capitalsHard.put("trinidad_and_tobago", "port of spain");

        countryCollection = new ArrayList(capitalsHard.keySet());
        // randomize all entries, so there is no particular order
        Collections.shuffle(countryCollection);

        capitalsMap = capitalsHard;
    }

} // end of class
