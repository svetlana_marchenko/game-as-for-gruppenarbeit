// Alexander Becker

package com.example.myapplication.game_xobig;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;

import java.util.Random;

public class Game_XObig extends AppCompatActivity {

    long startTime;
    private int currentTheme;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    // eine 9x9-Matrix: 40 Pixel pro Feld -> 360 Pixel (maximale Breite)
//    private final int N = 10;
    private final int N = 9;

    private final int             ELEMENTS_TO_WIN = 5;
    // kann am Anfang gewählt werden, 3-5 to win
    // private final int      ELEMENTS_TO_WIN = 4;
    // private final int      ELEMENTS_TO_WIN = 4;
    private       int[][]         fieldValues     = new int[N][N];
    private final int             FLAG_O          = 0;   // symbolisiert das "o"
    private final int             FLAG_X          = 1;   // symbolisiert das "x"
    private       int             currentPlayer;
    private       TextView        tvCurrentPlayer;
    private       ImageButton[][] buttons;
    private       Button          buttonNewGame;
    private final int             BUTTON_ID[][]   =
            {{R.id.button_00, R.id.button_01, R.id.button_02, R.id.button_03, R.id.button_04, R.id.button_05, R.id.button_06, R.id.button_07, R.id.button_08},
                    {R.id.button_10, R.id.button_11, R.id.button_12, R.id.button_13, R.id.button_14, R.id.button_15, R.id.button_16, R.id.button_17, R.id.button_18},
                    {R.id.button_20, R.id.button_21, R.id.button_22, R.id.button_23, R.id.button_24, R.id.button_25, R.id.button_26, R.id.button_27, R.id.button_28},
                    {R.id.button_30, R.id.button_31, R.id.button_32, R.id.button_33, R.id.button_34, R.id.button_35, R.id.button_36, R.id.button_37, R.id.button_38},
                    {R.id.button_40, R.id.button_41, R.id.button_42, R.id.button_43, R.id.button_44, R.id.button_45, R.id.button_46, R.id.button_47, R.id.button_48},
                    {R.id.button_50, R.id.button_51, R.id.button_52, R.id.button_53, R.id.button_54, R.id.button_55, R.id.button_56, R.id.button_57, R.id.button_58},
                    {R.id.button_60, R.id.button_61, R.id.button_62, R.id.button_63, R.id.button_64, R.id.button_65, R.id.button_66, R.id.button_67, R.id.button_68},
                    {R.id.button_70, R.id.button_71, R.id.button_72, R.id.button_73, R.id.button_74, R.id.button_75, R.id.button_76, R.id.button_77, R.id.button_78},
                    {R.id.button_80, R.id.button_81, R.id.button_82, R.id.button_83, R.id.button_84, R.id.button_85, R.id.button_86, R.id.button_87, R.id.button_88},};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.startTime = System.currentTimeMillis();

        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xobig);

        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        tvCurrentPlayer = findViewById(R.id.tvCurrentPlayer);

//        buttonNewGame = findViewById(R.id.buttonNewGame);
//        buttonNewGame.setOnClickListener(onClickListener);
//
//        buttons = new ImageButton[N][N];
//        for (int i = 0; i < N; i++) {
//            for (int j = 0; j < N; j++) {
//                buttons[i][j] = findViewById(BUTTON_ID[i][j]);
//                buttons[i][j].setOnClickListener(onClickListener);
//                // mit grauem/weißem Standard-Bild hinterlegen
//            }
//        }

        newGame();

    } // Ende: onCreate

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    private void initializeButtons(){
        buttonNewGame = findViewById(R.id.buttonNewGame);
        buttonNewGame.setOnClickListener(onClickListener);

        buttons = new ImageButton[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                buttons[i][j] = findViewById(BUTTON_ID[i][j]);
                buttons[i][j].setOnClickListener(onClickListener);
                // mit grauem/weißem Standard-Bild hinterlegen
            }
        }
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // ClickListener der Buttons setzen, die die Spielkarten repraesentieren
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (v.getId() == BUTTON_ID[i][j]) {
                        // move(i, j);
                        updateField(i, j);
                    }
                }
            }

            if (v.getId() == R.id.buttonNewGame) {
                newGame();
            }

        } // onClick
    }; // View.OnClickListener

    private void newGame() {
        ((GlobalVariables) this.getApplication()).addOneRound();

        initializeButtons();

        Random random = new Random();
        // liefert zufällige Zahl aus [0, 1]: dieser Spieler fängt an
        this.currentPlayer = random.nextInt(1);
        if (currentPlayer == 0) {
            tvCurrentPlayer.setText("[O]");
        } else {
            tvCurrentPlayer.setText("[X]");
        }


        // alle Felder initialisieren
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                fieldValues[i][j] = -10;
                buttons[i][j].setImageResource(R.drawable.xobig_white);
            }
        }
    } // newGame

    @Override
    protected void onStop() {
        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_xobig(millis); // Hier setTime_deinSpiel

    }


    private void updateField(int row, int column) {

        if (getFieldValue(row, column) == 0 || getFieldValue(row, column) == 1) {
            // Feld bereits belegt, es soll nichts passieren
            return;
        }

        if (getCurrentPlayer() == 0) {
            setFieldValue(row, column, FLAG_O);
            buttons[row][column].setImageResource(R.drawable.xobig_o);
            checkFinished(FLAG_O);
            setCurrentPlayer(FLAG_X);
            tvCurrentPlayer.setText("[X]");
        } else {
            setFieldValue(row, column, FLAG_X);
            buttons[row][column].setImageResource(R.drawable.xobig_x);
            checkFinished(FLAG_X);
            setCurrentPlayer(FLAG_O);
            tvCurrentPlayer.setText("[O]");
        }

    } // updateField


    private void checkFinished(int currPlayer) {
        boolean won = false;
//        ImageView/Image/Bild image_winner;
//        R.drawable image_winner;
        int image_winner;
        if (currPlayer == 0)
            image_winner = R.drawable.xobig_o_winner;
        else
            image_winner = R.drawable.xobig_x_winner;


        // horizontal prüfen
        print("horizontal");
        for (int r = 0; r < N; r++) {
            for (int c = 0; c <= N - ELEMENTS_TO_WIN; c++) {
                // die kürzere if-Abfrage ist für ELEMENTS_TO_WIN = 3... die längere ist für = 4
                // if(getFieldValue(r, c) == currPlayer && getFieldValue(r, c+1) == currPlayer && getFieldValue(r, c+2) == currPlayer){
//                if(getFieldValue(r, c) == currPlayer && getFieldValue(r, c+1) == currPlayer && getFieldValue(r, c+2) == currPlayer && getFieldValue(r, c+3) == currPlayer){
                if (getFieldValue(r, c) == currPlayer && getFieldValue(r, c + 1) == currPlayer && getFieldValue(r, c + 2) == currPlayer && getFieldValue(r, c + 3) == currPlayer && getFieldValue(r, c + 4) == currPlayer) {
                    // das geht mit for-schleife auch eleganter
                    buttons[r][c].setImageResource(image_winner);
                    buttons[r][c + 1].setImageResource(image_winner);
                    buttons[r][c + 2].setImageResource(image_winner);
                    buttons[r][c + 3].setImageResource(image_winner);
                    buttons[r][c + 4].setImageResource(image_winner);
                    won = true;
                }
            }
        }
        print("horizontal ende");

        print("vertikal");
        // vertikal prüfen
        for (int c = 0; c < N; c++) {
            for (int r = 0; r <= N - ELEMENTS_TO_WIN; r++) {
                // die kürzere if-Abfrage ist für ELEMENTS_TO_WIN = 3... die längere ist für = 4
                // if(getFieldValue(r, c) == currPlayer && getFieldValue(r+1, c) == currPlayer && getFieldValue(r+2, c) == currPlayer){
//                if(getFieldValue(r, c) == currPlayer && getFieldValue(r+1, c) == currPlayer && getFieldValue(r+2, c) == currPlayer && getFieldValue(r+3, c) == currPlayer){
                if (getFieldValue(r, c) == currPlayer && getFieldValue(r + 1, c) == currPlayer && getFieldValue(r + 2, c) == currPlayer && getFieldValue(r + 3, c) == currPlayer && getFieldValue(r + 4, c) == currPlayer) {
                    buttons[r][c].setImageResource(image_winner);
                    buttons[r + 1][c].setImageResource(image_winner);
                    buttons[r + 2][c].setImageResource(image_winner);
                    buttons[r + 3][c].setImageResource(image_winner);
                    buttons[r + 4][c].setImageResource(image_winner);
                    won = true;
                }
            }
        }
        print("vertikal ende");

        print("diagonal unten rechts");
        // diagonal prüfen (nach unten-rechts)
        for (int r = 0; r <= N - ELEMENTS_TO_WIN; r++) {
            for (int c = 0; c <= N - ELEMENTS_TO_WIN; c++) {
                // die kürzere if-Abfrage ist für ELEMENTS_TO_WIN = 3... die längere ist für = 4
                // if(getFieldValue(r, c) == currPlayer && getFieldValue(r+1, c+1) == currPlayer && getFieldValue(r+2, c+2) == currPlayer){
//                if(getFieldValue(r, c) == currPlayer && getFieldValue(r+1, c+1) == currPlayer && getFieldValue(r+2, c+2) == currPlayer && getFieldValue(r+3, c+3) == currPlayer){
                if (getFieldValue(r, c) == currPlayer && getFieldValue(r + 1, c + 1) == currPlayer && getFieldValue(r + 2, c + 2) == currPlayer && getFieldValue(r + 3, c + 3) == currPlayer && getFieldValue(r + 4, c + 4) == currPlayer) {
                    buttons[r][c].setImageResource(image_winner);
                    buttons[r + 1][c + 1].setImageResource(image_winner);
                    buttons[r + 2][c + 2].setImageResource(image_winner);
                    buttons[r + 3][c + 3].setImageResource(image_winner);
                    buttons[r + 4][c + 4].setImageResource(image_winner);
                    won = true;
                }
            }
        }
        print("diagonal unten rechts ende");

        print("diagonal unten links");
        // vertikal prüfen (nach unten-links)
        for (int r = 0; r <= N - ELEMENTS_TO_WIN; r++) {
            for (int c = ELEMENTS_TO_WIN - 1; c < N; c++) {
                // die kürzere if-Abfrage ist für ELEMENTS_TO_WIN = 3... die längere ist für = 4
                // if(getFieldValue(r, c) == currPlayer && getFieldValue(r+1, c-1) == currPlayer && getFieldValue(r+2, c-2) == currPlayer){
//                if(getFieldValue(r, c) == currPlayer && getFieldValue(r+1, c-1) == currPlayer && getFieldValue(r+2, c-2) == currPlayer && getFieldValue(r+3, c-3) == currPlayer){
                if (getFieldValue(r, c) == currPlayer && getFieldValue(r + 1, c - 1) == currPlayer && getFieldValue(r + 2, c - 2) == currPlayer && getFieldValue(r + 3, c - 3) == currPlayer && getFieldValue(r + 4, c - 4) == currPlayer) {
                    buttons[r][c].setImageResource(image_winner);
                    buttons[r + 1][c - 1].setImageResource(image_winner);
                    buttons[r + 2][c - 2].setImageResource(image_winner);
                    buttons[r + 3][c - 3].setImageResource(image_winner);
                    buttons[r + 4][c - 4].setImageResource(image_winner);
                    won = true;
                }
            }
        }
        print("diagonal unten links ende");

        if(won == true){
            for (int r = 0; r < N; r++) {
                for (int c = 0; c < N; c++) {
                    buttons[r][c].setOnClickListener(null);
                }
            }
        }



    } // checkFinished


    private int getCurrentPlayer() {
        return currentPlayer;
    }

    private void setCurrentPlayer(int value) {
        this.currentPlayer = value;
    }

    public int getFieldValue(int i, int j) {
        return fieldValues[i][j];
    }

    public void setFieldValue(int i, int j, int value) {
        fieldValues[i][j] = value;
    }

    private void print(String msg) {
        System.out.println(msg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


} // Ende: MainActivity
