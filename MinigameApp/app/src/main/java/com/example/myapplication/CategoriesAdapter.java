package com.example.myapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CategoriesAdapter extends BaseAdapter {

    private final List<Game> listData;
    private final LayoutInflater layoutInflater;
    private final Context context;

    public CategoriesAdapter(Context context, List<Game> listData) {
        this.listData = listData;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.activity_grid_item, null);
            holder = new ViewHolder();
            holder.gamePicView = convertView.findViewById(R.id.iv_gamePic);
            holder.gameNameView = convertView.findViewById(R.id.tv_gameName);
            holder.highScoreView = convertView.findViewById(R.id.tv_wedontknowyet);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Game game = this.listData.get(position);
        holder.gameNameView.setText(game.getGameName());
        holder.highScoreView.setText("" + game.gethighScore());

        int imageId = this.getMipmapResIdByName(game.getGamePicName());

        holder.gamePicView.setImageResource(imageId);

        return convertView;
    }

    public int getMipmapResIdByName(String resName) {

        String pkgName = context.getPackageName();

        // Return 0 if not found.
        int resID = context.getResources().getIdentifier(resName, "mipmap", pkgName);
        Log.i("CategoriesAdapter", "Res Name: " + resName + "==> Res ID = " + resID);
        return resID;

    }

    static class ViewHolder {
        ImageView gamePicView;
        TextView gameNameView;
        TextView highScoreView;
    }

}
