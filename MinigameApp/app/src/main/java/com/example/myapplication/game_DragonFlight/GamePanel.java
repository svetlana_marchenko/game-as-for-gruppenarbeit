package com.example.myapplication.game_DragonFlight;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

import androidx.annotation.NonNull;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {

    private MainThread thread;
    private Rect r = new Rect();
    public int roundsPlayed;

    private RectPlayer player;
    private Point playerPoint;
    private ObstacleManager obstacleManager;
    private BackgroundManager backgroundManager;

    private boolean movingPlayer = false;

    private boolean gameOver = false;
    private long gameOverTime;
    private long startTime;

    public GamePanel(Context context) {
        super(context);

        roundsPlayed = 1;

        getHolder().addCallback(this);

        Constants.CURRENT_CONTEXT = context;

        thread = new MainThread(getHolder(), this);

        BitmapFactory bf = new BitmapFactory();
        Bitmap idleImage = bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.dragonflight_galaxy);
        backgroundManager = new BackgroundManager(idleImage);

        player = new RectPlayer(new Rect(100, 100, 250, 239), Color.rgb(255, 0, 0));
        playerPoint = new Point(Constants.SCREEN_WIDTH / 2, 3 * Constants.SCREEN_HEIGHT / 4);
        player.update(playerPoint);
        this.startTime = System.currentTimeMillis();
        obstacleManager = new ObstacleManager(250, 500, 75, Color.WHITE);

        setFocusable(true);
    }

    public void reset() {

        roundsPlayed++;

        BitmapFactory bf = new BitmapFactory();
        /*
            Author:
            freedohm
            https://opengameart.org/content/nebulus
            license: http://creativecommons.org/licenses/by/3.0/
         */
        Bitmap idleImage = bf.decodeResource(Constants.CURRENT_CONTEXT.getResources(), R.drawable.dragonflight_galaxy);
        backgroundManager = new BackgroundManager(idleImage);

        playerPoint = new Point(Constants.SCREEN_WIDTH / 2, 3 * Constants.SCREEN_HEIGHT / 4);
        player.update(playerPoint);

        obstacleManager = new ObstacleManager(250, 500, 75, Color.WHITE);
        movingPlayer = false;
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        thread = new MainThread(getHolder(), this);

        Constants.INIT_TIME = System.currentTimeMillis();

        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (!gameOver && player.getRectangle().contains((int) event.getX(), (int) event.getY())) {
                    movingPlayer = true;
                }
                if (gameOver && System.currentTimeMillis() - gameOverTime >= 1000) {
                    reset();
                    gameOver = false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (movingPlayer && !gameOver) {
                    playerPoint.set((int) event.getX(), (int) event.getY());
                }
                break;
            case MotionEvent.ACTION_UP:
                movingPlayer = false;
                break;
        }
        return true;

    }

    public void update() {
        if (!gameOver) {
            //decides whether background is animated or not.
            backgroundManager.update();
            player.update(playerPoint);
            obstacleManager.update();
            if (obstacleManager.playerCollide(player)) {
                try {
                    /*
                    Sound Effect by:
                    Michel Baradari
                    (Submitted by qubodup)
                    https://opengameart.org/content/15-monster-gruntpaindeath-sounds
                    published here: http://cubeengine.com/forum.php4?action=display_thread&thread_id=2164
                    license: http://creativecommons.org/licenses/by/3.0/
                     */
                MediaPlayer mp = MediaPlayer.create(getContext(), R.raw.dragonflightdeath);
                mp.start();
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }

                    ;
                });

            } catch(Exception e){
                e.printStackTrace();
            }
            gameOver = true;
            gameOverTime = System.currentTimeMillis();
        }
    } }



    @Override
    public void draw(Canvas canvas){

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(GamePanel.this.getContext());
        SharedPreferences.Editor ed = pref.edit();
        Typeface plain = Typeface.createFromAsset(Constants.CURRENT_CONTEXT.getAssets(),"secular_one.ttf");

        if(canvas != null) {
            super.draw(canvas);

            backgroundManager.draw(canvas);
            player.draw(canvas);
            obstacleManager.draw(canvas);
            int score = obstacleManager.getScore();

            if(!gameOver) {
                Paint paint = new Paint();
                paint.setTextSize(100);
                paint.setColor(Color.WHITE);
                paint.setAlpha(70);
                paint.setTypeface(plain);
                drawCenterText(canvas, paint,"" + score , 200);
            }else {

                if(pref.getInt("highscore", -1) == -1){
                    ed.putInt("highscore", score);
                    ed.commit();
                } else {
                    if(score > pref.getInt("highscore", -1)) {
                        ed.putInt("highscore", score);
                        ed.commit();

                        Looper.prepare();

                        // Put record into database
                        String name = pref.getString("playername", "Username");
                        String gamename = new Game_Helper().GAMENAME_DRAGONFLIGHT;
                        Database_Helper database = new Database_Helper();
                        Database_Score scoreEntry = new Database_Score(gamename, score, name, 1);
                        database.addScoreToDatabase(scoreEntry);
                    }
                }


                Paint paint2 = new Paint();

                paint2.setTextSize(70);
                paint2.setColor(Color.GRAY);
                RectF r = new RectF(100, 400, Constants.SCREEN_WIDTH-100, 700);
                canvas.drawRoundRect(r, 20, 20, paint2);
                paint2.setColor(Color.BLACK);
                paint2.setTypeface(plain);
                drawCenterText(canvas, paint2,"You're score: " + score, 520);
                drawCenterText(canvas, paint2,"Highscore: " + pref.getInt("highscore", -1), 620);

                paint2.setColor(Color.WHITE);
                paint2.setTextSize(100);
                drawCenterText(canvas, paint2,"Tap to play again!", 900);
            }
        }
    }


    private void drawCenterText(Canvas canvas, Paint paint, String text, float yVal) {
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.getTextBounds(text, 0, text.length(), r);

        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y;
        if(yVal < 0){
            y = cHeight / 2f + r.height() / 2f - r.bottom;
        } else {
            y = yVal;
        }
        canvas.drawText(text, x, y, paint);

    }
}
