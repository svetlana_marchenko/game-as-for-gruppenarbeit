package com.example.myapplication.game_2048;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.view.ContextThemeWrapper;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Database_Helper;
import com.example.myapplication.Database_Score;
import com.example.myapplication.Game_Helper;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;
import com.example.myapplication.game_DragonFlight.Animation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static java.lang.Integer.parseInt;


public class Game_2048 extends AppCompatActivity {

    private int currentTheme;

    HashMap<String, Integer> map = null;
    HashMap<String, Integer> prevmap = null;
    GridView grid = null;
    GridView grid2 = null;
    String direction = "";
    private ImageButton backToMenu;
    private ImageButton toSettings;
    long startTime;

    public Game_2048(HashMap m)
    {
        map=m;
    }
    public Game_2048()
    {
        map=new HashMap<>();
    }

    int score, bestscore = 0;
    boolean timerStop = false;
    TextView scoreTv, timeTv = null, highscoreTv;
    ImageButton button, button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Add one Round to the database everytime a game is playes
        this.startTime = System.currentTimeMillis();
        ((GlobalVariables) this.getApplication()).addOneRound();

        // Get saved Theme out of Shared Preferances
        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_2048);
        scoreTv=(TextView) findViewById(R.id.score_2048);
        timeTv = (TextView) findViewById(R.id.time_2048);
        highscoreTv = (TextView) findViewById(R.id.highscore_2048);
        grid=(GridView)findViewById(R.id.gamebackground);
        grid2=(GridView)findViewById(R.id.gamebackground2);
        button = (ImageButton) findViewById(R.id.button_reset);

        // Get the best score
        SharedPreferences sp = this.getSharedPreferences("gamesetting", ContextThemeWrapper.MODE_PRIVATE);
        if (sp!= null) {
            bestscore = sp.getInt("bestscore", 0);
            highscoreTv.setText(bestscore + "");
        }

        Random random1=new Random();
        int r1 = random1.nextInt(16-1+1)+1;
        Random random2=new Random();
        int r2 = random2.nextInt(16-1+1)+1;
        if(r1==r2 && r2!=16)
        {
            r2++;
        }
        else
        {
            if(r1==r2)
            {
                r2--;
            }
        }

        String[] nums = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"};
        Random r3 = new Random();
        Random r4 = new Random();
        int[] choice={2,4};
        int ind1=r3.nextInt(choice.length);
        System.out.println(ind1);
        int ind2=r3.nextInt(choice.length);
        System.out.println(ind2);
        for(int i=0 ;i<nums.length;i++)
        {
            if(parseInt(nums[i])==r1)
            {
                map.put(nums[i],choice[ind1]);
            }
            if(parseInt(nums[i])==r2)
            {
                map.put(nums[i],choice[ind2]);
            }
            if(parseInt(nums[i])!=r2 && parseInt(nums[i])!=r1)
            {
                map.put(nums[i],0);
            }

        }
        // Populate a List from Array elements
        final List<String> plantsList = new ArrayList<String>(Arrays.asList(nums));

        // Data bind GridView with ArrayAdapter (String Array elements)
        gridChanged(plantsList);

        // Button to reset the game
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retry(v);
            }
        });

        // Button to go back to the previous screen
        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        // Button to go to settings
        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        //timer that counts up
        long totalSeconds = 100000;
        long intervalSeconds = 1;

        CountDownTimer timer = new CountDownTimer(totalSeconds * 1000, intervalSeconds * 1000) {
            public void onTick(long millisUntilFinished) {
                timeTv.setText("" + (totalSeconds * 1000 - millisUntilFinished) / 1000);
            }

            public void onFinish() {
            }
        };


        timer.start();

        // When swiped to the top, checks if all griditems above are ok, then put the element value to the corresponding element, then adds value to the score-textview
        grid.setOnTouchListener(new OnSwipeTouchListener(Game_2048.this){
            public void onSwipeTop() {
                prevmap=new HashMap<>(map);
                int k = 0;
                while(k == 0 || !(allOkTop(1) && allOkTop(2) && allOkTop(3) && allOkTop(4))) {
                    k = 1;
                    for (int i = 16; i > 4; i--) {
                        int j = i;
                        if (j > 4 && map.get(j + "") == map.get((j - 4) + "")) {
                            map.put((j - 4) + "", map.get(j + "") + map.get((j - 4) + ""));
                            map.put(j + "", 0);
                            String scoreHere=scoreTv.getText().toString();
                            int scorenumber= parseInt(scoreHere);
                            scorenumber+=map.get((j-4)+"");
                            scoreTv.setText(scorenumber+"");
                            score = scorenumber;
                        } else if (j > 4 && map.get((j - 4) + "") == 0) {
                            map.put((j - 4) + "", map.get(j + ""));
                            map.put(j + "", 0);
                        }
                    }
                }

                direction = "Up";
                gridChanged(plantsList);
                generateRandomNumber();
                checkSwipe();
                if (timerStop) {
                    timer.cancel();
                }
            }

            // When swiped to the right, checks if all griditems to the right are ok, then put the element value to the corresponding element, then adds value to the score-textview
            public void onSwipeRight() {
                prevmap=new HashMap<>(map);
                int k=0;
                int l=0;
                while(l==0 || !(allOkRight(4) && allOkRight(8) && allOkRight(12) && allOkRight(16))) {
                    l=1;
                    for (int i = 1; i < 17; i++) {
                        int j = i;
                        if (k == 0 && map.get(j + "") == map.get((j + 1) + "")) {
                            map.put((j + 1) + "", map.get(j + "") + map.get((j + 1) + ""));
                            map.put(j + "", 0);
                            String scoreHere=scoreTv.getText().toString();
                            int scorenumber= parseInt(scoreHere);
                            scorenumber+=map.get((j+1)+"");
                            scoreTv.setText(scorenumber+"");
                            score = scorenumber;
                        } else if (k == 0 && map.get((j + 1) + "") == 0) {
                            map.put((j + 1) + "", map.get(j + ""));
                            map.put(j + "", 0);
                        }
                        if ((j / 4) != (j + 1) / 4) {
                            k = 1;
                        } else {
                            k = 0;
                        }
                    }
                }
                direction = "Right";
                gridChanged(plantsList);
                generateRandomNumber();
                checkSwipe();
                if (timerStop) {
                    timer.cancel();
                }
            }

            // When swiped to the left, checks if all griditems to the left are ok, then put the element value to the corresponding element, then adds value to the score-textview
            public void onSwipeLeft() {
                prevmap=new HashMap<>(map);
                int k=0;
                int l=0;
                while(l==0 || !(allOkLeft(1) && allOkLeft(5) && allOkLeft(9) && allOkLeft(13))) {
                    l=1;
                    for (int i = 16; i > 1; i--) {
                        int j = i;
                        if (j == 13 || j == 9 || j == 5) {
                            k = 1;
                        } else {
                            k = 0;
                        }
                        if (k == 0 && map.get(j + "") == map.get((j - 1) + "")) {
                            map.put((j - 1) + "", map.get(j + "") + map.get((j - 1) + ""));
                            map.put(j + "", 0);
                            String scoreHere=scoreTv.getText().toString();
                            int scorenumber= parseInt(scoreHere);
                            scorenumber+=map.get((j-1)+"");
                            scoreTv.setText(scorenumber+"");
                            score = scorenumber;
                        } else if (k == 0 && map.get((j - 1) + "") == 0) {
                            map.put((j - 1) + "", map.get(j + ""));
                            map.put(j + "", 0);
                        }

                    }
                }
                direction = "Left";
                gridChanged(plantsList);
                generateRandomNumber();
                checkSwipe();
                if (timerStop) {
                    timer.cancel();
                }
            }

            // When swiped to the bottom, checks if all griditems under are ok, then put the element value to the corresponding element, then adds value to the score-textview
            public void onSwipeBottom() {
                prevmap=new HashMap<>(map);
                int k=0;
                while (k==0 || !(allOkBottom(13) && allOkBottom(14) && allOkBottom(15) && allOkBottom(16))) {
                    k=1;
                    for (int i = 1; i < 13; i++) {
                        int j = i;

                        if (j < 13 && map.get(j + "") == map.get((j + 4) + "")) {
                            map.put((j + 4) + "", map.get(j + "") + map.get((j + 4) + ""));
                            map.put(j + "", 0);
                            String scoreHere=scoreTv.getText().toString();
                            int scorenumber= parseInt(scoreHere);
                            scorenumber+=map.get((j+4)+"");
                            scoreTv.setText(scorenumber+"");
                            score = scorenumber;
                        } else if (j < 13 && map.get((j + 4) + "") == 0) {
                            map.put((j + 4) + "", map.get(j + ""));
                            map.put(j + "", 0);
                        }
                    }
                }

                direction = "Down";
                gridChanged(plantsList);
                generateRandomNumber();
                checkSwipe();
                if (timerStop) {
                    timer.cancel();
                }
            }
        });

    }

    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        // sets current game time to total time for the game
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_2048(millis);

    }

    // method to check if all the grid items above the current location of the element are ok
    public boolean allOkTop(int loc)
    {
        if(map.get(loc+"")==0 && map.get((loc+4)+"")==0 && map.get(""+(loc+8))==0 && map.get(""+(loc+12))==0)
        {
            return true;
        }
        else if(map.get((loc+4)+"")==0 && map.get(""+(loc+8))==0 && map.get(""+(loc+12))==0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc+8))==0 && map.get(""+(loc+12))==0 && map.get(""+(loc+4))!=0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc+12))==0 && map.get(""+(loc+4))!=0 && map.get(""+loc)!=0 && map.get(""+(loc+8))!=0)
        {
            return true;
        }
        else if(map.get(loc+"")!=0 && map.get((loc+4)+"")!=0 && map.get(""+(loc+8))!=0 && map.get(""+(loc+12))!=0)
        {
            return true;
        }
        return false;
    }

    // method to check if all the grid items under the current location of the element are ok
    public boolean allOkBottom(int loc)
    {
        if(map.get(loc+"")==0 && map.get((loc-4)+"")==0 && map.get(""+(loc-8))==0 && map.get(""+(loc-12))==0)
        {
            return true;
        }
        else if(map.get((loc-4)+"")==0 && map.get(""+(loc-8))==0 && map.get(""+(loc-12))==0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc-8))==0 && map.get(""+(loc-12))==0 && map.get(""+(loc-4))!=0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc-12))==0 && map.get(""+(loc-4))!=0 && map.get(""+loc)!=0 && map.get(""+(loc-8))!=0)
        {
            return true;
        }
        else if(map.get(loc+"")!=0 && map.get((loc-4)+"")!=0 && map.get(""+(loc-8))!=0 && map.get(""+(loc-12))!=0)
        {
            return true;
        }
        return false;
    }

    // method to check if all the grid items to the right of the current location of the element are ok
    public boolean allOkRight(int loc)
    {
        if(map.get(loc+"")==0 && map.get((loc-1)+"")==0 && map.get(""+(loc-2))==0 && map.get(""+(loc-3))==0)
        {
            return true;
        }
        else if(map.get((loc-1)+"")==0 && map.get(""+(loc-2))==0 && map.get(""+(loc-3))==0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc-2))==0 && map.get(""+(loc-3))==0 && map.get(""+(loc-1))!=0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc-3))==0 && map.get(""+(loc-1))!=0 && map.get(""+loc)!=0 && map.get(""+(loc-2))!=0)
        {
            return true;
        }
        else if(map.get(loc+"")!=0 && map.get((loc-1)+"")!=0 && map.get(""+(loc-2))!=0 && map.get(""+(loc-3))!=0)
        {
            return true;
        }
        return false;
    }

    // method to check if all the grid items to the left of the current location of the element are ok
    public boolean allOkLeft(int loc)
    {
        if(map.get(loc+"")==0 && map.get((loc+1)+"")==0 && map.get(""+(loc+2))==0 && map.get(""+(loc+3))==0)
        {
            return true;
        }
        else if(map.get((loc+1)+"")==0 && map.get(""+(loc+2))==0 && map.get(""+(loc+3))==0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc+2))==0 && map.get(""+(loc+3))==0 && map.get(""+(loc+1))!=0 && map.get(""+(loc))!=0)
        {
            return true;
        }
        else if(map.get(""+(loc+3))==0 && map.get(""+(loc+1))!=0 && map.get(""+(loc))!=0 && map.get(""+(loc+2))!=0)
        {
            return true;
        }
        else if(map.get(loc+"")!=0 && map.get((loc+1)+"")!=0 && map.get(""+(loc+2))!=0 && map.get(""+(loc+3))!=0)
        {
            return true;
        }
        return false;
    }

    // Method to check game is won or lost, is called for every swipe. When the game is won or lost, change to a win-screen (or lose-screen) and add the score to the database if its the highscore
    public void checkSwipe()
    {
        TextView result = findViewById(R.id.textViewResultWin);
        for(String key : map.keySet())
        {
            if(map.get(key)==2048)
            {
                result.setVisibility(View.VISIBLE);
                result.setText("CONGRATULATIONS! \n YOU WIN!");
                result.setGravity(Gravity.CENTER);
                timerStop = true;
                if (score>bestscore) {
                    bestscore = score;
                    highscoreTv.setText(bestscore + "");
                    SharedPreferences sp = this.getSharedPreferences("gamesetting", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putInt("bestscore", bestscore);
                    editor.apply();
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                    String name = sharedPref.getString("playername", "Username");
                    String gamename = new Game_Helper().GAMENAME_2048;
                    Database_Helper database = new Database_Helper();
                    Database_Score scoreEntry = new Database_Score(gamename, bestscore, name, 1);
                    database.addScoreToDatabase(scoreEntry);
                }
                return;
            }
        }
        for(String key : map.keySet())
        {
            if(map.get(key)!=prevmap.get(key))
            {

                return;
            }

        }

        result.setText("GAME OVER! \n YOU LOSE!");
        timerStop = true;
        result.setGravity(Gravity.CENTER);
        result.setVisibility(View.VISIBLE);
        grid.setVisibility(View.INVISIBLE);

        if (score>bestscore) {
            bestscore = score;
            highscoreTv.setText(bestscore + "");
            SharedPreferences sp = this.getSharedPreferences("gamesetting", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt("bestscore", bestscore);
            editor.apply();
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            String name = sharedPref.getString("playername", "Username");
            String gamename = new Game_Helper().GAMENAME_2048;
            Database_Helper database = new Database_Helper();
            Database_Score scoreEntry = new Database_Score(gamename, bestscore, name, 1);
            database.addScoreToDatabase(scoreEntry);
        }
//        prevmap=map;

    }

    // Generates a 2 or a 4 at a random location in the grid
    public void generateRandomNumber()
    {
        Random ran = new Random();
        int i=ran.nextInt(16)+1;
        int flag=0;
        for(String key: map.keySet())
        {
            if(map.get(key)!=0)
            {
                flag++;
            }
        }
        if(flag!=16)
        {
            while(map.get(i+"")!=0)
            {
                i=ran.nextInt(16)+1;
            }
            int[] choice={2,4};
            int ind1=ran.nextInt(choice.length);
            map.put(i+"",choice[ind1]);
            String[] nums = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"};
            List<String> plantsList= Arrays.asList(nums);

            gridChanged(plantsList);
        }

    }

    // Changes the grid-appearance if the grid was changed
    public void gridChanged(List plantsList)
    {
        final List<String> l=plantsList;
        grid.setAdapter(new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, l){
            public View getView(int position, View convertView, ViewGroup parent) {

                // Return the GridView current item as a View
                View view = super.getView(position,convertView,parent);

                Drawable drawable = getDrawable(R.drawable.grid_item_corner);
                Drawable drawable1 = getDrawable(R.drawable.grid_item);
                Drawable drawable2 = getDrawable(R.drawable.grid_item_2);
                Drawable drawable4 = getDrawable(R.drawable.grid_item_4);
                Drawable drawable8 = getDrawable(R.drawable.grid_item_8);
                Drawable drawable16 = getDrawable(R.drawable.grid_item_16);
                Drawable drawable32 = getDrawable(R.drawable.grid_item_32);
                Drawable drawable64 = getDrawable(R.drawable.grid_item_64);
                Drawable drawable128 = getDrawable(R.drawable.grid_item_128);
                view.setBackground(drawable);

                // Convert the view as a TextView widget
                TextView tv = (TextView) view;

                TextView tvBack = (TextView) view;

                // Set the layout parameters for TextView widget
                RelativeLayout.LayoutParams lp =  new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT
                );
                tv.setLayoutParams(lp);
                tvBack.setLayoutParams(lp);

                tv.setWidth(100);
                tv.setHeight(220);
                tvBack.setWidth(100);
                tvBack.setHeight(220);

                // Set the TextView background color
                tv.setBackgroundColor(Color.parseColor("#d6cdc4"));
                tvBack.setBackgroundColor(Color.parseColor("#d6cdc4"));

                // Display TextView text in center position
                tv.setGravity(Gravity.CENTER);
                tvBack.setGravity(Gravity.CENTER);

                tv.setTextSize(30);

                tv.setTextColor(Color.parseColor("#ffffff"));

                Typeface type = Typeface.createFromAsset(getAssets(),"luckiest_guy.ttf");
                tv.setTypeface(type);

                tv.setBackground(drawable1);

                if (map.get(l.get(position)) != 0) {

                    tv.setText(map.get(l.get(position)) + "");

                    switch (map.get(l.get(position))) {
                        case 2:
                            tv.setBackground(drawable2);

                            break;
                        case 4:
                            tv.setBackgroundColor(Color.parseColor("#ece0c8"));
                            tv.setBackground(drawable4);
                            break;
                        case 8:
                            tv.setBackgroundColor(Color.parseColor("#f2b179"));
                            tv.setBackground(drawable8);
                            break;
                        case 16:
                            tv.setBackgroundColor(Color.parseColor("#f59563"));
                            tv.setBackground(drawable16);
                            break;
                        case 32:
                            tv.setBackgroundColor(Color.parseColor("#f57c5f"));
                            tv.setBackground(drawable32);
                            break;
                        case 64:
                            tv.setBackgroundColor(Color.parseColor("#f65d3b"));
                            tv.setBackground(drawable64);
                            break;
                        case 128:
                            tv.setBackgroundColor(Color.parseColor("#edce71"));
                            tv.setBackground(drawable128);
                            break;
                        case 256:
                            tv.setBackgroundColor(Color.parseColor("#edcc61"));
                            tv.setBackground(drawable2);
                            break;

                    }
                } else {
                    tv.setText("");
                }

                tv.setId(position);

                // Return the TextView widget as GridView item
                return tv;
            }
        });
    }

    // Resets the current game
    public void retry(View v)
    {
        Intent i= new Intent(Game_2048.this, Game_2048.class);
        finish();
        startActivity(i);
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}

class OnSwipeTouchListener implements View.OnTouchListener {

    private final GestureDetector gestureDetector;

    public OnSwipeTouchListener (Context ctx){
        gestureDetector = new GestureDetector(ctx, new GestureListener());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }
                        result = true;
                    }
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                    result = true;
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    public void onSwipeRight() {
    }

    public void onSwipeLeft() {
    }

    public void onSwipeTop() {
    }

    public void onSwipeBottom() {
    }
}


