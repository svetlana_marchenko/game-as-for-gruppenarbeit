/**
 * Author Mathias
 */
package com.example.myapplication.game_kniffel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.gridlayout.widget.GridLayout;

import com.example.myapplication.GlobalVariables;
import com.example.myapplication.Settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.myapplication.R;

import java.util.Random;
import java.util.stream.IntStream;

public class GameKniffel extends AppCompatActivity {

    private ImageView zahl1, zahl2, zahl3, zahl4, zahl5;
    private GridLayout cubeArea;

    private ImageButton backToMenu;
    private ImageButton toSettings;

    private Button throwButton;
    private Button nextButton;
    private LinearLayout playerNumber;
    private TextView question;

    private Button firstP, secondP, thirdP, fourthP, fifthP;
    private TableLayout tasks;
    private TableLayout player1, player2, player3, player4, player5;
    private TableLayout buttomTasks;
    private ScrollView scroll;

    private boolean is1True = true;
    private boolean is2True = true;
    private boolean is3True = true;
    private boolean is4True = true;
    private boolean is5True = true;

    private int singleWuerfelCount = 0;
    private int playedRounds = 0;
    private int playersCount;

    private boolean saveCube = false;

    SharedPreferences theme;
    long startTime;
    private int currentTheme;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Get saved Theme out of Shared Preferances
        theme = getSharedPreferences("theme", 0);

        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_kniffel);

        backToMenu = (ImageButton) findViewById(R.id.goBack);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        toSettings = (ImageButton) findViewById(R.id.goToSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });


        ((GlobalVariables) this.getApplication()).addOneRound();

        zahl1 =findViewById(R.id.imageView1);
        zahl2 =findViewById(R.id.imageView2);
        zahl3 =findViewById(R.id.imageView3);
        zahl4 =findViewById(R.id.imageView4);
        zahl5 =findViewById(R.id.imageView5);
        cubeArea = (GridLayout) findViewById(R.id.cubeArea);
        playerNumber = findViewById(R.id.playersNumber);
        question = findViewById(R.id.question);

        throwButton = findViewById(R.id.throwButton);
        nextButton = findViewById(R.id.nextButton);

        firstP = findViewById(R.id.playerOne);
        secondP = findViewById(R.id.playerTwo);
        thirdP = findViewById(R.id.playerThree);
        fourthP = findViewById(R.id.playerFour);
        fifthP = findViewById(R.id.playerFive);
        tasks = findViewById(R.id.tasksTable);
        scroll = findViewById(R.id.scrollView);

        buttomTasks = findViewById(R.id.buttomTasks);
        player1 = findViewById(R.id.player1);
        player2 = findViewById(R.id.player2);
        player3 = findViewById(R.id.player3);
        player4 = findViewById(R.id.player4);
        player5 = findViewById(R.id.player5);
//        player6 = findViewById(R.id.player6);
    }

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Insert this in onStop()
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_kniffel(millis);
    }

    /**
     * This method sets the start game interface invisible and
     * sets the game relevant views visible
     * sets the players names in the table head
     * @param view current view
     */
    public void startGame(View view){
//        playerName.setVisibility(view.INVISIBLE);
//        startButton.setVisibility(view.INVISIBLE);
        scroll.setVisibility(View.VISIBLE);
        question.setVisibility(View.INVISIBLE);
        playerNumber.setVisibility(View.INVISIBLE);
        cubeArea.setVisibility(View.VISIBLE);
        tasks.setVisibility(View.VISIBLE);
        throwButton.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.VISIBLE);
        buttomTasks.setVisibility(View.VISIBLE);


        Button b = (Button) view;
        String i = b.getText().toString();
        playersCount = Integer.parseInt(b.getText().toString());
        /*
         * depending on the nr of players the columns become visible
         */
        switch(i){
            case "1": player1.setVisibility(View.VISIBLE);
                break;
            case "2":
                player1.setVisibility(View.VISIBLE);
                player2.setVisibility(View.VISIBLE);
                break;
            case "3":
                player1.setVisibility(View.VISIBLE);
                player2.setVisibility(View.VISIBLE);
                player3.setVisibility(View.VISIBLE);
                break;
            case "4":
                player1.setVisibility(View.VISIBLE);
                player2.setVisibility(View.VISIBLE);
                player3.setVisibility(View.VISIBLE);
                player4.setVisibility(View.VISIBLE);
                break;
            default:
                player1.setVisibility(View.VISIBLE);
                player2.setVisibility(View.VISIBLE);
                player3.setVisibility(View.VISIBLE);
                player4.setVisibility(View.VISIBLE);
                player5.setVisibility(View.VISIBLE);
                break;
                /*default:
                    player1.setVisibility(view.VISIBLE);
                    player2.setVisibility(view.VISIBLE);
                    player3.setVisibility(view.VISIBLE);
                    player4.setVisibility(view.VISIBLE);
                    player5.setVisibility(view.VISIBLE);
                    player6.setVisibility(view.VISIBLE);
                    break;*/

        }

        int playerCount = 0;

        firstP.setBackgroundColor(0x33ffffff);
        firstP.setTextColor(0xffffffff);

            /*
              The first col is clickable the others not
             */
        TableLayout[] tables = {player1, player2, player3, player4, player5};
        for(TableLayout table : tables){
            if(player1.getId() == table.getId()){
                for(int k = 0; k < player1.getChildCount(); k++) {
                    TableRow tr = (TableRow) player1.getChildAt(k);
                    Button btn = (Button) tr.getChildAt(0);
                    btn.setClickable(true);
                }
            }else{
                for(int k = 0; k < table.getChildCount(); k++) {
                    TableRow tr = (TableRow) table.getChildAt(k);
                    Button btn = (Button) tr.getChildAt(0);
                    btn.setClickable(false);
                }
            }

        }


//        }
    }

    /**
     * this method sets the logic behind throwing the cubes
     * @param view current view
     */
    public void wuerfeln(View view){
        saveCube = true;
        int eins = new Random().nextInt(5 + 1) + 1;
        int zwei = new Random().nextInt(5 + 1) + 1;
        int drei = new Random().nextInt(5 + 1) + 1;
        int vier = new Random().nextInt(5 + 1) + 1;
        int fuenf = new Random().nextInt(5 + 1) + 1;

        if(singleWuerfelCount < 3){
            singleWuerfelCount++;
            if(is1True) {
                switch (eins) {
                    case 1:
                        zahl1.setImageResource(R.drawable.kniffel_eins);
                        zahl1.setTag(1);
                        break;
                    case 2:
                        zahl1.setImageResource(R.drawable.kniffel_zwei);
                        zahl1.setTag(2);
                        break;
                    case 3:
                        zahl1.setImageResource(R.drawable.kniffel_drei);
                        zahl1.setTag(3);
                        break;
                    case 4:
                        zahl1.setImageResource(R.drawable.kniffel_vier);
                        zahl1.setTag(4);
                        break;
                    case 5:
                        zahl1.setImageResource(R.drawable.kniffel_fuenf);
                        zahl1.setTag(5);
                        break;
                    case 6:
                        zahl1.setImageResource(R.drawable.kniffel_sechs);
                        zahl1.setTag(6);
                        break;
                    default:
                        System.out.println("Error by throwing 1. Cube");
                        break;
                }
            }

            if(is2True) {
                switch (zwei) {
                    case 1:
                        zahl2.setImageResource(R.drawable.kniffel_eins);
                        zahl2.setTag(1);
                        break;
                    case 2:
                        zahl2.setImageResource(R.drawable.kniffel_zwei);
                        zahl2.setTag(2);
                        break;
                    case 3:
                        zahl2.setImageResource(R.drawable.kniffel_drei);
                        zahl2.setTag(3);
                        break;
                    case 4:
                        zahl2.setImageResource(R.drawable.kniffel_vier);
                        zahl2.setTag(4);
                        break;
                    case 5:
                        zahl2.setImageResource(R.drawable.kniffel_fuenf);
                        zahl2.setTag(5);
                        break;
                    case 6:
                        zahl2.setImageResource(R.drawable.kniffel_sechs);
                        zahl2.setTag(6);
                        break;
                    default:
                        System.out.println("Error by throwing 2. Cube");
                        break;
                }
            }

            if(is3True) {
                switch (drei) {
                    case 1:
                        zahl3.setImageResource(R.drawable.kniffel_eins);
                        zahl3.setTag(1);
                        break;
                    case 2:
                        zahl3.setImageResource(R.drawable.kniffel_zwei);
                        zahl3.setTag(2);
                        break;
                    case 3:
                        zahl3.setImageResource(R.drawable.kniffel_drei);
                        zahl3.setTag(3);
                        break;
                    case 4:
                        zahl3.setImageResource(R.drawable.kniffel_vier);
                        zahl3.setTag(4);
                        break;
                    case 5:
                        zahl3.setImageResource(R.drawable.kniffel_fuenf);
                        zahl3.setTag(5);
                        break;
                    case 6:
                        zahl3.setImageResource(R.drawable.kniffel_sechs);
                        zahl3.setTag(6);
                        break;
                    default:
                        System.out.println("Error by throwing 3. Cube");
                        break;
                }
            }

            if(is4True) {
                switch (vier) {
                    case 1:
                        zahl4.setImageResource(R.drawable.kniffel_eins);
                        zahl4.setTag(1);
                        break;
                    case 2:
                        zahl4.setImageResource(R.drawable.kniffel_zwei);
                        zahl4.setTag(2);
                        break;
                    case 3:
                        zahl4.setImageResource(R.drawable.kniffel_drei);
                        zahl4.setTag(3);
                        break;
                    case 4:
                        zahl4.setImageResource(R.drawable.kniffel_vier);
                        zahl4.setTag(4);
                        break;
                    case 5:
                        zahl4.setImageResource(R.drawable.kniffel_fuenf);
                        zahl4.setTag(5);
                        break;
                    case 6:
                        zahl4.setImageResource(R.drawable.kniffel_sechs);
                        zahl4.setTag(6);
                        break;
                    default:
                        System.out.println("Error by throwing 4. Cube");
                        break;
                }
            }

            if(is5True) {
                switch (fuenf) {
                    case 1:
                        zahl5.setImageResource(R.drawable.kniffel_eins);
                        zahl5.setTag(1);
                        break;
                    case 2:
                        zahl5.setImageResource(R.drawable.kniffel_zwei);
                        zahl5.setTag(2);
                        break;
                    case 3:
                        zahl5.setImageResource(R.drawable.kniffel_drei);
                        zahl5.setTag(3);
                        break;
                    case 4:
                        zahl5.setImageResource(R.drawable.kniffel_vier);
                        zahl5.setTag(4);
                        break;
                    case 5:
                        zahl5.setImageResource(R.drawable.kniffel_fuenf);
                        zahl5.setTag(5);
                        break;
                    case 6:
                        zahl5.setImageResource(R.drawable.kniffel_sechs);
                        zahl5.setTag(6);
                        break;
                    default:
                        System.out.println("Error by throwing 5. Cube");
                        break;
                }
            }
        }
    }

    /**
     * this method prepares the game for the next round
     * @param view current view
     */
    public void next(View view){
        singleWuerfelCount = 0;
        is1True = true;
        is2True = true;
        is3True = true;
        is4True = true;
        is5True = true;

        saveCube = false;

        zahl1.setBackgroundColor(View.INVISIBLE);
        zahl2.setBackgroundColor(View.INVISIBLE);
        zahl3.setBackgroundColor(View.INVISIBLE);
        zahl4.setBackgroundColor(View.INVISIBLE);
        zahl5.setBackgroundColor(View.INVISIBLE);

        /*
         * highlights the player's turn
         */
        Button[] players = {firstP, secondP, thirdP, fourthP, fifthP};
        if( playedRounds < 5 && playersCount > 1){
            Button current;
            Button next;
            if(playedRounds == playersCount-1){
                playedRounds = 0;
                current = players[playersCount-1];
                next = players[playedRounds];
            }else{
                current = players[playedRounds];
                next = players[playedRounds + 1];
                playedRounds++;
            }
            current.setBackgroundColor(View.INVISIBLE);
//            current.setTextColor(0xffffffff);
            next.setBackgroundColor(0x33ffffff);
//            next.setTextColor(0xffffffff);
        }

        /*
         * Blocks buttons of players who do not have their turn
         */
        TableLayout[] tables = {player1, player2, player3, player4, player5};
        switch(playedRounds){
            case 0:
                for(TableLayout table : tables){
                    if (player1.getId() == table.getId()){
                        for(int i = 0; i < player1.getChildCount(); i++) {
                            TableRow tr = (TableRow) player1.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(true);
                        }
                    }else{
                        for(int i = 0; i < table.getChildCount(); i++) {
                            TableRow tr = (TableRow) table.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(false);
                        }
                    }
                }
                break;
            case 1:
                for(TableLayout table : tables){
                    if (player2.getId() == table.getId()){
                        for(int i = 0; i < player2.getChildCount(); i++) {
                            TableRow tr = (TableRow) player2.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(true);
                        }
                    }else{
                        for(int i = 0; i < table.getChildCount(); i++) {
                            TableRow tr = (TableRow) table.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(false);
                        }
                    }
                }
                break;
            case 2:
                for(TableLayout table : tables){
                    if (player3.getId() == table.getId()){
                        for(int i = 0; i < player3.getChildCount(); i++) {
                            TableRow tr = (TableRow) player3.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(true);
                        }
                    }else{
                        for(int i = 0; i < table.getChildCount(); i++) {
                            TableRow tr = (TableRow) table.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(false);
                        }
                    }
                }
                break;
            case 3:
                for(TableLayout table : tables){
                    if (player4.getId() == table.getId()){
                        for(int i = 0; i < player4.getChildCount(); i++) {
                            TableRow tr = (TableRow) player4.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(true);
                        }
                    }else{
                        for(int i = 0; i < table.getChildCount(); i++) {
                            TableRow tr = (TableRow) table.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(false);
                        }
                    }
                }
                break;
            case 4:
                for(TableLayout table : tables){
                    if (player5.getId() == table.getId()){
                        for(int i = 0; i < player5.getChildCount(); i++) {
                            TableRow tr = (TableRow) player5.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(true);
                        }
                    }else{
                        for(int i = 0; i < table.getChildCount(); i++) {
                            TableRow tr = (TableRow) table.getChildAt(i);
                            Button btn = (Button) tr.getChildAt(0);
                            btn.setClickable(false);
                        }
                    }
                }
                break;
            default:
                System.out.print("Error set clickable of player column in next()");
                break;

        }

    }

    /**
     * enters the result for the clicked task
     * @param view current view
     */
    public void enterResult(View view){
        String tag = String.valueOf(view.getTag());
        Button btn = (Button) view;
        int result;

        switch(tag){
            case "one":
                result = getNumberResult("one");
                btn.setText(Integer.toString(result));
                break;
            case "two":
                result = getNumberResult("two");
                btn.setText(Integer.toString(result));
                break;
            case "three":
                result = getNumberResult("three");
                btn.setText(Integer.toString(result));
                break;
            case "four":
                result = getNumberResult("four");
                btn.setText(Integer.toString(result));
                break;
            case "five":
                result = getNumberResult("five");
                btn.setText(Integer.toString(result));
                break;
            case "six":
                result = getNumberResult("six");
                btn.setText(Integer.toString(result));
                break;
            case "top":
                btn.setText(String.valueOf(totalTop()));
                break;
            case "triplet":
                if(triplet() > 0){
                    btn.setText(Integer.toString(triplet()));
                }else{
                    btn.setText("0");
                }
                break;
            case "quad":
                if(quad() > 0){
                    btn.setText(Integer.toString(quad()));
                }else{
                    btn.setText("0");
                }
                break;
            case "full":
                if(fullHouse()){
                    btn.setText("25");
                }else{
                    btn.setText("0");
                }
                break;
            case "street":
                if(street()){
                    btn.setText("30");
                }else{
                    btn.setText("0");
                }
                break;
            case "streetBig":
                if(bigStreet()){
                    btn.setText("40");
                }else{
                    btn.setText("0");
                }
                break;
            case "kniffel":
                if (kniffel()){
                    btn.setText("50");
                }else{
                    btn.setText("0");
                }
                break;
            case "chance":
                if (zahl1.getTag() != null || zahl2.getTag() != null || zahl3.getTag() != null || zahl4.getTag() != null || zahl5.getTag() != null){
                    int[] cubes = {Integer.parseInt(String.valueOf(zahl1.getTag())), Integer.parseInt(String.valueOf(zahl2.getTag())), Integer.parseInt(String.valueOf(zahl3.getTag())), Integer.parseInt(String.valueOf(zahl4.getTag())), Integer.parseInt(String.valueOf(zahl5.getTag()))};
                    btn.setText(Integer.toString(IntStream.of(cubes).sum()));
                }
                break;
            case "buttom":
                btn.setText(String.valueOf(totalButtom()));
                break;
            case "total":
                btn.setText(Integer.toString(total()));
        }
    }

    /**
     * function for calculate the count of the first six tasks
     * @param cubeNumber which number must be summed up
     * @return returns the result
     */
    public int getNumberResult(String cubeNumber){
        int count = 0;
        switch(cubeNumber){
            case "one":
                if(String.valueOf(zahl1.getTag()).equals("1")){
                    count++;
                }
                if(String.valueOf(zahl2.getTag()).equals("1")){
                    count++;
                }
                if(String.valueOf(zahl3.getTag()).equals("1")){
                    count++;
                }
                if(String.valueOf(zahl4.getTag()).equals("1")){
                    count++;
                }
                if(String.valueOf(zahl5.getTag()).equals("1")){
                    count++;
                }
                return count;
            case "two":
                if(String.valueOf(zahl1.getTag()).equals("2")){
                    count+=2;
                }
                if(String.valueOf(zahl2.getTag()).equals("2")){
                    count+=2;
                }
                if(String.valueOf(zahl3.getTag()).equals("2")){
                    count+=2;
                }
                if(String.valueOf(zahl4.getTag()).equals("2")){
                    count+=2;
                }
                if(String.valueOf(zahl5.getTag()).equals("2")){
                    count+=2;
                }
                return count;
            case "three":
                if(String.valueOf(zahl1.getTag()).equals("3")){
                    count+=3;
                }
                if(String.valueOf(zahl2.getTag()).equals("3")){
                    count+=3;
                }
                if(String.valueOf(zahl3.getTag()).equals("3")){
                    count+=3;
                }
                if(String.valueOf(zahl4.getTag()).equals("3")){
                    count+=3;
                }
                if(String.valueOf(zahl5.getTag()).equals("3")){
                    count+=3;
                }
                return count;
            case "four":
                if(String.valueOf(zahl1.getTag()).equals("4")){
                    count+=4;
                }
                if(String.valueOf(zahl2.getTag()).equals("4")){
                    count+=4;
                }
                if(String.valueOf(zahl3.getTag()).equals("4")){
                    count+=4;
                }
                if(String.valueOf(zahl4.getTag()).equals("4")){
                    count+=4;
                }
                if(String.valueOf(zahl5.getTag()).equals("4")){
                    count+=4;
                }
                return count;
            case "five":
                if(String.valueOf(zahl1.getTag()).equals("5")){
                    count+=5;
                }
                if(String.valueOf(zahl2.getTag()).equals("5")){
                    count+=5;
                }
                if(String.valueOf(zahl3.getTag()).equals("5")){
                    count+=5;
                }
                if(String.valueOf(zahl4.getTag()).equals("5")){
                    count+=5;
                }
                if(String.valueOf(zahl5.getTag()).equals("5")){
                    count+=5;
                }
                return count;
            case "six":
                if(String.valueOf(zahl1.getTag()).equals("6")){
                    count+=6;
                }
                if(String.valueOf(zahl2.getTag()).equals("6")){
                    count+=6;
                }
                if(String.valueOf(zahl3.getTag()).equals("6")){
                    count+=6;
                }
                if(String.valueOf(zahl4.getTag()).equals("6")){
                    count+=6;
                }
                if(String.valueOf(zahl5.getTag()).equals("6")){
                    count+=6;
                }
                return count;
            default:
                return 0;
        }
    }

    /**
     * sum up the first six results
     * @return the result of first six
     */
    public int totalTop(){
        int totalTop = 0;
        switch(playedRounds){
            case 0:
                for(int i = 1; i < 7; i++) {
                    TableRow tr = (TableRow) player1.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            case 1:
                for(int i = 1; i < 7; i++) {
                    TableRow tr = (TableRow) player2.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            case 2:
                for(int i = 1; i < 7; i++) {
                    TableRow tr = (TableRow) player3.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            case 3:
                for(int i = 1; i < 7; i++) {
                    TableRow tr = (TableRow) player4.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            case 4:
                for(int i = 1; i < 7; i++) {
                    TableRow tr = (TableRow) player5.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            default:
                System.out.println("Error at total top");
        }
        if (totalTop >= 63){
            return totalTop + 35;
        }else {
            return totalTop;
        }
    }

    /**
     * sum up the results from triplet to yahtzee
     * @return the result of the buttom area
     */
    public int totalButtom(){
        int totalTop = 0;
        switch(playedRounds){
            case 0:
                for(int i = 8; i < 15; i++) {
                    TableRow tr = (TableRow) player1.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            case 1:
                for(int i = 8; i < 15; i++) {
                    TableRow tr = (TableRow) player2.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            case 2:
                for(int i = 8; i < 15; i++) {
                    TableRow tr = (TableRow) player3.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            case 3:
                for(int i = 8; i < 15; i++) {
                    TableRow tr = (TableRow) player4.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            case 4:
                for(int i = 8; i < 15; i++) {
                    TableRow tr = (TableRow) player5.getChildAt(i);
                    Button btn = (Button) tr.getChildAt(0);
                    if (!btn.getText().equals("")){
                        totalTop += Integer.parseInt(String.valueOf(btn.getText()));
                    }
                }
                break;
            default:
                System.out.println("Error at calculating total buttom");
                break;
        }
        return totalTop;
    }

    /**
     * sum up the end result
     * @return the end result
     */
    public int total(){
        int totalTop = 0;
        TableRow top;
        TableRow buttom;
        Button topBtn;
        Button buttomBtn;
        switch(playedRounds){
            case 0:
                top = (TableRow) player1.getChildAt(7);
                buttom = (TableRow) player1.getChildAt(15);
                topBtn = (Button) top.getChildAt(0);
                buttomBtn = (Button) buttom.getChildAt(0);
                if (!String.valueOf(topBtn.getText()).equals("") && !String.valueOf(buttomBtn.getText()).equals("")){
                    totalTop += (Integer.parseInt(String.valueOf(topBtn.getText())) + Integer.parseInt(String.valueOf(buttomBtn.getText())));
                }

                break;
            case 1:
                top = (TableRow) player2.getChildAt(7);
                buttom = (TableRow) player2.getChildAt(15);
                topBtn = (Button) top.getChildAt(0);
                buttomBtn = (Button) buttom.getChildAt(0);
                if (!String.valueOf(topBtn.getText()).equals("") && !String.valueOf(buttomBtn.getText()).equals("")){
                    totalTop += (Integer.parseInt(String.valueOf(topBtn.getText())) + Integer.parseInt(String.valueOf(buttomBtn.getText())));
                }
                break;
            case 2:
                top = (TableRow) player3.getChildAt(7);
                buttom = (TableRow) player3.getChildAt(15);
                topBtn = (Button) top.getChildAt(0);
                buttomBtn = (Button) buttom.getChildAt(0);
                if (!String.valueOf(topBtn.getText()).equals("") && !String.valueOf(buttomBtn.getText()).equals("")){
                    totalTop += (Integer.parseInt(String.valueOf(topBtn.getText())) + Integer.parseInt(String.valueOf(buttomBtn.getText())));
                }
                break;
            case 3:
                top = (TableRow) player4.getChildAt(7);
                buttom = (TableRow) player4.getChildAt(15);
                topBtn = (Button) top.getChildAt(0);
                buttomBtn = (Button) buttom.getChildAt(0);
                if (!String.valueOf(topBtn.getText()).equals("") && !String.valueOf(buttomBtn.getText()).equals("")){
                    totalTop += (Integer.parseInt(String.valueOf(topBtn.getText())) + Integer.parseInt(String.valueOf(buttomBtn.getText())));
                }
                break;
            case 4:
                top = (TableRow) player5.getChildAt(7);
                buttom = (TableRow) player5.getChildAt(15);
                topBtn = (Button) top.getChildAt(0);
                buttomBtn = (Button) buttom.getChildAt(0);
                if (!String.valueOf(topBtn.getText()).equals("") && !String.valueOf(buttomBtn.getText()).equals("")){
                    totalTop += (Integer.parseInt(String.valueOf(topBtn.getText())) + Integer.parseInt(String.valueOf(buttomBtn.getText())));
                }
                break;
            default:
                System.out.println("Error at calculating total count");
                break;
        }

        return totalTop;

    }

    /**
     * checks if displayed cubes are full house
     * @return boolean depending if full hous
     */
    public boolean fullHouse(){
        int[] amount = {0, 0, 0, 0, 0, 0};

        if (zahl1.getTag() != null || zahl2.getTag() != null || zahl3.getTag() != null || zahl4.getTag() != null || zahl5.getTag() != null){
            int[] arr = {Integer.parseInt(String.valueOf(zahl1.getTag())), Integer.parseInt(String.valueOf(zahl2.getTag())), Integer.parseInt(String.valueOf(zahl3.getTag())), Integer.parseInt(String.valueOf(zahl4.getTag())), Integer.parseInt(String.valueOf(zahl5.getTag()))};

            for (int value : arr) { // dices is your integer array
                switch (value) {
                    case 1:
                        amount[0] += 1;
                        break;
                    case 2:
                        amount[1] += 1;
                        break;
                    case 3:
                        amount[2] += 1;
                        break;
                    case 4:
                        amount[3] += 1;
                        break;
                    case 5:
                        amount[4] += 1;
                        break;
                    case 6:
                        amount[5] += 1;
                        break;
                }
            }
        }


        boolean got3same = false;
        boolean got2same = false;
        for(int i = 0; i < amount.length && (!got3same || !got2same); i++) {
            if(!got3same && amount[i] == 3) {
                got3same = true;
            } else if(!got2same && amount[i] == 2) {
                got2same = true;
            }
        }

        return got3same && got2same;
    }

    /**
     * checks if cubes are triplet and sum up
     * @return sum of triplet or 0
     */
    public int triplet(){
        int[] amount = {0, 0, 0, 0, 0, 0};
        int count = 0;
        if (zahl1.getTag() != null || zahl2.getTag() != null || zahl3.getTag() != null || zahl4.getTag() != null || zahl5.getTag() != null){
            int[] arr = {Integer.parseInt(String.valueOf(zahl1.getTag())), Integer.parseInt(String.valueOf(zahl2.getTag())), Integer.parseInt(String.valueOf(zahl3.getTag())), Integer.parseInt(String.valueOf(zahl4.getTag())), Integer.parseInt(String.valueOf(zahl5.getTag()))};

            for (int value : arr) { // dices is your integer array
                switch (value) {
                    case 1:
                        amount[0] += 1;
                        break;
                    case 2:
                        amount[1] += 1;
                        break;
                    case 3:
                        amount[2] += 1;
                        break;
                    case 4:
                        amount[3] += 1;
                        break;
                    case 5:
                        amount[4] += 1;
                        break;
                    case 6:
                        amount[5] += 1;
                        break;
                }
            }

            for (int value : amount) {
                if (value >= 3) {
                    count = IntStream.of(arr).sum();
                }
            }

        }
        return count;

    }

    /**
     * checks if cubes are four of a kind and sum up
     * @return sum of four of a kind or 0
     */
    public int quad(){
        int[] amount = {0, 0, 0, 0, 0, 0};
        int count = 0;

        if (zahl1.getTag() != null || zahl2.getTag() != null || zahl3.getTag() != null || zahl4.getTag() != null || zahl5.getTag() != null){
            int[] arr = {Integer.parseInt(String.valueOf(zahl1.getTag())), Integer.parseInt(String.valueOf(zahl2.getTag())), Integer.parseInt(String.valueOf(zahl3.getTag())), Integer.parseInt(String.valueOf(zahl4.getTag())), Integer.parseInt(String.valueOf(zahl5.getTag()))};

            for (int value : arr) { // dices is your integer array
                switch (value) {
                    case 1:
                        amount[0] += 1;
                        break;
                    case 2:
                        amount[1] += 1;
                        break;
                    case 3:
                        amount[2] += 1;
                        break;
                    case 4:
                        amount[3] += 1;
                        break;
                    case 5:
                        amount[4] += 1;
                        break;
                    case 6:
                        amount[5] += 1;
                        break;
                }
            }

            for (int value : amount) {
                if (value >= 4) {
                    count = IntStream.of(arr).sum();
                }
            }

        }
        return count;
    }

    /**
     * checks if cubes are small straight
     * @return boolean
     */
    public boolean street(){
        int[] amount = {0, 0, 0, 0, 0, 0};

        if (zahl1.getTag() != null || zahl2.getTag() != null || zahl3.getTag() != null || zahl4.getTag() != null || zahl5.getTag() != null){
            int[] arr = {Integer.parseInt(String.valueOf(zahl1.getTag())), Integer.parseInt(String.valueOf(zahl2.getTag())), Integer.parseInt(String.valueOf(zahl3.getTag())), Integer.parseInt(String.valueOf(zahl4.getTag())), Integer.parseInt(String.valueOf(zahl5.getTag()))};

            System.out.println("Arr length: " + arr.length + " " + arr[0] + " " + arr[1] + " " + arr[2] + " " + arr[3] + " " + arr[4]);

            for (int value : arr) { // dices is your integer array
                switch (value) {
                    case 1:
                        amount[0] += 1;
                        break;
                    case 2:
                        amount[1] += 1;
                        break;
                    case 3:
                        amount[2] += 1;
                        break;
                    case 4:
                        amount[3] += 1;
                        break;
                    case 5:
                        amount[4] += 1;
                        break;
                    case 6:
                        amount[5] += 1;
                        break;
                }
            }
        }

        if((amount[0] >= 1 && amount[1] >= 1 && amount[2] >= 1 && amount[3] >= 1) ||
                (amount[1] >= 1 && amount[2] >= 1 && amount[3] >= 1 && amount[4] >= 1) ||
                (amount[2] >= 1 && amount[3] >= 1 && amount[4] >= 1 && amount[5] >= 1)){
            System.out.println("True:   1: " + amount[0] + " |2: " + amount[1] + " |3: "+ amount[2] + " |4: "+ amount[3] + " |5: " + amount[4] + " |6: " + amount[5]);
            return true;
        }else {
            System.out.println("False:   1: " + amount[0] + " |2: " + amount[1] + " |3: "+ amount[2] + " |4: "+ amount[3] + " |5: " + amount[4] + " |6: " + amount[5]);
            return false;
        }
    }

    /**
     * checks if cubes are great straight
     * @return boolean
     */
    public boolean bigStreet(){
        int[] amount = {0, 0, 0, 0, 0, 0};

        if (zahl1.getTag() != null || zahl2.getTag() != null || zahl3.getTag() != null || zahl4.getTag() != null || zahl5.getTag() != null){
            int[] arr = {Integer.parseInt(String.valueOf(zahl1.getTag())), Integer.parseInt(String.valueOf(zahl2.getTag())), Integer.parseInt(String.valueOf(zahl3.getTag())), Integer.parseInt(String.valueOf(zahl4.getTag())), Integer.parseInt(String.valueOf(zahl5.getTag()))};

            for (int value : arr) { // dices is your integer array
                switch (value) {
                    case 1:
                        amount[0] += 1;
                        break;
                    case 2:
                        amount[1] += 1;
                        break;
                    case 3:
                        amount[2] += 1;
                        break;
                    case 4:
                        amount[3] += 1;
                        break;
                    case 5:
                        amount[4] += 1;
                        break;
                    case 6:
                        amount[5] += 1;
                        break;
                }
            }
        }

        return (amount[0] == 1 && amount[1] == 1 && amount[2] == 1 && amount[3] == 1 && amount[4] == 1) ||
                (amount[1] == 1 && amount[2] == 1 && amount[3] == 1 && amount[4] == 1 && amount[5] == 1);

    }

    /**
     * checks if cubes are a yahtzee
     * @return boolean
     */
    public boolean kniffel(){
        int[] amount = {0, 0, 0, 0, 0, 0};

        if (zahl1.getTag() != null || zahl2.getTag() != null || zahl3.getTag() != null || zahl4.getTag() != null || zahl5.getTag() != null){
            int[] arr = {Integer.parseInt(String.valueOf(zahl1.getTag())), Integer.parseInt(String.valueOf(zahl2.getTag())), Integer.parseInt(String.valueOf(zahl3.getTag())), Integer.parseInt(String.valueOf(zahl4.getTag())), Integer.parseInt(String.valueOf(zahl5.getTag()))};

            for (int value : arr) { // dices is your integer array
                switch (value) {
                    case 1:
                        amount[0] += 1;
                        break;
                    case 2:
                        amount[1] += 1;
                        break;
                    case 3:
                        amount[2] += 1;
                        break;
                    case 4:
                        amount[3] += 1;
                        break;
                    case 5:
                        amount[4] += 1;
                        break;
                    case 6:
                        amount[5] += 1;
                        break;
                }
            }
        }

        return amount[0] == 5 || amount[1] == 5 || amount[2] == 5 || amount[3] == 5 || amount[4] == 5 || amount[5] == 5;
    }

    /**
     * This methods is for saving your cube
     * @param view current view
     */
    public void setEinsFalse(View view){
        if(saveCube){
            if(is1True) {
                is1True = false;
                view.setBackgroundColor(0xff3C8CAE);
            }else{
                is1True = true;
                view.setBackgroundColor(View.INVISIBLE);
            }
        }

    }
    public void setZweiFalse(View view){
        if(saveCube){
            if(is2True) {
                is2True = false;
                view.setBackgroundColor(0xff3C8CAE);
            }else{
                is2True = true;
                view.setBackgroundColor(View.INVISIBLE);
            }
        }

    }
    public void setDreiFalse(View view){
        if(saveCube){
            if(is3True) {
                is3True = false;
                view.setBackgroundColor(0xff3C8CAE);
            }else{
                is3True = true;
                view.setBackgroundColor(View.INVISIBLE);
            }
        }

    }
    public void setVierFalse(View view){
        if(saveCube){
            if(is4True) {
                is4True = false;
                view.setBackgroundColor(0xff3C8CAE);
            }else{
                is4True = true;
                view.setBackgroundColor(View.INVISIBLE);
            }
        }

    }
    public void setFuenfFalse(View view){
        if(saveCube){
            if(is5True) {
                is5True = false;
                view.setBackgroundColor(0xff3C8CAE);
            }else{
                is5True = true;
                view.setBackgroundColor(View.INVISIBLE);
            }
        }

    }

    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void restart(){
        finish();
        startActivity(getIntent());
    }
}