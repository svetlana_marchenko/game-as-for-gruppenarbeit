package com.example.myapplication.game_DragonFlight;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.ArrayList;

public class BackgroundManager {

    private long startTime;
    private long initTime;
    private float oldSpeed;

    private Bitmap bm;

    private ArrayList<Background> backgrounds;

    public BackgroundManager(Bitmap bm){
        this.bm = bm;
        startTime = initTime = System.currentTimeMillis();

        backgrounds = new ArrayList<>();

        populateBackground();
    }

    private void populateBackground() {
        int currY = -4*Constants.SCREEN_HEIGHT/3;
        while(currY < 0){
            Background bg = new Background(bm, currY);
            backgrounds.add(bg);
            currY += Constants.SCREEN_HEIGHT/2;
        }
    }

    public void update(){
        if(startTime < Constants.INIT_TIME){
            startTime = Constants.INIT_TIME - 10;
        }
        int elapsedTime = (int)(System.currentTimeMillis() - startTime);
        startTime = System.currentTimeMillis();
        float speed;
        if(oldSpeed > 0.7) {
            speed = oldSpeed;
        } else {
            speed = (float) (Math.sqrt((startTime - initTime) / 5000.0)) * (Constants.SCREEN_HEIGHT / (4000.0f));
            oldSpeed = speed;
        }
        for(Background bg : backgrounds){
            bg.incrementY((speed/2) * elapsedTime);
        }
        //setpoint for when the new picture should be added
        if(backgrounds.get(backgrounds.size() - 1).getRectangle().top >= Constants.SCREEN_HEIGHT){
            backgrounds.add(0, new Background(bm, backgrounds.get(0).getRectangle().top-Constants.SCREEN_HEIGHT));
            backgrounds.remove(backgrounds.size() - 1);
        }
    }

    public void draw(Canvas canvas) {
        for (Background bg : backgrounds) {
            bg.draw(canvas);
        }
    }
}
