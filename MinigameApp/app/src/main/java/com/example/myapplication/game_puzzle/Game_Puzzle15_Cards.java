// Alexander Becker

package com.example.myapplication.game_puzzle;

import android.widget.Toast;

public class Game_Puzzle15_Cards {
    private int[][] playground;
    private int     n, m;
    private boolean isPossible;
//    private boolean isSolvable;

    public Game_Puzzle15_Cards(int N, int M) {
        n = N;
        m = M;
        playground = new int[n][m];
    }

    public void shuffleCards() {
        playground = new int[n][m];
        int tempX, tempY;

        // Funktiosnweise des Algorithmus ist:
        //      Modulo liefert die Spalte
        //      Division liefert Zeile
        // Bsp. dritte Karte auf 4x4-Feld: Feldindex 2, 0 (Spalte 2, Zeile 0)
        // n ist playground-Groesse ... = 4
        // Schleifendurchlauf i = 2
        // 2 modulo 4 = 2 (Spalte)d
        // 2 / 4 = 0 (Zeile)

        // i laeuft von 0 bis 15
        for (int i = 0; i < (n * m - 1); i++) {
            // zunaechst mal alle Felder auf Wert 0 setzen
            playground[i % n][i / m] = 0;
        }

        for (int i = 0; i < (n * m); i++) {
            tempX = (int) (Math.random() * n);
            tempY = (int) (Math.random() * m);
            // solange tempX und tempY mit random belegen, bis man ein Feld mit dem Wert 0 erwischt
            while (!(playground[tempX][tempY] == 0)) {
                tempX = (int) (Math.random() * n);
                tempY = (int) (Math.random() * m);
            }
            // in dieses Feld den Wert i eintragen (0 ... 15)
            playground[tempX][tempY] = i;
        }

//        isSolvable();
        if (!isSolvable())
            shuffleCards();

    }

//    public void isSolvable() {
    public boolean isSolvable() {
        
        boolean isSolvable = false;
        int countInversions = 0;

        int[] playgroundArray = new int[n * n];
        int   x               = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                playgroundArray[x] = playground[i][j];
                x++;
            }
        }

        for (int i = 0; i < n * n; i++) {
            for (int j = 0; j < i; j++) {
                if (playgroundArray[j] > playgroundArray[i])
                    countInversions++;
            }
        }

//        return countInversions % 2 == 0;

        if (countInversions % 2 == 0)
            isSolvable = true;
        else
            isSolvable = false;
        
        return isSolvable;
    }


    public void moveCard(int xClicked, int yClicked) {
        int X0 = -1, Y0 = -1;
        // zuerst das leere Feld finden
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                // leeres Feld wird durch den Wert 0 repraesentiert
                if (playground[i][j] == 0) {
                    X0 = i;
                    Y0 = j;
                }
            }
        }
        // hier sind X0 und Y0 definitiv mit dem leeren Feld belegt
        // X0 = Zeilenindex
        // Y0 = Spaltenindex

        // isPossible ist ein Boolean, der angibt, ob der geplante Zug erlaubt ist
        // vor der Ueberpruefung ist er per default auf false gesetzt
        isPossible = false;

        // hier wird ueberprueft, ob die geklickte Stelle mit der Zeile ODER der Spalte des freien Feldes uebereinstimmt
        // wenn weder Zeile noch Spalte uebereinstimmen, wird nichts passieren
        // wenn es eine Uebereinstimmung gibt, geht es weiter...
        if (X0 == xClicked || Y0 == yClicked)
            // ... wenn Zeile UND Spalte uebereinstimmen, wurde auf das leere Feld geklickt. Dann soll nichts passieren
            // (dies simuliert ein exclusive-oder)
            if (!(X0 == xClicked && Y0 == yClicked)) {
                // wenn man hier angelangt ist, stimmte Zeile ODER Spalte ueberein... pruefen, welcher Fall zutrifft

                // X0 ist Zeilenindex -> horizontal bewegen!
                if (X0 == xClicked)
                    // herausfinden, ob nach links oder rechts bewegen
                    // wenn Spaltenindex von freiem Feld kleiner als yClicked ist, muessen wir nach links
                    // wenn Spaltenindex von freiem Feld groesser als yClicked ist, muessen wir nach rechts
                    if (Y0 < yClicked)
                        // nach links bewegen
                        for (int i = Y0 + 1; i <= yClicked; i++)
                            // for-Schleife, damit nicht nur einzelne Karten bewegt werden koennen, sondern ganze Kolonnen
                            // das beschleunigt das Spiel, bessere User Experience
                            playground[xClicked][i - 1] = playground[xClicked][i];
                    else
                        // nach rechts bewegen
                        for (int i = Y0; i > yClicked; i--)
                            // ganze Kolonne bewegen
                            playground[xClicked][i] = playground[xClicked][i - 1];
                // analog zu oben:
                // Y0 ist Spaltenindex -> vertikal bewegen
                if (Y0 == yClicked)
                    // herausfinden, ob nach oben oder unten bewegen
                    if (X0 < xClicked)
                        // nach oben bewegen
                        for (int i = X0 + 1; i <= xClicked; i++)
                            playground[i - 1][yClicked] = playground[i][yClicked];
                    else
                        // nach unten bewegen
                        for (int i = X0; i > xClicked; i--)
                            playground[i][yClicked] = playground[i - 1][yClicked];
                // die urspruengliche Karte, die geklickt wurde, auf 0 setzen
                playground[xClicked][yClicked] = 0;
                isPossible = true;
            }
    }


    public boolean checkFinished(int N, int M) {
        boolean finished = false;
        // das Spiel kann nur beendet sein, wenn die letzte Karte (Brett-Index: 3, 3) den Wert 0 hat
        // ueberpruefen, ob die letzte Karte = 0 ist
        if (playground[N - 1][M - 1] == 0) {
            // wert_1_bis_15 ist eine Hilfsvariable zum Vergleichen der Kartenwerte
            int wert_1_bis_15 = 1;
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++) {
                    // wert_1_bis_15 wird bei jedem Schleifendurchlauf (also bei jeder Karte) inkrementiert
                    // am Anfang ist wert_1_bis_15 = 1; pruefe, ob playground[0, 0] = 1 ist
                    if (playground[i][j] == wert_1_bis_15) wert_1_bis_15++;
                }
            if (wert_1_bis_15 == (N * M)) finished = true;
        }
        return finished;
    } // checkFinished

    public int getFieldValue(int i, int j) {
        return playground[i][j];
    }

    public void setFieldValue(int i, int j, int wert) {
        playground[i][j] = wert;
    }

    public boolean isPossible() {
        return isPossible;
    }

} // Cards
