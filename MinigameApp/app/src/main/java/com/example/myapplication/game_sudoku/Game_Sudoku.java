package com.example.myapplication.game_sudoku;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.myapplication.Game;
import com.example.myapplication.GlobalVariables;
import com.example.myapplication.R;
import com.example.myapplication.Settings;
import com.example.myapplication.game_2048.Game_2048;
import com.example.myapplication.game_sudoku.view.sudokugrid.GameGrid;
import com.example.myapplication.game_sudoku.view.sudokugrid.SudokuCell;

public class Game_Sudoku extends AppCompatActivity {

    Button btn_easy, btn_medium, btn_difficult;
    RelativeLayout rl_startscreen;
    GridView buttonsGridView, sudokuGridView, sudokuGridViewBackground;
    TextView result, timeTv;
    private ImageButton backToMenu;
    private ImageButton toSettings;
    ImageView gridPic;
    long startTime;
    Button newGame;
    CardView cardviewTime;

    private int difficulty;

    private final int EASY   = 1;
    private final int MEDIUM = 2;
    private final int HARD   = 3;

    int currentTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        difficulty = EASY;
        Bundle b = getIntent().getExtras();
        int diff = b.getInt("difficulty");
        if(diff == 1)       difficulty = EASY;
        else if(diff == 2)  difficulty = MEDIUM;
        else                difficulty = HARD;

        this.startTime = System.currentTimeMillis();
        ((GlobalVariables) this.getApplication()).addOneRound();

        changeTheme();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sudoku);
        GameEngine.getInstance().createGrid(this);
        buttonsGridView = findViewById(R.id.buttonsgridview);
        sudokuGridView = findViewById(R.id.sudokuGridView);
        timeTv = findViewById(R.id.time_sudoku);
        //sudokuGridViewBackground = findViewById(R.id.sudokuGridViewBackground);
        result = findViewById(R.id.textViewResultWin);
        gridPic = findViewById(R.id.gridPic);
        newGame = findViewById(R.id.button_Sudoku_new_Game);
        cardviewTime = findViewById(R.id.cardViewTime);
        GameGrid grid = new GameGrid(this);

        // Button to go back to the previous screen
        backToMenu = (ImageButton) findViewById(R.id.backToMenu);
        backToMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        // Button to go to settings
        toSettings = (ImageButton) findViewById(R.id.toSettings);
        toSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });

        // Button that resets the game
        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(Game_Sudoku.this, Game_Sudoku.class);
                finish();
                i.putExtra("difficulty", getDifficulty());
                startActivity(i);
            }
        });

        // timer that counts up
        long totalSeconds = 100000;
        long intervalSeconds = 1;

        CountDownTimer timer = new CountDownTimer(totalSeconds * 1000, intervalSeconds * 1000) {
            public void onTick(long millisUntilFinished) {
                timeTv.setText("" + (totalSeconds * 1000 - millisUntilFinished) / 1000);
            }

            public void onFinish() {
            }
        };


        timer.start();

        if (grid.isWon()) {
            result.setVisibility(View.VISIBLE);
            result.setText("CONGRATULATIONS! YOU WON!");
            result.setGravity(Gravity.CENTER);
            timer.cancel();
            SharedPreferences sp = this.getSharedPreferences("gamesetting", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt("bestscore", Integer.parseInt(timer.toString()));
            editor.apply();
        }
    }

    // Get saved Theme out of Shared Preferances
    private void changeTheme(){

        SharedPreferences themes = getSharedPreferences("theme", 0);

        switch (themes.getInt("theme", 0)) {
            case 1:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
            case 2:
                setTheme(R.style.green_theme);
                currentTheme = 1;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(1);
                break;
            case 3:
                setTheme(R.style.dark_theme);
                currentTheme = 3;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(3);
                break;
            default:
                setTheme(R.style.blue_theme);
                currentTheme = 2;
                ((GlobalVariables) this.getApplication()).setCurrentTheme(2);
                break;
        }
    }

    // Checks if the theme has been changed and then applies it
    @Override
    protected void onResume() {
        super.onResume();

        int themeNr = ((GlobalVariables) this.getApplication()).getCurrentTheme();

        if (themeNr != currentTheme) {
            changeTheme();
            recreate();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        // sets current game time to total time for the game
        long endTime = System.currentTimeMillis();
        long millis = endTime - startTime;

        ((GlobalVariables) this.getApplication()).setTotalTime(millis);
        ((GlobalVariables) this.getApplication()).setTime_sudoku(millis); // Hier setTime_deinSpiel

    }



    private void printSudoku(int Sudoku[][]) {
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                System.out.print(Sudoku[x][y] + "|");
            }
            System.out.println();
        }
    }

    public Button getBtn_easy() {
        return btn_easy;
    }

    public Button getBtn_medium() {
        return btn_medium;
    }

    public Button getBtn_difficult() {
        return btn_difficult;
    }

    public TextView getResult() {
        return result;
    }

    public GridView getButtonsGridView() {
        return buttonsGridView;
    }

    public GridView getSudokuGridView() {
        return sudokuGridView;
    }

    public TextView getTimeTv() {
        return timeTv;
    }

    public ImageView getGridPic() {
        return gridPic;
    }

    public Button getNewGame() {
        return newGame;
    }

    public CardView getCardviewTime() {
        return cardviewTime;
    }

    public int getDifficulty() {
        return difficulty;
    }

    // Brings the user to the previous activity
    private void goBack() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    // Brings the user to the settings menu
    private void goToSettings() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

}
















