package com.example.myapplication.game_sudoku.view.sudokugrid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;


public class SudokuCell extends BaseSudokuCell {

    private Paint mPaint;
    private Paint sPaint;

    public SudokuCell( Context context ){
        super(context);

        mPaint = new Paint();
        sPaint = new Paint();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawNumber(canvas);
        drawLines(canvas);
    }

    //draws the numbers for the grid
    private void drawNumber(Canvas canvas){
        mPaint.setColor(Color.BLACK);
        mPaint.setTextSize(60);
        mPaint.setStyle(Paint.Style.FILL);

        Typeface type = Typeface.createFromAsset(super.getContext().getAssets(),"luckiest_guy.ttf");
        mPaint.setTypeface(type);

        Rect bounds = new Rect();
        mPaint.getTextBounds(String.valueOf(getValue()), 0, String.valueOf(getValue()).length(), bounds);

        if( getValue() != 0 ){
            canvas.drawText(String.valueOf(getValue()), (getWidth() - bounds.width())/2, (getHeight() + bounds.height())/2	, mPaint);
        }
    }

    // draws the lines of the grid
    private void drawLines(Canvas canvas) {
        mPaint.setColor(Color.BLACK);
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.STROKE);
        sPaint.setColor(Color.BLACK);
        sPaint.setStrokeWidth(10);
        sPaint.setStyle(Paint.Style.STROKE);

        canvas.drawRect(0, 0, getWidth(), getHeight(), mPaint);

        canvas.drawLine(3 * getWidth(), 0, 3 * getWidth(), 9 * getHeight(), sPaint);
    }
}