package com.example.myapplication.game_2048;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class Game_2048_3x3 extends AppCompatActivity {

    HashMap<String, Integer> map = null;
    HashMap<String, Integer> prevmap = null;
    GridView grid = null;

    public Game_2048_3x3(HashMap m)
    {
        map=m;
    }
    public Game_2048_3x3()
    {
        map=new HashMap<>();
    }

    int score = 0;
    TextView scoreTv, timeTv = null;
    Button button, button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_2048_3x3);
        scoreTv=(TextView) findViewById(R.id.score_2048);
        timeTv = (TextView) findViewById(R.id.time_2048);
        grid=(GridView)findViewById(R.id.gamebackground);
        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);

        Random random1=new Random();
        int r1 = random1.nextInt(9-1+1)+1;
        Random random2=new Random();
        int r2 = random2.nextInt(9-1+1)+1;
        if(r1==r2 && r2!=9)
        {
            r2++;
        }
        else
        {
            if(r1==r2)
            {
                r2--;
            }
        }



        String[] nums = new String[]{"1","2","3","4","5","6","7","8","9"};
        Random r3 = new Random();
        Random r4 = new Random();
        int[] choice={2,4};
        int ind1=r3.nextInt(choice.length);
        System.out.println(ind1);
        int ind2=r3.nextInt(choice.length);
        System.out.println(ind2);
        for(int i=0 ;i<nums.length;i++)
        {
            if(Integer.parseInt(nums[i])==r1)
            {
                map.put(nums[i],choice[ind1]);
            }
            if(Integer.parseInt(nums[i])==r2)
            {
                map.put(nums[i],choice[ind2]);
            }
            if(Integer.parseInt(nums[i])!=r2 && Integer.parseInt(nums[i])!=r1)
            {
                map.put(nums[i],0);
            }

        }
        // Populate a List from Array elements
        final List<String> plantsList = new ArrayList<String>(Arrays.asList(nums));

        // Data bind GridView with ArrayAdapter (String Array elements)
        gridChanged(plantsList);


        grid.setOnTouchListener(new OnSwipeTouchListener_3x3(Game_2048_3x3.this){
            public void onSwipeTop() {
                prevmap=new HashMap<>(map);
                int k = 0;
                while(!(allOkTop(1) && allOkTop(2) && allOkTop(3) && allOkTop(4))) {
                    k = 1;
                    for (int i = 9; i > 3; i--) {
                        int j = i;
                        if (j > 3 && map.get(j + "") == map.get((j - 3) + "")) {
                            map.put((j - 3) + "", map.get(j + "") + map.get((j - 3) + ""));
                            map.put(j + "", 0);
                            String score=scoreTv.getText().toString();
                            int scorenumber= Integer.parseInt(score);
                            scorenumber+=map.get((j-3)+"");
                            scoreTv.setText(scorenumber+"");
                        } else if (j > 3 && map.get((j - 3) + "") == 0) {
                            map.put((j - 3) + "", map.get(j + ""));
                            map.put(j + "", 0);
                        }
                    }
                }

                gridChanged(plantsList);
                generateRandomNumber();
                checkSwipe();
            }
            public void onSwipeRight() {
                prevmap=new HashMap<>(map);
                int k=0;
                int l=0;
                while(l==0 || !(allOkRight(4) && allOkRight(8) && allOkRight(12) && allOkRight(16))) {
                    l=1;
                    for (int i = 1; i < 10; i++) {
                        int j = i;
                        if (k == 0 && map.get(j + "") == map.get((j + 1) + "")) {
                            map.put((j + 1) + "", map.get(j + "") + map.get((j + 1) + ""));
                            map.put(j + "", 0);
                            String score=scoreTv.getText().toString();
                            int scorenumber= Integer.parseInt(score);
                            scorenumber+=map.get((j+1)+"");
                            scoreTv.setText(scorenumber+"");
                        } else if (k == 0 && map.get((j + 1) + "") == 0) {
                            map.put((j + 1) + "", map.get(j + ""));
                            map.put(j + "", 0);
                        }
                        if ((j / 3) != (j + 1) / 3) {
                            k = 1;
                        } else {
                            k = 0;
                        }
                    }
                }
                gridChanged(plantsList);
                generateRandomNumber();
                checkSwipe();
            }
            public void onSwipeLeft() {
                prevmap=new HashMap<>(map);
                int k=0;
                int l=0;
                while(l==0 || !(allOkLeft(1) && allOkLeft(5) && allOkLeft(9) && allOkLeft(13))) {
                    l=1;
                    for (int i = 9; i > 1; i--) {
                        int j = i;
                        if (j == 7 || j == 4) {
                            k = 1;
                        } else {
                            k = 0;
                        }
                        if (k == 0 && map.get(j + "") == map.get((j - 1) + "")) {
                            map.put((j - 1) + "", map.get(j + "") + map.get((j - 1) + ""));
                            map.put(j + "", 0);
                            String score=scoreTv.getText().toString();
                            int scorenumber= Integer.parseInt(score);
                            scorenumber+=map.get((j-1)+"");
                            scoreTv.setText(scorenumber+"");
                        } else if (k == 0 && map.get((j - 1) + "") == 0) {
                            map.put((j - 1) + "", map.get(j + ""));
                            map.put(j + "", 0);
                        }

                    }
                }
                gridChanged(plantsList);
                generateRandomNumber();
                checkSwipe();
            }
            public void onSwipeBottom() {
                prevmap=new HashMap<>(map);
                int k=0;
                while (k==0 || !(allOkBottom(7) && allOkBottom(8) && allOkBottom(9))) {
                    k=1;
                    for (int i = 1; i < 7; i++) {
                        int j = i;

                        if (j < 7 && map.get(j + "") == map.get((j + 3) + "")) {
                            map.put((j + 3) + "", map.get(j + "") + map.get((j + 3) + ""));
                            map.put(j + "", 0);
                            String score=scoreTv.getText().toString();
                            int scorenumber= Integer.parseInt(score);
                            scorenumber+=map.get((j+3)+"");
                            scoreTv.setText(scorenumber+"");
                        } else if (j < 7 && map.get((j + 3) + "") == 0) {
                            map.put((j + 3) + "", map.get(j + ""));
                            map.put(j + "", 0);
                        }
                    }
                }

                gridChanged(plantsList);
                generateRandomNumber();
                checkSwipe();
            }
        });

    }
    public boolean allOkTop(int loc)
    {
        if(map.get(loc+"")==0 && map.get((loc+3)+"")==0 && map.get(""+(loc+6))==0 && map.get(""+(loc+9))==0)
        {
            return true;
        }
        else if(map.get((loc+3)+"")==0 && map.get(""+(loc+6))==0 && map.get(""+(loc+9))==0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc+6))==0 && map.get(""+(loc+9))==0 && map.get(""+(loc+3))!=0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc+9))==0 && map.get(""+(loc+3))!=0 && map.get(""+loc)!=0 && map.get(""+(loc+6))!=0)
        {
            return true;
        }
        else if(map.get(loc+"")!=0 && map.get((loc+3)+"")!=0 && map.get(""+(loc+6))!=0 && map.get(""+(loc+9))!=0)
        {
            return true;
        }
        return false;
    }
    public boolean allOkBottom(int loc)
    {
        if(map.get(loc+"")==0 && map.get((loc-3)+"")==0 && map.get(""+(loc-6))==0 && map.get(""+(loc-9))==0)
        {
            return true;
        }
        else if(map.get((loc-3)+"")==0 && map.get(""+(loc-6))==0 && map.get(""+(loc-9))==0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc-6))==0 && map.get(""+(loc-9))==0 && map.get(""+(loc-3))!=0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc-9))==0 && map.get(""+(loc-3))!=0 && map.get(""+loc)!=0 && map.get(""+(loc-6))!=0)
        {
            return true;
        }
        else if(map.get(loc+"")!=0 && map.get((loc-3)+"")!=0 && map.get(""+(loc-6))!=0 && map.get(""+(loc-9))!=0)
        {
            return true;
        }
        return false;
    }
    public boolean allOkRight(int loc)
    {
        if(map.get(loc+"")==0 && map.get((loc-1)+"")==0 && map.get(""+(loc-2))==0)
        {
            return true;
        }
        else if(map.get((loc-1)+"")==0 && map.get(""+(loc-2))==0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc-2))==0 && map.get(""+(loc-1))!=0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc-1))!=0 && map.get(""+loc)!=0 && map.get(""+(loc-2))!=0)
        {
            return true;
        }
        else if(map.get(loc+"")!=0 && map.get((loc-1)+"")!=0 && map.get(""+(loc-2))!=0)
        {
            return true;
        }
        return false;
    }
    public boolean allOkLeft(int loc)
    {
        if(map.get(loc+"")==0 && map.get((loc+1)+"")==0 && map.get(""+(loc+2))==0)
        {
            return true;
        }
        else if(map.get((loc+1)+"")==0 && map.get(""+(loc+2))==0 && map.get(""+loc)!=0)
        {
            return true;
        }
        else if(map.get(""+(loc+2))==0 && map.get(""+(loc+1))!=0 && map.get(""+(loc))!=0)
        {
            return true;
        }
        else if(map.get(""+(loc+1))!=0 && map.get(""+(loc))!=0 && map.get(""+(loc+2))!=0)
        {
            return true;
        }
        else if(map.get(loc+"")!=0 && map.get((loc+1)+"")!=0 && map.get(""+(loc+2))!=0)
        {
            return true;
        }
        return false;
    }
    public void checkSwipe()
    {
        ConstraintLayout cl=(ConstraintLayout) findViewById(R.id.wholelayout);
        LinearLayout lll= new LinearLayout(this);
        lll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        //lll.setBackgroundColor(getResources().getColor(R.color.translucent, null));
        lll.setGravity(Gravity.CENTER);
        TextView result= new TextView(this);
        for(String key : map.keySet())
        {
            if(map.get(key)==2048)
            {
                result.setText("CONGRATULATIONS! YOU WIN!");
                result.setGravity(Gravity.CENTER);
                lll.addView(result);
                cl.addView(lll);
                return;
            }
        }
        for(String key : map.keySet())
        {
            if(map.get(key)!=prevmap.get(key))
            {

                return;
            }
        }

        result.setText("GAME OVER! YOU LOSE!");
        result.setGravity(Gravity.CENTER);
        lll.addView(result);
        cl.addView(lll);
//        prevmap=map;

    }
    public void generateRandomNumber()
    {
        Random ran = new Random();
        int i=ran.nextInt(9)+1;
        int flag=0;
        for(String key: map.keySet())
        {
            if(map.get(key)!=0)
            {
                flag++;
            }
        }
        if(flag!=9)
        {
            while(map.get(i+"")!=0)
            {
                i=ran.nextInt(9)+1;
            }
            int[] choice={2,4};
            int ind1=ran.nextInt(choice.length);
            map.put(i+"",choice[ind1]);
            String[] nums = new String[]{"1","2","3","4","5","6","7","8","9"};
            List<String> plantsList= Arrays.asList(nums);

            gridChanged(plantsList);
        }

    }
    public void gridChanged(List plantsList)
    {
        final List<String> l=plantsList;
        grid.setAdapter(new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, l){
            public View getView(int position, View convertView, ViewGroup parent) {

                // Return the GridView current item as a View
                View view = super.getView(position,convertView,parent);


                // Convert the view as a TextView widget
                TextView tv = (TextView) view;

                tv.setBackgroundColor(Color.parseColor("#bbada0"));

                //tv.setTextColor(Color.DKGRAY);

                // Set the layout parameters for TextView widget
                RelativeLayout.LayoutParams lp =  new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT
                );
                tv.setLayoutParams(lp);

                tv.setWidth(100);
                tv.setHeight(250);

                //tv.set

                // Display TextView text in center position
                tv.setGravity(Gravity.CENTER);

                tv.setTextSize(40);

                // Set the TextView text (GridView item text)
                if(map.get(l.get(position))!=0)
                {
                    tv.setText(map.get(l.get(position))+"");
                }
                else
                {
                    tv.setText("");
                }

                tv.setId(position);

                // Set the TextView background color
                tv.setBackgroundColor(Color.parseColor("#f2b179"));

                // Return the TextView widget as GridView item
                return tv;
            }
        });
    }

    public void retry(View v)
    {
        Intent i= new Intent(Game_2048_3x3.this, Game_2048.class);
        startActivity(i);
    }
}

class OnSwipeTouchListener_3x3 implements View.OnTouchListener {

    private final GestureDetector gestureDetector;

    public OnSwipeTouchListener_3x3 (Context ctx){
        gestureDetector = new GestureDetector(ctx, new GestureListener());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }
                        result = true;
                    }
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                    result = true;
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    public void onSwipeRight() {
    }

    public void onSwipeLeft() {
    }

    public void onSwipeTop() {
    }

    public void onSwipeBottom() {
    }
}