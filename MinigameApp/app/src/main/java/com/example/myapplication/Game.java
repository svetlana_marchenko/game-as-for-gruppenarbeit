package com.example.myapplication;

import java.io.Serializable;

public class Game implements Serializable {

    private String gameName;
    private String gamePicName;
    private String highScore;
    private int difficulty;
    private int activity_easy;
    private int activity_medium;
    private int activity_hard;
    private Class<?> game;

    // This constructior is used for the games in the main menu
    public Game(String gameName, String gamePicName, String highScore) {
        this.gameName = gameName;
        this.gamePicName = gamePicName;
        this.highScore = highScore;
    }

    // This constructor is used in the GameConfig-class for passing the game-layout for each difficulty
    public Game(String gameName, int difficulty, Class<?> game, int... activity) {

        this.gameName = gameName;
        this.difficulty = difficulty;
        this.game = game;

        this.activity_easy = activity[0];
        if (activity.length > 4) this.activity_medium = activity[1];
        if (activity.length > 5) this.activity_hard = activity[2];

    }

    // Setter-methods
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public void setGamePicName(String gamePicName) {
        this.gamePicName = gamePicName;
    }

    public void setHighScore(String highScore) {
        this.highScore = highScore;
    }


    // Getter-methods
    public String getGameName() {
        return gameName;
    }

    public String getGamePicName() {
        return gamePicName;
    }

    public String gethighScore() {
        return highScore;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public int getActivity_easy() {
        return activity_easy;
    }

    public int getActivity_medium() {
        return activity_medium;
    }

    public int getActivity_hard() {
        return activity_hard;
    }

    public Class<?> getGame() {
        return game;
    }


}
