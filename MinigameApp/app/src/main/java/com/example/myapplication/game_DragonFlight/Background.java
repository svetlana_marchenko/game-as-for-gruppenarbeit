package com.example.myapplication.game_DragonFlight;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Insets;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.widget.BaseAdapter;

import com.example.myapplication.R;

public class Background implements GameObject {
    private Bitmap bm;
    private Rect destinationRect;
    private Rect imageRect;

    public Background(Bitmap bm, int currY){

        this.bm = bm;
       // this.imageRect = new Rect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        this.destinationRect = new Rect(0, currY, Constants.SCREEN_WIDTH, currY + Constants.SCREEN_HEIGHT);

    }

    public void incrementY(float y) {
        destinationRect.top += y;
        destinationRect.bottom += y;
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        canvas.drawBitmap(bm, null, destinationRect, paint);
    }

    @Override
    public void update() {

    }

    public Rect getRectangle() {
        return destinationRect;
    }
}